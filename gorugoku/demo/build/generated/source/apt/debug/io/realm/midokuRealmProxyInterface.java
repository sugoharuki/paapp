package io.realm;


public interface midokuRealmProxyInterface {
    public String realmGet$targetname();
    public void realmSet$targetname(String value);
    public String realmGet$flg();
    public void realmSet$flg(String value);
    public String realmGet$ids();
    public void realmSet$ids(String value);
}
