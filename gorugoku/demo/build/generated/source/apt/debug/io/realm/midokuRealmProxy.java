package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class midokuRealmProxy extends net.city.paapp.realmModel.midoku
    implements RealmObjectProxy, midokuRealmProxyInterface {

    static final class midokuColumnInfo extends ColumnInfo {
        long targetnameIndex;
        long flgIndex;
        long idsIndex;

        midokuColumnInfo(SharedRealm realm, Table table) {
            super(3);
            this.targetnameIndex = addColumnDetails(table, "targetname", RealmFieldType.STRING);
            this.flgIndex = addColumnDetails(table, "flg", RealmFieldType.STRING);
            this.idsIndex = addColumnDetails(table, "ids", RealmFieldType.STRING);
        }

        midokuColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new midokuColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final midokuColumnInfo src = (midokuColumnInfo) rawSrc;
            final midokuColumnInfo dst = (midokuColumnInfo) rawDst;
            dst.targetnameIndex = src.targetnameIndex;
            dst.flgIndex = src.flgIndex;
            dst.idsIndex = src.idsIndex;
        }
    }

    private midokuColumnInfo columnInfo;
    private ProxyState<net.city.paapp.realmModel.midoku> proxyState;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("targetname");
        fieldNames.add("flg");
        fieldNames.add("ids");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    midokuRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (midokuColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<net.city.paapp.realmModel.midoku>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$targetname() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.targetnameIndex);
    }

    @Override
    public void realmSet$targetname(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.targetnameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.targetnameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.targetnameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.targetnameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$flg() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.flgIndex);
    }

    @Override
    public void realmSet$flg(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.flgIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.flgIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.flgIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.flgIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$ids() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idsIndex);
    }

    @Override
    public void realmSet$ids(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'ids' cannot be changed after object was created.");
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("midoku");
        builder.addProperty("targetname", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("flg", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("ids", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
         return expectedObjectSchemaInfo;
    }

    public static midokuColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (!sharedRealm.hasTable("class_midoku")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'midoku' class is missing from the schema for this Realm.");
        }
        Table table = sharedRealm.getTable("class_midoku");
        final long columnCount = table.getColumnCount();
        if (columnCount != 3) {
            if (columnCount < 3) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 3 but was " + columnCount);
            }
            if (allowExtraColumns) {
                RealmLog.debug("Field count is more than expected - expected 3 but was %1$d", columnCount);
            } else {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 3 but was " + columnCount);
            }
        }
        Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
        for (long i = 0; i < columnCount; i++) {
            columnTypes.put(table.getColumnName(i), table.getColumnType(i));
        }

        final midokuColumnInfo columnInfo = new midokuColumnInfo(sharedRealm, table);

        if (!table.hasPrimaryKey()) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'ids' in existing Realm file. @PrimaryKey was added.");
        } else {
            if (table.getPrimaryKey() != columnInfo.idsIndex) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field ids");
            }
        }

        if (!columnTypes.containsKey("targetname")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'targetname' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("targetname") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'targetname' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.targetnameIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'targetname' is required. Either set @Required to field 'targetname' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("flg")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'flg' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("flg") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'flg' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.flgIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'flg' is required. Either set @Required to field 'flg' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("ids")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'ids' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("ids") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'ids' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.idsIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'ids' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
        }
        if (!table.hasSearchIndex(table.getColumnIndex("ids"))) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'ids' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
        }

        return columnInfo;
    }

    public static String getTableName() {
        return "class_midoku";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static net.city.paapp.realmModel.midoku createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        net.city.paapp.realmModel.midoku obj = null;
        if (update) {
            Table table = realm.getTable(net.city.paapp.realmModel.midoku.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("ids")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("ids"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(net.city.paapp.realmModel.midoku.class), false, Collections.<String> emptyList());
                    obj = new io.realm.midokuRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("ids")) {
                if (json.isNull("ids")) {
                    obj = (io.realm.midokuRealmProxy) realm.createObjectInternal(net.city.paapp.realmModel.midoku.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.midokuRealmProxy) realm.createObjectInternal(net.city.paapp.realmModel.midoku.class, json.getString("ids"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'ids'.");
            }
        }
        if (json.has("targetname")) {
            if (json.isNull("targetname")) {
                ((midokuRealmProxyInterface) obj).realmSet$targetname(null);
            } else {
                ((midokuRealmProxyInterface) obj).realmSet$targetname((String) json.getString("targetname"));
            }
        }
        if (json.has("flg")) {
            if (json.isNull("flg")) {
                ((midokuRealmProxyInterface) obj).realmSet$flg(null);
            } else {
                ((midokuRealmProxyInterface) obj).realmSet$flg((String) json.getString("flg"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static net.city.paapp.realmModel.midoku createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        net.city.paapp.realmModel.midoku obj = new net.city.paapp.realmModel.midoku();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("targetname")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((midokuRealmProxyInterface) obj).realmSet$targetname(null);
                } else {
                    ((midokuRealmProxyInterface) obj).realmSet$targetname((String) reader.nextString());
                }
            } else if (name.equals("flg")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((midokuRealmProxyInterface) obj).realmSet$flg(null);
                } else {
                    ((midokuRealmProxyInterface) obj).realmSet$flg((String) reader.nextString());
                }
            } else if (name.equals("ids")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((midokuRealmProxyInterface) obj).realmSet$ids(null);
                } else {
                    ((midokuRealmProxyInterface) obj).realmSet$ids((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'ids'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static net.city.paapp.realmModel.midoku copyOrUpdate(Realm realm, net.city.paapp.realmModel.midoku object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (net.city.paapp.realmModel.midoku) cachedRealmObject;
        }

        net.city.paapp.realmModel.midoku realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(net.city.paapp.realmModel.midoku.class);
            long pkColumnIndex = table.getPrimaryKey();
            String value = ((midokuRealmProxyInterface) object).realmGet$ids();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, value);
            }
            if (rowIndex != Table.NO_MATCH) {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(net.city.paapp.realmModel.midoku.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.midokuRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static net.city.paapp.realmModel.midoku copy(Realm realm, net.city.paapp.realmModel.midoku newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (net.city.paapp.realmModel.midoku) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        net.city.paapp.realmModel.midoku realmObject = realm.createObjectInternal(net.city.paapp.realmModel.midoku.class, ((midokuRealmProxyInterface) newObject).realmGet$ids(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        midokuRealmProxyInterface realmObjectSource = (midokuRealmProxyInterface) newObject;
        midokuRealmProxyInterface realmObjectCopy = (midokuRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$targetname(realmObjectSource.realmGet$targetname());
        realmObjectCopy.realmSet$flg(realmObjectSource.realmGet$flg());
        return realmObject;
    }

    public static long insert(Realm realm, net.city.paapp.realmModel.midoku object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(net.city.paapp.realmModel.midoku.class);
        long tableNativePtr = table.getNativePtr();
        midokuColumnInfo columnInfo = (midokuColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.midoku.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((midokuRealmProxyInterface) object).realmGet$ids();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$targetname = ((midokuRealmProxyInterface) object).realmGet$targetname();
        if (realmGet$targetname != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.targetnameIndex, rowIndex, realmGet$targetname, false);
        }
        String realmGet$flg = ((midokuRealmProxyInterface) object).realmGet$flg();
        if (realmGet$flg != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.flgIndex, rowIndex, realmGet$flg, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(net.city.paapp.realmModel.midoku.class);
        long tableNativePtr = table.getNativePtr();
        midokuColumnInfo columnInfo = (midokuColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.midoku.class);
        long pkColumnIndex = table.getPrimaryKey();
        net.city.paapp.realmModel.midoku object = null;
        while (objects.hasNext()) {
            object = (net.city.paapp.realmModel.midoku) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((midokuRealmProxyInterface) object).realmGet$ids();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$targetname = ((midokuRealmProxyInterface) object).realmGet$targetname();
            if (realmGet$targetname != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.targetnameIndex, rowIndex, realmGet$targetname, false);
            }
            String realmGet$flg = ((midokuRealmProxyInterface) object).realmGet$flg();
            if (realmGet$flg != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.flgIndex, rowIndex, realmGet$flg, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, net.city.paapp.realmModel.midoku object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(net.city.paapp.realmModel.midoku.class);
        long tableNativePtr = table.getNativePtr();
        midokuColumnInfo columnInfo = (midokuColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.midoku.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((midokuRealmProxyInterface) object).realmGet$ids();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$targetname = ((midokuRealmProxyInterface) object).realmGet$targetname();
        if (realmGet$targetname != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.targetnameIndex, rowIndex, realmGet$targetname, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.targetnameIndex, rowIndex, false);
        }
        String realmGet$flg = ((midokuRealmProxyInterface) object).realmGet$flg();
        if (realmGet$flg != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.flgIndex, rowIndex, realmGet$flg, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.flgIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(net.city.paapp.realmModel.midoku.class);
        long tableNativePtr = table.getNativePtr();
        midokuColumnInfo columnInfo = (midokuColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.midoku.class);
        long pkColumnIndex = table.getPrimaryKey();
        net.city.paapp.realmModel.midoku object = null;
        while (objects.hasNext()) {
            object = (net.city.paapp.realmModel.midoku) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((midokuRealmProxyInterface) object).realmGet$ids();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$targetname = ((midokuRealmProxyInterface) object).realmGet$targetname();
            if (realmGet$targetname != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.targetnameIndex, rowIndex, realmGet$targetname, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.targetnameIndex, rowIndex, false);
            }
            String realmGet$flg = ((midokuRealmProxyInterface) object).realmGet$flg();
            if (realmGet$flg != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.flgIndex, rowIndex, realmGet$flg, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.flgIndex, rowIndex, false);
            }
        }
    }

    public static net.city.paapp.realmModel.midoku createDetachedCopy(net.city.paapp.realmModel.midoku realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        net.city.paapp.realmModel.midoku unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new net.city.paapp.realmModel.midoku();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (net.city.paapp.realmModel.midoku) cachedObject.object;
            }
            unmanagedObject = (net.city.paapp.realmModel.midoku) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        midokuRealmProxyInterface unmanagedCopy = (midokuRealmProxyInterface) unmanagedObject;
        midokuRealmProxyInterface realmSource = (midokuRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$targetname(realmSource.realmGet$targetname());
        unmanagedCopy.realmSet$flg(realmSource.realmGet$flg());
        unmanagedCopy.realmSet$ids(realmSource.realmGet$ids());
        return unmanagedObject;
    }

    static net.city.paapp.realmModel.midoku update(Realm realm, net.city.paapp.realmModel.midoku realmObject, net.city.paapp.realmModel.midoku newObject, Map<RealmModel, RealmObjectProxy> cache) {
        midokuRealmProxyInterface realmObjectTarget = (midokuRealmProxyInterface) realmObject;
        midokuRealmProxyInterface realmObjectSource = (midokuRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$targetname(realmObjectSource.realmGet$targetname());
        realmObjectTarget.realmSet$flg(realmObjectSource.realmGet$flg());
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("midoku = proxy[");
        stringBuilder.append("{targetname:");
        stringBuilder.append(realmGet$targetname() != null ? realmGet$targetname() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{flg:");
        stringBuilder.append(realmGet$flg() != null ? realmGet$flg() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{ids:");
        stringBuilder.append(realmGet$ids() != null ? realmGet$ids() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        midokuRealmProxy amidoku = (midokuRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = amidoku.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = amidoku.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != amidoku.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
