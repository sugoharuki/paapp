package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class allmemberRealmProxy extends net.city.paapp.realmModel.allmember
    implements RealmObjectProxy, allmemberRealmProxyInterface {

    static final class allmemberColumnInfo extends ColumnInfo {
        long nameIndex;
        long useridIndex;
        long syainIndex;
        long furiganaIndex;
        long divisionnameIndex;

        allmemberColumnInfo(SharedRealm realm, Table table) {
            super(5);
            this.nameIndex = addColumnDetails(table, "name", RealmFieldType.STRING);
            this.useridIndex = addColumnDetails(table, "userid", RealmFieldType.STRING);
            this.syainIndex = addColumnDetails(table, "syain", RealmFieldType.STRING);
            this.furiganaIndex = addColumnDetails(table, "furigana", RealmFieldType.STRING);
            this.divisionnameIndex = addColumnDetails(table, "divisionname", RealmFieldType.STRING);
        }

        allmemberColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new allmemberColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final allmemberColumnInfo src = (allmemberColumnInfo) rawSrc;
            final allmemberColumnInfo dst = (allmemberColumnInfo) rawDst;
            dst.nameIndex = src.nameIndex;
            dst.useridIndex = src.useridIndex;
            dst.syainIndex = src.syainIndex;
            dst.furiganaIndex = src.furiganaIndex;
            dst.divisionnameIndex = src.divisionnameIndex;
        }
    }

    private allmemberColumnInfo columnInfo;
    private ProxyState<net.city.paapp.realmModel.allmember> proxyState;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("name");
        fieldNames.add("userid");
        fieldNames.add("syain");
        fieldNames.add("furigana");
        fieldNames.add("divisionname");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    allmemberRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (allmemberColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<net.city.paapp.realmModel.allmember>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$name() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nameIndex);
    }

    @Override
    public void realmSet$name(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.nameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.nameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.nameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.nameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$userid() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.useridIndex);
    }

    @Override
    public void realmSet$userid(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.useridIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.useridIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.useridIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.useridIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$syain() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.syainIndex);
    }

    @Override
    public void realmSet$syain(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.syainIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.syainIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.syainIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.syainIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$furigana() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.furiganaIndex);
    }

    @Override
    public void realmSet$furigana(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.furiganaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.furiganaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.furiganaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.furiganaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$divisionname() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.divisionnameIndex);
    }

    @Override
    public void realmSet$divisionname(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.divisionnameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.divisionnameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.divisionnameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.divisionnameIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("allmember");
        builder.addProperty("name", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("userid", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("syain", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("furigana", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("divisionname", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
         return expectedObjectSchemaInfo;
    }

    public static allmemberColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (!sharedRealm.hasTable("class_allmember")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'allmember' class is missing from the schema for this Realm.");
        }
        Table table = sharedRealm.getTable("class_allmember");
        final long columnCount = table.getColumnCount();
        if (columnCount != 5) {
            if (columnCount < 5) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 5 but was " + columnCount);
            }
            if (allowExtraColumns) {
                RealmLog.debug("Field count is more than expected - expected 5 but was %1$d", columnCount);
            } else {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 5 but was " + columnCount);
            }
        }
        Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
        for (long i = 0; i < columnCount; i++) {
            columnTypes.put(table.getColumnName(i), table.getColumnType(i));
        }

        final allmemberColumnInfo columnInfo = new allmemberColumnInfo(sharedRealm, table);

        if (table.hasPrimaryKey()) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key defined for field " + table.getColumnName(table.getPrimaryKey()) + " was removed.");
        }

        if (!columnTypes.containsKey("name")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'name' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("name") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'name' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.nameIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'name' is required. Either set @Required to field 'name' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("userid")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'userid' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("userid") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'userid' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.useridIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'userid' is required. Either set @Required to field 'userid' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("syain")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'syain' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("syain") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'syain' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.syainIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'syain' is required. Either set @Required to field 'syain' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("furigana")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'furigana' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("furigana") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'furigana' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.furiganaIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'furigana' is required. Either set @Required to field 'furigana' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("divisionname")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'divisionname' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("divisionname") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'divisionname' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.divisionnameIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'divisionname' is required. Either set @Required to field 'divisionname' or migrate using RealmObjectSchema.setNullable().");
        }

        return columnInfo;
    }

    public static String getTableName() {
        return "class_allmember";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static net.city.paapp.realmModel.allmember createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        net.city.paapp.realmModel.allmember obj = realm.createObjectInternal(net.city.paapp.realmModel.allmember.class, true, excludeFields);
        if (json.has("name")) {
            if (json.isNull("name")) {
                ((allmemberRealmProxyInterface) obj).realmSet$name(null);
            } else {
                ((allmemberRealmProxyInterface) obj).realmSet$name((String) json.getString("name"));
            }
        }
        if (json.has("userid")) {
            if (json.isNull("userid")) {
                ((allmemberRealmProxyInterface) obj).realmSet$userid(null);
            } else {
                ((allmemberRealmProxyInterface) obj).realmSet$userid((String) json.getString("userid"));
            }
        }
        if (json.has("syain")) {
            if (json.isNull("syain")) {
                ((allmemberRealmProxyInterface) obj).realmSet$syain(null);
            } else {
                ((allmemberRealmProxyInterface) obj).realmSet$syain((String) json.getString("syain"));
            }
        }
        if (json.has("furigana")) {
            if (json.isNull("furigana")) {
                ((allmemberRealmProxyInterface) obj).realmSet$furigana(null);
            } else {
                ((allmemberRealmProxyInterface) obj).realmSet$furigana((String) json.getString("furigana"));
            }
        }
        if (json.has("divisionname")) {
            if (json.isNull("divisionname")) {
                ((allmemberRealmProxyInterface) obj).realmSet$divisionname(null);
            } else {
                ((allmemberRealmProxyInterface) obj).realmSet$divisionname((String) json.getString("divisionname"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static net.city.paapp.realmModel.allmember createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        net.city.paapp.realmModel.allmember obj = new net.city.paapp.realmModel.allmember();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("name")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((allmemberRealmProxyInterface) obj).realmSet$name(null);
                } else {
                    ((allmemberRealmProxyInterface) obj).realmSet$name((String) reader.nextString());
                }
            } else if (name.equals("userid")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((allmemberRealmProxyInterface) obj).realmSet$userid(null);
                } else {
                    ((allmemberRealmProxyInterface) obj).realmSet$userid((String) reader.nextString());
                }
            } else if (name.equals("syain")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((allmemberRealmProxyInterface) obj).realmSet$syain(null);
                } else {
                    ((allmemberRealmProxyInterface) obj).realmSet$syain((String) reader.nextString());
                }
            } else if (name.equals("furigana")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((allmemberRealmProxyInterface) obj).realmSet$furigana(null);
                } else {
                    ((allmemberRealmProxyInterface) obj).realmSet$furigana((String) reader.nextString());
                }
            } else if (name.equals("divisionname")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((allmemberRealmProxyInterface) obj).realmSet$divisionname(null);
                } else {
                    ((allmemberRealmProxyInterface) obj).realmSet$divisionname((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static net.city.paapp.realmModel.allmember copyOrUpdate(Realm realm, net.city.paapp.realmModel.allmember object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (net.city.paapp.realmModel.allmember) cachedRealmObject;
        }

        return copy(realm, object, update, cache);
    }

    public static net.city.paapp.realmModel.allmember copy(Realm realm, net.city.paapp.realmModel.allmember newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (net.city.paapp.realmModel.allmember) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        net.city.paapp.realmModel.allmember realmObject = realm.createObjectInternal(net.city.paapp.realmModel.allmember.class, false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        allmemberRealmProxyInterface realmObjectSource = (allmemberRealmProxyInterface) newObject;
        allmemberRealmProxyInterface realmObjectCopy = (allmemberRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$name(realmObjectSource.realmGet$name());
        realmObjectCopy.realmSet$userid(realmObjectSource.realmGet$userid());
        realmObjectCopy.realmSet$syain(realmObjectSource.realmGet$syain());
        realmObjectCopy.realmSet$furigana(realmObjectSource.realmGet$furigana());
        realmObjectCopy.realmSet$divisionname(realmObjectSource.realmGet$divisionname());
        return realmObject;
    }

    public static long insert(Realm realm, net.city.paapp.realmModel.allmember object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(net.city.paapp.realmModel.allmember.class);
        long tableNativePtr = table.getNativePtr();
        allmemberColumnInfo columnInfo = (allmemberColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.allmember.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$name = ((allmemberRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        }
        String realmGet$userid = ((allmemberRealmProxyInterface) object).realmGet$userid();
        if (realmGet$userid != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.useridIndex, rowIndex, realmGet$userid, false);
        }
        String realmGet$syain = ((allmemberRealmProxyInterface) object).realmGet$syain();
        if (realmGet$syain != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.syainIndex, rowIndex, realmGet$syain, false);
        }
        String realmGet$furigana = ((allmemberRealmProxyInterface) object).realmGet$furigana();
        if (realmGet$furigana != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.furiganaIndex, rowIndex, realmGet$furigana, false);
        }
        String realmGet$divisionname = ((allmemberRealmProxyInterface) object).realmGet$divisionname();
        if (realmGet$divisionname != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.divisionnameIndex, rowIndex, realmGet$divisionname, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(net.city.paapp.realmModel.allmember.class);
        long tableNativePtr = table.getNativePtr();
        allmemberColumnInfo columnInfo = (allmemberColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.allmember.class);
        net.city.paapp.realmModel.allmember object = null;
        while (objects.hasNext()) {
            object = (net.city.paapp.realmModel.allmember) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$name = ((allmemberRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            }
            String realmGet$userid = ((allmemberRealmProxyInterface) object).realmGet$userid();
            if (realmGet$userid != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.useridIndex, rowIndex, realmGet$userid, false);
            }
            String realmGet$syain = ((allmemberRealmProxyInterface) object).realmGet$syain();
            if (realmGet$syain != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.syainIndex, rowIndex, realmGet$syain, false);
            }
            String realmGet$furigana = ((allmemberRealmProxyInterface) object).realmGet$furigana();
            if (realmGet$furigana != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.furiganaIndex, rowIndex, realmGet$furigana, false);
            }
            String realmGet$divisionname = ((allmemberRealmProxyInterface) object).realmGet$divisionname();
            if (realmGet$divisionname != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.divisionnameIndex, rowIndex, realmGet$divisionname, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, net.city.paapp.realmModel.allmember object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(net.city.paapp.realmModel.allmember.class);
        long tableNativePtr = table.getNativePtr();
        allmemberColumnInfo columnInfo = (allmemberColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.allmember.class);
        long rowIndex = OsObject.createRow(table);
        cache.put(object, rowIndex);
        String realmGet$name = ((allmemberRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
        }
        String realmGet$userid = ((allmemberRealmProxyInterface) object).realmGet$userid();
        if (realmGet$userid != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.useridIndex, rowIndex, realmGet$userid, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.useridIndex, rowIndex, false);
        }
        String realmGet$syain = ((allmemberRealmProxyInterface) object).realmGet$syain();
        if (realmGet$syain != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.syainIndex, rowIndex, realmGet$syain, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.syainIndex, rowIndex, false);
        }
        String realmGet$furigana = ((allmemberRealmProxyInterface) object).realmGet$furigana();
        if (realmGet$furigana != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.furiganaIndex, rowIndex, realmGet$furigana, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.furiganaIndex, rowIndex, false);
        }
        String realmGet$divisionname = ((allmemberRealmProxyInterface) object).realmGet$divisionname();
        if (realmGet$divisionname != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.divisionnameIndex, rowIndex, realmGet$divisionname, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.divisionnameIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(net.city.paapp.realmModel.allmember.class);
        long tableNativePtr = table.getNativePtr();
        allmemberColumnInfo columnInfo = (allmemberColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.allmember.class);
        net.city.paapp.realmModel.allmember object = null;
        while (objects.hasNext()) {
            object = (net.city.paapp.realmModel.allmember) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = OsObject.createRow(table);
            cache.put(object, rowIndex);
            String realmGet$name = ((allmemberRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
            }
            String realmGet$userid = ((allmemberRealmProxyInterface) object).realmGet$userid();
            if (realmGet$userid != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.useridIndex, rowIndex, realmGet$userid, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.useridIndex, rowIndex, false);
            }
            String realmGet$syain = ((allmemberRealmProxyInterface) object).realmGet$syain();
            if (realmGet$syain != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.syainIndex, rowIndex, realmGet$syain, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.syainIndex, rowIndex, false);
            }
            String realmGet$furigana = ((allmemberRealmProxyInterface) object).realmGet$furigana();
            if (realmGet$furigana != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.furiganaIndex, rowIndex, realmGet$furigana, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.furiganaIndex, rowIndex, false);
            }
            String realmGet$divisionname = ((allmemberRealmProxyInterface) object).realmGet$divisionname();
            if (realmGet$divisionname != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.divisionnameIndex, rowIndex, realmGet$divisionname, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.divisionnameIndex, rowIndex, false);
            }
        }
    }

    public static net.city.paapp.realmModel.allmember createDetachedCopy(net.city.paapp.realmModel.allmember realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        net.city.paapp.realmModel.allmember unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new net.city.paapp.realmModel.allmember();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (net.city.paapp.realmModel.allmember) cachedObject.object;
            }
            unmanagedObject = (net.city.paapp.realmModel.allmember) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        allmemberRealmProxyInterface unmanagedCopy = (allmemberRealmProxyInterface) unmanagedObject;
        allmemberRealmProxyInterface realmSource = (allmemberRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$name(realmSource.realmGet$name());
        unmanagedCopy.realmSet$userid(realmSource.realmGet$userid());
        unmanagedCopy.realmSet$syain(realmSource.realmGet$syain());
        unmanagedCopy.realmSet$furigana(realmSource.realmGet$furigana());
        unmanagedCopy.realmSet$divisionname(realmSource.realmGet$divisionname());
        return unmanagedObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("allmember = proxy[");
        stringBuilder.append("{name:");
        stringBuilder.append(realmGet$name() != null ? realmGet$name() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{userid:");
        stringBuilder.append(realmGet$userid() != null ? realmGet$userid() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{syain:");
        stringBuilder.append(realmGet$syain() != null ? realmGet$syain() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{furigana:");
        stringBuilder.append(realmGet$furigana() != null ? realmGet$furigana() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{divisionname:");
        stringBuilder.append(realmGet$divisionname() != null ? realmGet$divisionname() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        allmemberRealmProxy aallmember = (allmemberRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aallmember.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aallmember.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aallmember.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
