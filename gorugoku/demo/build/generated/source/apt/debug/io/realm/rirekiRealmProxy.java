package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class rirekiRealmProxy extends net.city.paapp.realmModel.rireki
    implements RealmObjectProxy, rirekiRealmProxyInterface {

    static final class rirekiColumnInfo extends ColumnInfo {
        long rirekinameIndex;
        long sendtypeIndex;
        long timesIndex;

        rirekiColumnInfo(SharedRealm realm, Table table) {
            super(3);
            this.rirekinameIndex = addColumnDetails(table, "rirekiname", RealmFieldType.STRING);
            this.sendtypeIndex = addColumnDetails(table, "sendtype", RealmFieldType.STRING);
            this.timesIndex = addColumnDetails(table, "times", RealmFieldType.STRING);
        }

        rirekiColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new rirekiColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final rirekiColumnInfo src = (rirekiColumnInfo) rawSrc;
            final rirekiColumnInfo dst = (rirekiColumnInfo) rawDst;
            dst.rirekinameIndex = src.rirekinameIndex;
            dst.sendtypeIndex = src.sendtypeIndex;
            dst.timesIndex = src.timesIndex;
        }
    }

    private rirekiColumnInfo columnInfo;
    private ProxyState<net.city.paapp.realmModel.rireki> proxyState;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("rirekiname");
        fieldNames.add("sendtype");
        fieldNames.add("times");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    rirekiRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (rirekiColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<net.city.paapp.realmModel.rireki>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$rirekiname() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.rirekinameIndex);
    }

    @Override
    public void realmSet$rirekiname(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'rirekiname' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$sendtype() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.sendtypeIndex);
    }

    @Override
    public void realmSet$sendtype(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.sendtypeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.sendtypeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.sendtypeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.sendtypeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$times() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.timesIndex);
    }

    @Override
    public void realmSet$times(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.timesIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.timesIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.timesIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.timesIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("rireki");
        builder.addProperty("rirekiname", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("sendtype", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("times", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
         return expectedObjectSchemaInfo;
    }

    public static rirekiColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (!sharedRealm.hasTable("class_rireki")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'rireki' class is missing from the schema for this Realm.");
        }
        Table table = sharedRealm.getTable("class_rireki");
        final long columnCount = table.getColumnCount();
        if (columnCount != 3) {
            if (columnCount < 3) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 3 but was " + columnCount);
            }
            if (allowExtraColumns) {
                RealmLog.debug("Field count is more than expected - expected 3 but was %1$d", columnCount);
            } else {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 3 but was " + columnCount);
            }
        }
        Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
        for (long i = 0; i < columnCount; i++) {
            columnTypes.put(table.getColumnName(i), table.getColumnType(i));
        }

        final rirekiColumnInfo columnInfo = new rirekiColumnInfo(sharedRealm, table);

        if (!table.hasPrimaryKey()) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'rirekiname' in existing Realm file. @PrimaryKey was added.");
        } else {
            if (table.getPrimaryKey() != columnInfo.rirekinameIndex) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field rirekiname");
            }
        }

        if (!columnTypes.containsKey("rirekiname")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'rirekiname' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("rirekiname") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'rirekiname' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.rirekinameIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'rirekiname' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
        }
        if (!table.hasSearchIndex(table.getColumnIndex("rirekiname"))) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'rirekiname' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
        }
        if (!columnTypes.containsKey("sendtype")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'sendtype' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("sendtype") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'sendtype' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.sendtypeIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'sendtype' is required. Either set @Required to field 'sendtype' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("times")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'times' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("times") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'times' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.timesIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'times' is required. Either set @Required to field 'times' or migrate using RealmObjectSchema.setNullable().");
        }

        return columnInfo;
    }

    public static String getTableName() {
        return "class_rireki";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static net.city.paapp.realmModel.rireki createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        net.city.paapp.realmModel.rireki obj = null;
        if (update) {
            Table table = realm.getTable(net.city.paapp.realmModel.rireki.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("rirekiname")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("rirekiname"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(net.city.paapp.realmModel.rireki.class), false, Collections.<String> emptyList());
                    obj = new io.realm.rirekiRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("rirekiname")) {
                if (json.isNull("rirekiname")) {
                    obj = (io.realm.rirekiRealmProxy) realm.createObjectInternal(net.city.paapp.realmModel.rireki.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.rirekiRealmProxy) realm.createObjectInternal(net.city.paapp.realmModel.rireki.class, json.getString("rirekiname"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'rirekiname'.");
            }
        }
        if (json.has("sendtype")) {
            if (json.isNull("sendtype")) {
                ((rirekiRealmProxyInterface) obj).realmSet$sendtype(null);
            } else {
                ((rirekiRealmProxyInterface) obj).realmSet$sendtype((String) json.getString("sendtype"));
            }
        }
        if (json.has("times")) {
            if (json.isNull("times")) {
                ((rirekiRealmProxyInterface) obj).realmSet$times(null);
            } else {
                ((rirekiRealmProxyInterface) obj).realmSet$times((String) json.getString("times"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static net.city.paapp.realmModel.rireki createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        net.city.paapp.realmModel.rireki obj = new net.city.paapp.realmModel.rireki();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("rirekiname")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((rirekiRealmProxyInterface) obj).realmSet$rirekiname(null);
                } else {
                    ((rirekiRealmProxyInterface) obj).realmSet$rirekiname((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("sendtype")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((rirekiRealmProxyInterface) obj).realmSet$sendtype(null);
                } else {
                    ((rirekiRealmProxyInterface) obj).realmSet$sendtype((String) reader.nextString());
                }
            } else if (name.equals("times")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((rirekiRealmProxyInterface) obj).realmSet$times(null);
                } else {
                    ((rirekiRealmProxyInterface) obj).realmSet$times((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'rirekiname'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static net.city.paapp.realmModel.rireki copyOrUpdate(Realm realm, net.city.paapp.realmModel.rireki object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (net.city.paapp.realmModel.rireki) cachedRealmObject;
        }

        net.city.paapp.realmModel.rireki realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(net.city.paapp.realmModel.rireki.class);
            long pkColumnIndex = table.getPrimaryKey();
            String value = ((rirekiRealmProxyInterface) object).realmGet$rirekiname();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, value);
            }
            if (rowIndex != Table.NO_MATCH) {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(net.city.paapp.realmModel.rireki.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.rirekiRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static net.city.paapp.realmModel.rireki copy(Realm realm, net.city.paapp.realmModel.rireki newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (net.city.paapp.realmModel.rireki) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        net.city.paapp.realmModel.rireki realmObject = realm.createObjectInternal(net.city.paapp.realmModel.rireki.class, ((rirekiRealmProxyInterface) newObject).realmGet$rirekiname(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        rirekiRealmProxyInterface realmObjectSource = (rirekiRealmProxyInterface) newObject;
        rirekiRealmProxyInterface realmObjectCopy = (rirekiRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$sendtype(realmObjectSource.realmGet$sendtype());
        realmObjectCopy.realmSet$times(realmObjectSource.realmGet$times());
        return realmObject;
    }

    public static long insert(Realm realm, net.city.paapp.realmModel.rireki object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(net.city.paapp.realmModel.rireki.class);
        long tableNativePtr = table.getNativePtr();
        rirekiColumnInfo columnInfo = (rirekiColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.rireki.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((rirekiRealmProxyInterface) object).realmGet$rirekiname();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$sendtype = ((rirekiRealmProxyInterface) object).realmGet$sendtype();
        if (realmGet$sendtype != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.sendtypeIndex, rowIndex, realmGet$sendtype, false);
        }
        String realmGet$times = ((rirekiRealmProxyInterface) object).realmGet$times();
        if (realmGet$times != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.timesIndex, rowIndex, realmGet$times, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(net.city.paapp.realmModel.rireki.class);
        long tableNativePtr = table.getNativePtr();
        rirekiColumnInfo columnInfo = (rirekiColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.rireki.class);
        long pkColumnIndex = table.getPrimaryKey();
        net.city.paapp.realmModel.rireki object = null;
        while (objects.hasNext()) {
            object = (net.city.paapp.realmModel.rireki) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((rirekiRealmProxyInterface) object).realmGet$rirekiname();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$sendtype = ((rirekiRealmProxyInterface) object).realmGet$sendtype();
            if (realmGet$sendtype != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.sendtypeIndex, rowIndex, realmGet$sendtype, false);
            }
            String realmGet$times = ((rirekiRealmProxyInterface) object).realmGet$times();
            if (realmGet$times != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.timesIndex, rowIndex, realmGet$times, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, net.city.paapp.realmModel.rireki object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(net.city.paapp.realmModel.rireki.class);
        long tableNativePtr = table.getNativePtr();
        rirekiColumnInfo columnInfo = (rirekiColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.rireki.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((rirekiRealmProxyInterface) object).realmGet$rirekiname();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$sendtype = ((rirekiRealmProxyInterface) object).realmGet$sendtype();
        if (realmGet$sendtype != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.sendtypeIndex, rowIndex, realmGet$sendtype, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.sendtypeIndex, rowIndex, false);
        }
        String realmGet$times = ((rirekiRealmProxyInterface) object).realmGet$times();
        if (realmGet$times != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.timesIndex, rowIndex, realmGet$times, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.timesIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(net.city.paapp.realmModel.rireki.class);
        long tableNativePtr = table.getNativePtr();
        rirekiColumnInfo columnInfo = (rirekiColumnInfo) realm.schema.getColumnInfo(net.city.paapp.realmModel.rireki.class);
        long pkColumnIndex = table.getPrimaryKey();
        net.city.paapp.realmModel.rireki object = null;
        while (objects.hasNext()) {
            object = (net.city.paapp.realmModel.rireki) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((rirekiRealmProxyInterface) object).realmGet$rirekiname();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$sendtype = ((rirekiRealmProxyInterface) object).realmGet$sendtype();
            if (realmGet$sendtype != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.sendtypeIndex, rowIndex, realmGet$sendtype, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.sendtypeIndex, rowIndex, false);
            }
            String realmGet$times = ((rirekiRealmProxyInterface) object).realmGet$times();
            if (realmGet$times != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.timesIndex, rowIndex, realmGet$times, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.timesIndex, rowIndex, false);
            }
        }
    }

    public static net.city.paapp.realmModel.rireki createDetachedCopy(net.city.paapp.realmModel.rireki realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        net.city.paapp.realmModel.rireki unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new net.city.paapp.realmModel.rireki();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (net.city.paapp.realmModel.rireki) cachedObject.object;
            }
            unmanagedObject = (net.city.paapp.realmModel.rireki) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        rirekiRealmProxyInterface unmanagedCopy = (rirekiRealmProxyInterface) unmanagedObject;
        rirekiRealmProxyInterface realmSource = (rirekiRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$rirekiname(realmSource.realmGet$rirekiname());
        unmanagedCopy.realmSet$sendtype(realmSource.realmGet$sendtype());
        unmanagedCopy.realmSet$times(realmSource.realmGet$times());
        return unmanagedObject;
    }

    static net.city.paapp.realmModel.rireki update(Realm realm, net.city.paapp.realmModel.rireki realmObject, net.city.paapp.realmModel.rireki newObject, Map<RealmModel, RealmObjectProxy> cache) {
        rirekiRealmProxyInterface realmObjectTarget = (rirekiRealmProxyInterface) realmObject;
        rirekiRealmProxyInterface realmObjectSource = (rirekiRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$sendtype(realmObjectSource.realmGet$sendtype());
        realmObjectTarget.realmSet$times(realmObjectSource.realmGet$times());
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("rireki = proxy[");
        stringBuilder.append("{rirekiname:");
        stringBuilder.append(realmGet$rirekiname() != null ? realmGet$rirekiname() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{sendtype:");
        stringBuilder.append(realmGet$sendtype() != null ? realmGet$sendtype() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{times:");
        stringBuilder.append(realmGet$times() != null ? realmGet$times() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        rirekiRealmProxy arireki = (rirekiRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = arireki.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = arireki.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != arireki.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
