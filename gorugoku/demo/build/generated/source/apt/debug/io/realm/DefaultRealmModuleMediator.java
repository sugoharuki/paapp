package io.realm;


import android.util.JsonReader;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmProxyMediator;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@io.realm.annotations.RealmModule
class DefaultRealmModuleMediator extends RealmProxyMediator {

    private static final Set<Class<? extends RealmModel>> MODEL_CLASSES;
    static {
        Set<Class<? extends RealmModel>> modelClasses = new HashSet<Class<? extends RealmModel>>();
        modelClasses.add(net.city.paapp.realmModel.rireki.class);
        modelClasses.add(net.city.paapp.realmModel.allmember.class);
        modelClasses.add(net.city.paapp.realmModel.midoku.class);
        MODEL_CLASSES = Collections.unmodifiableSet(modelClasses);
    }

    @Override
    public Map<Class<? extends RealmModel>, OsObjectSchemaInfo> getExpectedObjectSchemaInfoMap() {
        Map<Class<? extends RealmModel>, OsObjectSchemaInfo> infoMap = new HashMap<Class<? extends RealmModel>, OsObjectSchemaInfo>();
        infoMap.put(net.city.paapp.realmModel.rireki.class, io.realm.rirekiRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(net.city.paapp.realmModel.allmember.class, io.realm.allmemberRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(net.city.paapp.realmModel.midoku.class, io.realm.midokuRealmProxy.getExpectedObjectSchemaInfo());
        return infoMap;
    }

    @Override
    public ColumnInfo validateTable(Class<? extends RealmModel> clazz, SharedRealm sharedRealm, boolean allowExtraColumns) {
        checkClass(clazz);

        if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
            return io.realm.rirekiRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        }
        if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
            return io.realm.allmemberRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        }
        if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
            return io.realm.midokuRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public List<String> getFieldNames(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
            return io.realm.rirekiRealmProxy.getFieldNames();
        }
        if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
            return io.realm.allmemberRealmProxy.getFieldNames();
        }
        if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
            return io.realm.midokuRealmProxy.getFieldNames();
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public String getTableName(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
            return io.realm.rirekiRealmProxy.getTableName();
        }
        if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
            return io.realm.allmemberRealmProxy.getTableName();
        }
        if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
            return io.realm.midokuRealmProxy.getTableName();
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E newInstance(Class<E> clazz, Object baseRealm, Row row, ColumnInfo columnInfo, boolean acceptDefaultValue, List<String> excludeFields) {
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        try {
            objectContext.set((BaseRealm) baseRealm, row, columnInfo, acceptDefaultValue, excludeFields);
            checkClass(clazz);

            if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
                return clazz.cast(new io.realm.rirekiRealmProxy());
            }
            if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
                return clazz.cast(new io.realm.allmemberRealmProxy());
            }
            if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
                return clazz.cast(new io.realm.midokuRealmProxy());
            }
            throw getMissingProxyClassException(clazz);
        } finally {
            objectContext.clear();
        }
    }

    @Override
    public Set<Class<? extends RealmModel>> getModelClasses() {
        return MODEL_CLASSES;
    }

    @Override
    public <E extends RealmModel> E copyOrUpdate(Realm realm, E obj, boolean update, Map<RealmModel, RealmObjectProxy> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
            return clazz.cast(io.realm.rirekiRealmProxy.copyOrUpdate(realm, (net.city.paapp.realmModel.rireki) obj, update, cache));
        }
        if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
            return clazz.cast(io.realm.allmemberRealmProxy.copyOrUpdate(realm, (net.city.paapp.realmModel.allmember) obj, update, cache));
        }
        if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
            return clazz.cast(io.realm.midokuRealmProxy.copyOrUpdate(realm, (net.city.paapp.realmModel.midoku) obj, update, cache));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public void insert(Realm realm, RealmModel object, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

        if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
            io.realm.rirekiRealmProxy.insert(realm, (net.city.paapp.realmModel.rireki) object, cache);
        } else if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
            io.realm.allmemberRealmProxy.insert(realm, (net.city.paapp.realmModel.allmember) object, cache);
        } else if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
            io.realm.midokuRealmProxy.insert(realm, (net.city.paapp.realmModel.midoku) object, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insert(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
                io.realm.rirekiRealmProxy.insert(realm, (net.city.paapp.realmModel.rireki) object, cache);
            } else if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
                io.realm.allmemberRealmProxy.insert(realm, (net.city.paapp.realmModel.allmember) object, cache);
            } else if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
                io.realm.midokuRealmProxy.insert(realm, (net.city.paapp.realmModel.midoku) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
                    io.realm.rirekiRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
                    io.realm.allmemberRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
                    io.realm.midokuRealmProxy.insert(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, RealmModel obj, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
            io.realm.rirekiRealmProxy.insertOrUpdate(realm, (net.city.paapp.realmModel.rireki) obj, cache);
        } else if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
            io.realm.allmemberRealmProxy.insertOrUpdate(realm, (net.city.paapp.realmModel.allmember) obj, cache);
        } else if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
            io.realm.midokuRealmProxy.insertOrUpdate(realm, (net.city.paapp.realmModel.midoku) obj, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
                io.realm.rirekiRealmProxy.insertOrUpdate(realm, (net.city.paapp.realmModel.rireki) object, cache);
            } else if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
                io.realm.allmemberRealmProxy.insertOrUpdate(realm, (net.city.paapp.realmModel.allmember) object, cache);
            } else if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
                io.realm.midokuRealmProxy.insertOrUpdate(realm, (net.city.paapp.realmModel.midoku) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
                    io.realm.rirekiRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
                    io.realm.allmemberRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
                    io.realm.midokuRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public <E extends RealmModel> E createOrUpdateUsingJsonObject(Class<E> clazz, Realm realm, JSONObject json, boolean update)
        throws JSONException {
        checkClass(clazz);

        if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
            return clazz.cast(io.realm.rirekiRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
            return clazz.cast(io.realm.allmemberRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
            return clazz.cast(io.realm.midokuRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createUsingJsonStream(Class<E> clazz, Realm realm, JsonReader reader)
        throws IOException {
        checkClass(clazz);

        if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
            return clazz.cast(io.realm.rirekiRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
            return clazz.cast(io.realm.allmemberRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
            return clazz.cast(io.realm.midokuRealmProxy.createUsingJsonStream(realm, reader));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createDetachedCopy(E realmObject, int maxDepth, Map<RealmModel, RealmObjectProxy.CacheData<RealmModel>> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) realmObject.getClass().getSuperclass();

        if (clazz.equals(net.city.paapp.realmModel.rireki.class)) {
            return clazz.cast(io.realm.rirekiRealmProxy.createDetachedCopy((net.city.paapp.realmModel.rireki) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(net.city.paapp.realmModel.allmember.class)) {
            return clazz.cast(io.realm.allmemberRealmProxy.createDetachedCopy((net.city.paapp.realmModel.allmember) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(net.city.paapp.realmModel.midoku.class)) {
            return clazz.cast(io.realm.midokuRealmProxy.createDetachedCopy((net.city.paapp.realmModel.midoku) realmObject, 0, maxDepth, cache));
        }
        throw getMissingProxyClassException(clazz);
    }

}
