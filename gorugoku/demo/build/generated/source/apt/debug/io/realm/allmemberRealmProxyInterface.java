package io.realm;


public interface allmemberRealmProxyInterface {
    public String realmGet$name();
    public void realmSet$name(String value);
    public String realmGet$userid();
    public void realmSet$userid(String value);
    public String realmGet$syain();
    public void realmSet$syain(String value);
    public String realmGet$furigana();
    public void realmSet$furigana(String value);
    public String realmGet$divisionname();
    public void realmSet$divisionname(String value);
}
