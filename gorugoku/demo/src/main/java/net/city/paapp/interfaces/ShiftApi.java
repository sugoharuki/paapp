package net.city.paapp.interfaces;

import net.city.paapp.Model.shiftModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ShiftApi {
    @FormUrlEncoded
    @POST(".")
    Call<List<shiftModel>> getShiftApi(@Field("date_start")String date_start,@Field("date_end")String date_end);
}
