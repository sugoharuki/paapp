package net.city.paapp.interfaces;

import net.city.paapp.Model.TimeLineComment;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface getCommentInterface {
    @FormUrlEncoded
    @POST("/getcomment.php")
    Call<List<TimeLineComment>> getCommentapi(@Field("ids") String ids);
}
