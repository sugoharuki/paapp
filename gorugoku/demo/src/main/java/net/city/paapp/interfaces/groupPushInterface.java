package net.city.paapp.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface groupPushInterface {
    @FormUrlEncoded
    @POST("/firebasepushgroupandroid.php")
    //@POST("/firebasepushgroup.php")
    Call<String> androidgrouppush(@Field("targetname") String targetname, @Field("msg") String msg, @Field("talktype") String talktype);
}
