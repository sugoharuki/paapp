package net.city.paapp.ListView;

public class ListActivityItem {
    private String mThumbnail = null;
    private String mTitle = null;
    private String mTitle2 = null;

    /**
     * 空のコンストラクタ
     */
    public ListActivityItem() {}

    /**
     * コンストラクタ
     * @param thumbnail サムネイル画像
     * @param title タイトル
     */
    public ListActivityItem(String thumbnail, String title, String title2) {
        mThumbnail = thumbnail;
        mTitle = title;
        mTitle2 = title2;
    }

    /**
     * サムネイル画像を設定
     * @param thumbnail サムネイル画像
     */
    public void setThumbnail2(String thumbnail) {
        mThumbnail = thumbnail;
    }

    /**
     * タイトルを設定
     * @param title タイトル
     */
    public void setmTitle2(String title,String title2) {
        mTitle = title;
        mTitle2 = title2;
    }


    /**
     * サムネイル画像を取得
     * @return サムネイル画像
     */
    public String getThumbnail2() {
        return mThumbnail;
    }

    /**
     * タイトルを取得
     * @return タイトル
     */
    public String getTitle2() {
        return mTitle;
    }
    public String getTitle3() {
        return mTitle2;
    }

}
