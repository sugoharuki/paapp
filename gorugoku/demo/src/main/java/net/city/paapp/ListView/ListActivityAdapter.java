package net.city.paapp.ListView;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Point;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import net.city.paapp.R;

import java.util.List;

import static net.city.paapp.Common.Common.mTextSize;

public class ListActivityAdapter extends ArrayAdapter<ListActivityItem> {

    private int mResource;
    private List<ListActivityItem> mItems;
    private LayoutInflater mInflater;

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param resource リソースID
     * @param items リストビューの要素
     */
    public ListActivityAdapter(Context context, int resource, List<ListActivityItem> items) {
        super(context, resource, items);

        mResource = resource;
        mItems = items;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView != null) {
            view = convertView;
        }
        else {
            view = mInflater.inflate(mResource, null);
        }

        // リストビューに表示する要素を取得
        ListActivityItem item = mItems.get(position);

        // サムネイル画像を設定
        ImageView thumbnail = (ImageView)view.findViewById(R.id.thumbnail2);

        WindowManager wm = (WindowManager)view.getContext().getSystemService(Context.WINDOW_SERVICE);
        // Displayのインスタンス取得
        Display disp = wm.getDefaultDisplay();
        Point size = new Point();
        disp.getSize(size);
        ViewGroup.LayoutParams params = thumbnail.getLayoutParams();
        int widthSize = (int)(size.x / 3.5);
        int heightSize = widthSize - (widthSize / 4);
        params.width = widthSize;
        params.height = heightSize;
        thumbnail.setLayoutParams(params);
        Picasso.get().load(item.getThumbnail2()).fit().into(thumbnail);


        Paint paint = new Paint();
        paint.setTextSize(mTextSize);
        Paint.FontMetrics fm = paint.getFontMetrics();
        float textHeight = (float) (Math.abs(fm.top)) + (Math.abs(fm.descent));
        // タイトルを設定
        TextView title = (TextView)view.findViewById(R.id.toukou);
        ViewGroup.LayoutParams textParams = title.getLayoutParams();
        textParams.height = (heightSize / 2) - (int)textHeight;
        title.setLayoutParams(textParams);
        title.setText(item.getTitle2());
        title.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);

        TextView title2 = (TextView)view.findViewById(R.id.naiyou);
        ViewGroup.LayoutParams textParams2 = title2.getLayoutParams();
        textParams2.height = heightSize - (int)(textHeight * 2);
        title2.setLayoutParams(textParams2);
        title2.setText(item.getTitle3());
        title2.setRawInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        title2.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
        return view;
    }
}
