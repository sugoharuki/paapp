package net.city.paapp.chat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.support.v7.app.AppCompatActivity;

import net.city.paapp.R;
import net.city.paapp.async.AsyncHelper;
import net.city.paapp.async.AsyncInterFace;
import net.city.paapp.async.makegroup;
import net.city.paapp.realmModel.allmember;
import net.city.paapp.timeline.commons;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

//import async.getaddmygroup;

public class AddGroupActivity extends AppCompatActivity {

    AlertDialog dia;
    void makeGroup(final String  groupStr,final int flg){
        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {
                dia = new AlertDialog.Builder(ccc)
                        .setTitle("処理中")
                        .setMessage("しばらくお待ちください")
                        .setCancelable(false)
                        .create();
                dia.show();
            }

            @Override
            public String back() {

                //招待制
                String r ="";

                if(flg==0){
                    r = makegroup.douki(commons.username, groupStr, "0");

                    int count = adapter.getCount();
                    for (int i = 0; i < count; i++)
                    {
                        makegroup.shoutai(adapter.getItem(i).getTextData(),groupStr);
                    }
                }else{
                    r = makegroup.douki(commons.username,groupStr,"1");
                }

                if(r.equals("ok")){
                    //backs();
                    return "add";
                }else if(r.equals("doubleerr")){
                    return "in";
                }

                int aa = 0;
                aa++;
                return "ng";
            }

            @Override
            public void end(String end) {
                //dia.dismiss();

                AlertDialog.Builder d = new AlertDialog.Builder(ccc)
                        .setCancelable(false)
                        .setTitle("グループ作成")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dia.dismiss();
                                dialog.dismiss();
                                finish();
                            }
                        });
                if (end.equals("add")) {
                    //変更したら画面に適用するため
                    //tab4Activity.changeFlg = 1;
                    list.setAdapter(adapter);
                    d.setMessage("グループを作成しました");
                    AlertDialog alertDialog = d.create();
                    alertDialog.show();
                } else if (end.equals("in")) {
                    d.setMessage("すでに使用されています");
                    AlertDialog alertDialog = d.create();
                    alertDialog.show();
                } else if (end.equals("ng")) {
                    d.setMessage("エラーです");
                    AlertDialog alertDialog = d.create();
                    alertDialog.show();
                }
            }
        });
    }


    void gonext(String targetAct){
        // インテントのインスタンス生成
        //Intent intent = new Intent(MainActivity.this, ListActivity.class);


        // インテントの生成
        Intent intent = new Intent();
        intent.setClassName("com.sns.smileapp04.posthttp", "com.sns.smileapp04.posthttp."+targetAct);
        // SubActivity の起動
        startActivity(intent);

        /*
        Intent intent = new Intent(MainActivity.this, ListActivity.class);
        // 次画面のアクティビティ起動
        startActivity(intent);
        */
        dia.dismiss();
        finish();
    }

    int finishflg=0;
    void showList(){
        finishflg =0;
        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {
                View   lay = (View)findViewById(R.id.reloads);
                lay.setVisibility(View.VISIBLE);
            }
            @Override
            public String back() {
                //backs();
                return null;
            }
            @Override
            public void end(String result) {
                //ＵＩハンドラにて操作
                finishflg=2;
                list.setAdapter(adapter);
                View   lay = (View)findViewById(R.id.reloads);
                lay.setVisibility(View.GONE);
                //dia.dismiss();
            }
        });
    }

     public static ListView list;
     List<CustomData> objects;
     public static CustomAdapter adapter;
     Context ccc;
     EditText grouptext;
    String prevactivityName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.groupactivity);
        ccc = this;

        //EditText grouptext = (EditText) findViewById(R.id.buytext);
        grouptext = (EditText) findViewById(R.id.buytext);
        grouptext.setInputType(InputType.TYPE_CLASS_TEXT);


          list = (ListView)findViewById(R.id.groupListView);
          objects = new ArrayList<CustomData>();
          adapter = new CustomAdapter(ccc,0,objects);
          showList();

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            //voidだと怒られるのでとりあえずboolean値を返す
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                // 長押しされたアイテムを取得する。
                adapter.remove(adapter.getItem(position));

                return false;
            }
        });

        Button   allbtn = (Button)findViewById(R.id.allbtn);
        allbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 loadList();
            }
        });

        Button   ok = (Button)findViewById(R.id.buybtn);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText str = (EditText) findViewById(R.id.buytext);
                GroupMenberActivity.str = str.getText().toString();
                Intent intent = new Intent(AddGroupActivity.this, GroupMenberActivity.class);
                intent.putExtra("gamenname","AddGroupActivity");

                // 次画面のアクティビティ起動
                startActivity(intent);
            }
        });

        Button  makebtn = (Button)findViewById(R.id.makebtn);
        makebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int cnt = adapter.getCount();
                int cnt2 = membergetcount();

                int grouplength = grouptext.getText().toString().getBytes(Charset.forName("Shift_JIS")).length;

                if(grouplength > 40)
                {
                    new AlertDialog.Builder(ccc)
                            .setTitle("エラー")
                            .setMessage("グループ名の文字数制限を越えています")
                            .setPositiveButton("OK", null)
                            .show();

                    return;
                }

                if(grouptext.getText().toString().equals(""))
                {
                    new AlertDialog.Builder(ccc)
                            .setTitle("エラー")
                            .setMessage("グループ名が未入力です")
                            .setPositiveButton("OK", null)
                            .show();

                    return;
                }

                EditText grouptext = (EditText) findViewById(R.id.buytext);
                final String groupStr = grouptext.getText().toString();
                grouptext.setText("");

                //全公開
                if(cnt == cnt2) makeGroup(groupStr,1);
                //非公開
                else makeGroup(groupStr,0);

                //v.setVisibility(View.VISIBLE);

                //finish();
            }
        });
    }

    private void loadList(){
        objects.clear();

        Realm.init(ccc);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<allmember> memberlist;
        try{
            memberlist = realm.where(allmember.class).findAll();
            for(allmember mem : memberlist)
            {
                CustomData obj = new CustomData();
                obj.setTextData(mem.name);
                obj.setTextData2(mem.syain);
                obj.setTextData3("");
                objects.add(obj);
            }
        }
        finally{
            realm.close();
        }
        list.setAdapter(adapter);
    }

    private int membergetcount(){
        int cnt = 0;
        Realm.init(ccc);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<allmember> memberlist;
        try{
            memberlist = realm.where(allmember.class).findAll();
            cnt = memberlist.size();
        }
        finally{
            realm.close();
        }
        return  cnt;
    }

}
