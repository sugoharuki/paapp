package net.city.paapp.timeline;

import android.content.Context;
import android.content.SharedPreferences;


public class midokupri {

    public static  void save(Context con ,int num){
        // プリファレンスの準備 //
        SharedPreferences pref = con.getSharedPreferences( "midokupri", Context.MODE_PRIVATE );
        // プリファレンスに書き込むためのEditorオブジェクト取得 //
        SharedPreferences.Editor editor = pref.edit();
        // "user_name" というキーで名前を登録
        //editor.putString( "nums", num );
        editor.putInt("nums",num);
        // 書き込みの確定（実際にファイルに書き込む）
        editor.commit();
    }

    public static int load(Context con){
        SharedPreferences pref = con.getSharedPreferences( "midokupri", Context.MODE_PRIVATE );
        //return pref.getString("nums",0);
        return pref.getInt("nums",0);
    }

}
