package net.city.paapp.timeline;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import net.city.paapp.async.AsyncHelper;
import net.city.paapp.Model.timeline;
import net.city.paapp.R;
import net.city.paapp.async.AsyncInterFace2;
import net.city.paapp.interfaces.timelineInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class timelineActivity extends Activity {


    @Override
    protected void onResume() {
        super.onResume();
        loadlist(ccc,list,ada,objects);
    }

    @Override
    protected  void onPause(){
        super.onPause();
        try{
        timelineActivity.position = list.getFirstVisiblePosition();
        yOffset = list.getChildAt(0).getTop();
        }catch (Exception e)
        {

        }
    }

    public static List<timeline> objects;
    public static TimeLineCustomAdapter ada;
    public static ListView list;

    public static int scroolpos = 0;

    public static int position = 0;
    public static int yOffset = 0;

    public static  void loadlist(final Context ccc,final ListView lists,final  TimeLineCustomAdapter adas,final List<timeline> objectss ){

        final AlertDialog dia =new AlertDialog.Builder(ccc)
                .setTitle("読み込み中")
                .setMessage("しばらくお待ち下さい")
                .setCancelable(false)
                .create();

        AsyncHelper.Async4(new AsyncInterFace2() {
            @Override
            public void start() {
            }

            @Override
            public List<timeline> back() {
                try {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://smileapp.yokohama/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    timelineInterface API = retrofit.create(timelineInterface.class);
                    return API.gettimeline().execute().body();
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public void end(List<timeline> result) {
                int a=0;
                a++;
                objectss.clear();
                try{
                    midokupri.save(ccc,Integer.parseInt(result.get(0).id));

                    for (int i=0; i<result.size(); i++){
                        objectss.add(result.get(i));
                        int aaa=0;
                        aaa++;
                    }
                    lists.setAdapter(adas);
                    //lists.setSelection(scroolpos);
                    lists.setSelectionFromTop(position, yOffset);

                    int aa=0;
                    aa++;
                }catch (Exception e){
                    int aa=0;
                    aa++;
                }
            }
        }).execute();
    }

    public static Context ccc;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ccc = this;
        setContentView(R.layout.timelineact);
        list = (ListView)findViewById(R.id.タイムラインリスト);
        objects = new ArrayList<timeline>();
        ada = new TimeLineCustomAdapter(this,0,objects);
        ada.setData(objects,list,ada);
    }

}