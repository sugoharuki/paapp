package net.city.paapp.async;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.city.paapp.chat.BitmapHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UploadAsyncTask2 {
    //共有防止？
    public static  AsyncTask<Void, Void, String> Upload(final Activity act,final Uri uri,final Context context,final String path,final uploadInterFace v){
        final ProgressDialog dialog;
        dialog = new ProgressDialog(context);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setTitle("送信中");
        dialog.setCancelable(false);
        dialog.setMessage("少々お待ち下さい。");
        dialog.show();
        //ブロッキングといえど非同期を使う

        return  new AsyncTask<Void, Void, String>() {
            //uiスレッドにて最初にやる処理
            @Override
            protected void onPreExecute()
            {
                //v.start();
            }
            //バック完了後にuiスレッドにて最後にやる処理

            @Override
            protected void onPostExecute(String result) {
                if(dialog != null){ dialog.dismiss(); }

                v.end(result);
            }

            @Override
            protected String doInBackground(Void... params) {
                try {
                    try {


                        String strBase64 = "";
                        try {
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            Bitmap bmp = BitmapHelper.getBitmapFromUri(act, uri);
                            bmp = BitmapHelper.makeBmp(path,bmp);

                            int h = bmp.getHeight();
                            int w = bmp.getWidth();


                            float www = 400.0f / bmp.getHeight() * bmp.getWidth();
                            float ws = Math.round(www);
                            int ww = (int) ws;
                            bmp = Bitmap.createScaledBitmap(bmp, ww, 400, false);


                            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            byte[] bytes = stream.toByteArray();
                            strBase64 = Base64.encodeToString(bytes, Base64.NO_WRAP);
                        } catch (Exception o) {

                        }

                        //String fileName = params[0];
                        // Log.d("hogename",fileName);
                        File file = new File(path);
                        Gson gson = new GsonBuilder()
                                .setLenient()
                                .create();
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl("http://smileapp.yokohama/")
                                .addConverterFactory(GsonConverterFactory.create(gson))
                                .build();
                        imgsendInterface API = retrofit.create(imgsendInterface.class);
                        String r = API.imgSendApi(strBase64,file.getName()).execute().body();
                        return file.getName();

                    }
                    catch (IOException e) {
                        e.printStackTrace();
                        return "ng";
                    }
                    //return v.back();
                } catch (Exception e) {
                    return "ng";
                }
            }
        };
    }
}


