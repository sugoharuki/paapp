package net.city.paapp.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;



public class ImageAsyncTask2 extends AsyncTask<String, Integer, Bitmap> {

    ProgressDialog dialog;
    Context context;
    String ReceiveStr;
    inter act;

    public ImageAsyncTask2(Context context, inter a){
        this.context = context;
        act = a;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return act.act();
    }

    @Override
    protected void onPostExecute(Bitmap result) {

        act.end(result);

        //img.setImageBitmap(result);

        //saveImage(img);
       // if(dialog != null){ dialog.dismiss(); }
        // サーバ側phpでechoした内容を表示
        //Toast.makeText(context, ReceiveStr, Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onPreExecute() {

        act.first();
        /*
        dialog = new ProgressDialog(context);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setTitle("Please wait");
        dialog.setMessage("Uploading...");
        dialog.show();
        */

    }
}


