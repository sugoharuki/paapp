package net.city.paapp.async;

import net.city.paapp.Model.TimeLineComment;

import java.util.List;


public interface AsyncInterFace6 {
    public void start();
    public List<TimeLineComment> back();
    public void end(List<TimeLineComment> result);
}
