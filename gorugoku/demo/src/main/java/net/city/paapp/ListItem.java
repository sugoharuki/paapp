package net.city.paapp;

import android.graphics.Bitmap;

public class ListItem {
    private String mTitle = null;

    /**
     * 空のコンストラクタ
     */
    public ListItem() {}

    /**
     * コンストラクタ
     * @param title タイトル
     */
    public ListItem(String title) {
        mTitle = title;
    }

    /**
     * サムネイル画像を設定
     * @param thumbnail サムネイル画像
     */

    /**
     * タイトルを設定
     * @param title タイトル
     */
    public void setmTitle(String title) {
        mTitle = title;
    }

    /**
     * サムネイル画像を取得
     * @return サムネイル画像
     */

    /**
     * タイトルを取得
     * @return タイトル
     */
    public String getTitle() {
        return mTitle;
    }
}
