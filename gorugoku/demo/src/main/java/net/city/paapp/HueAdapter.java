package net.city.paapp;

import android.content.Context;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Display;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import net.city.paapp.Common.Common;

import com.squareup.picasso.Picasso;

public class HueAdapter extends BaseAdapter {
    private LayoutInflater mLayoutInflater;
    private String mHueIdArray[] = new String[(Common.videoModel.size()) - 1];
    private static class ViewHolder {
        public ImageView hueImageView;
    }

    public HueAdapter(Context context) {
        mLayoutInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return mHueIdArray.length;
    }

    public Object getItem(int position) {
        return mHueIdArray[position];
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.grid_item_hue, null);
            holder = new ViewHolder();
            holder.hueImageView = (ImageView)convertView.findViewById(R.id.hue_imageview);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        WindowManager wm = (WindowManager)convertView.getContext().getSystemService(Context.WINDOW_SERVICE);
        // Displayのインスタンス取得
        Display disp = wm.getDefaultDisplay();
        Point size = new Point();
        disp.getSize(size);
        int halfsize = size.x / 2;
        ViewGroup.LayoutParams params = holder.hueImageView.getLayoutParams();
        params.height = halfsize - (halfsize /4);
        holder.hueImageView.setLayoutParams(params);
        //String mHueIdArray[] = new String[(Common.lessonmodel.body.total - 1)];
        mHueIdArray = new String[(Common.videoModel.size()) - 1];

        for(int i = 1 ; i < Common.videoModel.size(); i++){
            mHueIdArray[i - 1] = Common.videoModel.get(i).url;
        }


        Picasso.get()
                .load(mHueIdArray[position])
                .fit()
                .into((ImageView) holder.hueImageView);
        return convertView;
    }
}