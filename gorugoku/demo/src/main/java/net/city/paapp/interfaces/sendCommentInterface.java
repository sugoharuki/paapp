package net.city.paapp.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface sendCommentInterface {
    @FormUrlEncoded
    @POST("/sendcomment.php")
    Call<Void> sendCommentapi(@Field("sendname") String sendname, @Field("ids") String ids, @Field("msg") String msg);
}
