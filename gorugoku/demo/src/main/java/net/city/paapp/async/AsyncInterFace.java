package net.city.paapp.async;


// イベントクラスの定義
public interface AsyncInterFace {

    public void start();
    public String back();
    public void end(String result);

}
