package net.city.paapp.Survey;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;


import net.city.paapp.Model.answer;
import net.city.paapp.Model.data;
import net.city.paapp.R;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class testAdapter extends ArrayAdapter<data> {
    private LayoutInflater layoutInflater_;
    //Bitmap oBmp;
    Context ccc;

    private boolean checkflg = false;
    public testAdapter(Context context, int textViewResourceId, List<data> objects) {
        super(context, textViewResourceId, objects);
        ccc = context;
        layoutInflater_ = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    List<data> objects;
    ListView list;
    testAdapter ada;
    public void setData(List<data> objects, ListView list,testAdapter ada) {
        this.list = list;
        this.objects = objects;
        this.ada = ada;
    }


    public Object fetch(String address) throws MalformedURLException,IOException {
        URL url = new URL(address);
        Object content = url.getContent();
        return content;
    }

    private int flg=0;
    private int cnt=0;

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // 特定の行(position)のデータを得る
        final data item = (data)getItem(position);
        // convertViewは使い回しされている可能性があるのでnullの時だけ新しく作る
        //if(null != convertView)
        //{
        //    convertView = null;
        //}
        final View convView = convertView;//動的に背景色を変更するのに使用
        if (null == convertView) {
            convertView = layoutInflater_.inflate(R.layout.sample_layout, null);
        }
        final int nowposi = position;
        final TextView subTitle= (TextView)convertView.findViewById(R.id.subTitle);
        subTitle.setText(item.subtitle);
        final TextView subTitleNo= (TextView)convertView.findViewById(R.id.subTitleNo);
        if(SurveyActivity.jotaiChecked && getItem(position).jotai == "0")
            convertView.setBackgroundColor(Color.RED);
        else
            convertView.setBackgroundColor(Color.WHITE);
        subTitleNo.setText("No" +(position + 1) + " ");
        if(item.type == "1") {//チェックボックス
            final List<CheckBox> check = new ArrayList<CheckBox>();
            check.add((CheckBox)convertView.findViewById(R.id.checkbox1));
            check.add((CheckBox)convertView.findViewById(R.id.checkbox2));
            check.add((CheckBox)convertView.findViewById(R.id.checkbox3));
            check.add((CheckBox)convertView.findViewById(R.id.checkbox4));
            check.add((CheckBox)convertView.findViewById(R.id.checkbox5));
            check.add((CheckBox)convertView.findViewById(R.id.checkbox6));
            convertView.findViewById(R.id.checklist).setVisibility(View.VISIBLE);
            convertView.findViewById(R.id.radiolist).setVisibility(View.GONE);
            int i = 0;
            for(answer ans : item.answer)
            {
                final int nowi = i;
                if(getItem(position).answer.get(i).checked == "1")
                    check.get(i).setChecked(true);
                else
                    check.get(i).setChecked(false);

                check.get(i).setVisibility(View.VISIBLE);
                check.get(i).setText(ans.naiyo);
                check.get(i).setTag("check" + position + "_"+i);
                final int posi = position;
                final int posi2 = i;
                check.get(i).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if(b) {
                            getItem(posi).answer.get(posi2).checked = "1";
                            //チェックが入った場合とりあえず全体チェックを入れておく
                            getItem(posi).jotai = "1";
                            if(SurveyActivity.jotaiChecked)
                                convView.setBackgroundColor(Color.WHITE);
                        }
                        else {
                            getItem(posi).answer.get(posi2).checked = "0";
                            //チェックが外れた場合全体を確認後すべて0の場合jotaiを0に変更
                            boolean flg = false;
                            for(int y = 0;y <getItem(posi).answer.size();y++)
                            {
                                if(getItem(posi).answer.get(y).checked == "1") {
                                    flg = true;
                                    break;
                                }
                            }
                            if(!flg){//全て0のためjotaiを0に変更
                                getItem(posi).jotai = "0";
                            }
                        }
                    }
                });
                i++;
            }
        }
        if(item.type == "2") {//ラジオボタン
            final List<RadioButton> radio = new ArrayList<RadioButton>();
            radio.add((RadioButton)convertView.findViewById(R.id.radio1));
            radio.add((RadioButton)convertView.findViewById(R.id.radio2));
            radio.add((RadioButton)convertView.findViewById(R.id.radio3));
            radio.add((RadioButton)convertView.findViewById(R.id.radio4));
            radio.add((RadioButton)convertView.findViewById(R.id.radio5));
            radio.add((RadioButton)convertView.findViewById(R.id.radio6));
            convertView.findViewById(R.id.radiolist).setVisibility(View.VISIBLE);
            convertView.findViewById(R.id.RadioGroup).setVisibility(View.VISIBLE);
            convertView.findViewById(R.id.checklist).setVisibility(View.GONE);
            int i = 0;
            for(answer ans : item.answer)
            {
                if(getItem(position).answer.get(i).checked == "1")
                    radio.get(i).setChecked(true);
                else
                    radio.get(i).setChecked(false);
                radio.get(i).setVisibility(View.VISIBLE);
                radio.get(i).setText(ans.naiyo);
                radio.get(i).setTag("radio"+ position + "_" + i);
                final int posi = position;
                final int posi2 = i;
                radio.get(i).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if(b) {
                            getItem(posi).answer.get(posi2).checked = "1";
                            //ラジオボタンは消すことができないのでチェック状態になった場合全体チェック状態をtrueに変更
                            getItem(posi).jotai = "1";
                            if(SurveyActivity.jotaiChecked)
                                convView.setBackgroundColor(Color.WHITE);
                        }
                        else
                            getItem(posi).answer.get(posi2).checked = "0";
                    }
                });
                i++;
            }
        }

        return convertView;
    }
}

