package net.city.paapp.interfaces;

import net.city.paapp.Model.GroupMenber;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface getGroupMenberInterface {
    @FormUrlEncoded
    @POST("/getgroupmembersa.php")
    Call<List<GroupMenber>> getMenberapi(@Field("groupname") String groupname);
}
