package net.city.paapp.async;

import net.city.paapp.Model.KojinChat;

import java.util.List;


public interface AsyncInterFace4 {
    public void start();
    public List<KojinChat> back();
    public void end(List<KojinChat> result);
}
