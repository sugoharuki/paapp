package net.city.paapp.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface makeGroupInterface {
    @FormUrlEncoded
    @POST("/makegroup.php")
    Call<String> makeGroupapi(@Field("targetname") String targetname, @Field("groupname") String groupname, @Field("allsendflg") String allsendflg);
}
