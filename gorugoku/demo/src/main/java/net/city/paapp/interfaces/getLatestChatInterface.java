package net.city.paapp.interfaces;

import net.city.paapp.Model.KojinChat;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface getLatestChatInterface {
    @FormUrlEncoded
    @POST("/getsaisinchat.php")
    Call<List<KojinChat>> getLatestChatapi(@Field("jsons") String jsons);
}
