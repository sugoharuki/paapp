package net.city.paapp.timeline;

import android.graphics.Bitmap;

public class TimeLineCustomData {

 private String iine;
 private String name;
 private String body;
 private String times;
 private String flg;
 private String id;
 private String imagepath;
 private String iconpath;
 private Bitmap img;

 public void setid(String text) {
  id = text;
 }
 public String getid() {
  return id;
 }


 public void setiine(String text) {
  iine = text;
 }
 public String getiine() {
  return iine;
 }



 public void seticonpath(String text) {
  iconpath = text;
 }
 public String geticonpath() {
  return iconpath;
 }

 public void setImagaData(Bitmap image) {
  img = image;
 }
 public Bitmap getImageData() {
  return img;
 }



 public void setname(String text) {
  name = text;
 }
 public String getname() {
  return name;
 }


 public void setbody(String text) {
  body = text;
 }
 public String getbody() {
  return body;
 }


 public void setflg(String text) {
  flg = text;
 }
 public String getflg() {
  return flg;
 }

 public void settime(String text) {
  times = text;
 }
 public String gettime() {
  return times;
 }

 public void setimagepath(String text) {
  imagepath = text;
 }
 public String getimagepath() {
  return imagepath;
 }

}