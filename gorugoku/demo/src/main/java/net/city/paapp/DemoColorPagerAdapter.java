package net.city.paapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import net.city.paapp.Calendar.CalendarLayout;
import net.city.paapp.Calendar.CalendarView;

import net.city.paapp.Common.Common;
import net.city.paapp.Model.KojinChat;
import net.city.paapp.Model.SurveyTitleModel;
import net.city.paapp.Model.BroadCastModel;
import net.city.paapp.Model.answerModel;
import net.city.paapp.Model.contentModel;
import net.city.paapp.Model.shiftModel;
import net.city.paapp.Model.timeline;
import net.city.paapp.Survey.SurveyActivity;
import net.city.paapp.WebView.WebActivity;
import net.city.paapp.async.AsyncInterFace2;
import net.city.paapp.chat.CustomAdapter;
import net.city.paapp.chat.CustomData;
import net.city.paapp.chat.DtlActivity;
import net.city.paapp.chat.GetMidoku;
import net.city.paapp.chat.groupChatActivity;
import net.city.paapp.colorful.ColorfulActivity;
import net.city.paapp.interfaces.ResultInterface2;
import net.city.paapp.interfaces.ShiftApi;
import net.city.paapp.interfaces.answerApi;
import net.city.paapp.interfaces.contentApi;
import net.city.paapp.interfaces.timelineInterface;
import net.city.paapp.multi.MultiActivity;
import net.city.paapp.realmModel.midoku;
import net.city.paapp.realmModel.rireki;
import net.city.paapp.timeline.TimeLineCustomAdapter;
import net.city.paapp.timeline.commons;
import net.city.paapp.timeline.midokupri;

import android.widget.RelativeLayout;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static net.city.paapp.timeline.timelineActivity.ada;
import static net.city.paapp.timeline.timelineActivity.objects;
import static net.city.paapp.timeline.timelineActivity.yOffset;

public class DemoColorPagerAdapter extends PagerAdapter {

    private List<ColorItem> mItems = new ArrayList<>();

    public static View Page1;

    public static View Page2;

    public static View Page3;

    public static View Page4;

    public static View Page5;

    public static View Page6;

    //public static View Page7;

    public DemoColorPagerAdapter() {
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        if(position==0){
            return CreatePage1(container);
        }
        else if (position == 1) {
            return CreatePage2(container);
        }
        else if(position == 2){
            return CreatePage3(container);
        }
        else if(position == 3){
            return CreatePage4(container);
        }
        else if(position == 4){
            return CreatePage5(container);
        }
        else if(position == 5){
            return CreatePage6(container);
        }
        //else if(position == 6){
        //    return CreatePage7(container);
        //}
        else{
            return null;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public String getPageTitle(int position) {
        return mItems.get(position).name;
    }


    public void addAll(List<ColorItem> items) {
        mItems = new ArrayList<>(items);
    }
    //マニュアル動画
    public Object CreatePage1(ViewGroup container){

        if(Page1 != null)
            return Page1;

        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.layout_page, container, false);
        ListView listView = view.findViewById(R.id.basic_list);
        ArrayList<ListItem> listItems = new ArrayList<>();
        if(Common.videoModel == null)
        {
            return view;
        }
        for (int i = 0; i < Common.videoModel.size(); i++) {
            ListItem item = new ListItem(Common.videoModel.get(i).title);
            listItems.add(item);
        }
        ListAdapter adapter = new ListAdapter(container.getContext(), R.layout.list_item, listItems);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), VideoPlayerActivity.class);
                intent.putExtra("Position2",position);
                view.getContext().startActivity(intent);
            }
        });
        listView.setAdapter(adapter);
        container.addView(view);
        Page1 = view;
        return view;
    }


    TextView mTextMonthDay;

    TextView mTextYear;

    TextView mTextLunar;

    TextView mTextCurrentDay;

    CalendarView mCalendarView;

    RelativeLayout mRelativeTool;
    private int mYear;
    CalendarLayout mCalendarLayout;

    //スタッフスイング
    public Object CreatePage2(ViewGroup container) {
        if(Page2 != null)
            return Page2;

        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.layout_page, container, false);
        ListView listView = view.findViewById(R.id.basic_list);
        ArrayList<ListItem> listItems = new ArrayList<>();



        ListItem item = new ListItem("シフト確認");
        listItems.add(item);
        item = new ListItem("シフト希望入力");
        listItems.add(item);
        ListAdapter adapter = new ListAdapter(container.getContext(), R.layout.list_item, listItems);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if(position == 0)
                        ColorfulActivity.show(view.getContext());
                    else if(position == 1)
                        MultiActivity.show(view.getContext());
                }
                catch(Exception e) {
                     int i = 0;
                }
            }
        });
        listView.setAdapter(adapter);
        container.addView(view);
        /*
        ListView listView = view.findViewById(R.id.basic_list);
        ArrayList<ListItem> listItems = new ArrayList<>();
        if(Common.staffmodel == null)
        {
            return view;
        }
        for (int i = 0; i < Common.staffmodel.body.total; i++) {
            String path = Common.staffmodel.body.data.get(i).pictures.sizes.get(3).link;
            ListItem item = new ListItem(path, Common.staffmodel.body.data.get(i).name);
            listItems.add(item);
        }
        ListAdapter adapter = new ListAdapter(container.getContext(), R.layout.list_item, listItems);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), VideoPlayerActivity.class);
                intent.putExtra("Position2",position);
                view.getContext().startActivity(intent);
            }
        });
        listView.setAdapter(adapter);
        container.addView(view);
        */
        Page2 = view;
        return view;
    }

    //アンケートリスト
    public Object CreatePage3(ViewGroup container){
        if(Page3 != null)
            return Page3;
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.layout_page, container, false);
        //survey id に紐づく survey Titleを選択
        List<SurveyTitleModel> titles = new ArrayList<>();
        for(SurveyTitleModel title :Common.surveyTitleModel)
        {
            for(BroadCastModel id :Common.broadCastModel)
            {
                if(id.broadcast_id == title.id)
                {
                    titles.add(title);
                }
            }
        }

        ListView listView = view.findViewById(R.id.basic_list);
        ArrayList<ListItem> listItems = new ArrayList<>();
        for (int i = 0; i < titles.size(); i++) {
            String path = titles.get(i).TITLE;
            ListItem item = new ListItem(titles.get(i).TITLE);
            listItems.add(item);
        }

        ListAdapter adapter = new ListAdapter(container.getContext(), R.layout.list_item, listItems);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //データ取得を行う?
                ProgressDialog progressDialog = new ProgressDialog(view.getContext());
                progressDialog.setTitle("データ取得中");
                progressDialog.setMessage("データ取得中です");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                CreateModel(progressDialog,view,Common.surveyTitleModel.get(position).TITLE,Common.surveyTitleModel.get(position).id);
                    //CreateWeb(Common.snsmodel.body.data.get(position).description,view);
            }
        });
        listView.setAdapter(adapter);
        container.addView(view);

        Page3 = view;
        return view;
    }
    ProgressDialog progDialog;
    boolean errorFlag= false;
    View mView;
    String mtitle;
    int mId;
    private void CreateModel(ProgressDialog progressDialog,View view,String title,int id){
        progDialog = progressDialog;
        mView = view;
        mtitle = title;
        mId = id;
        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {

            }

            @Override
            public String back() {
                try {
                    Common.ContentModel = CreateContentModel("http://220.247.10.217/getcontents.php/",1);
                    Common.AnswerModel = CreateAnswerModel("http://220.247.10.217/getanswers.php/");
                } catch (IOException e) {
                    errorFlag=true;
                }
                return null;
            }

            @Override
            public void end(String result) {
                if(errorFlag) {
                    progDialog.dismiss();
                    //goerror();
                }
                else {
                    progDialog.dismiss();
                    CreateSurvey(mtitle,mView,mId);
                }
            }
        });
    }
    private List<answerModel> CreateAnswerModel(String Path) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        answerApi API = retrofit.create(answerApi.class);
        return  API.getAnswerModel().execute().body();
    }
    private List<contentModel> CreateContentModel(String Path, int surveyid) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        contentApi API = retrofit.create(contentApi.class);
        return  API.getContents(surveyid).execute().body();
    }

    private void CreateSurvey(String title,View view,int id) {
        Intent intent = new Intent(view.getContext(), SurveyActivity.class);
        intent.putExtra("title",title);
        intent.putExtra("id",id);
        view.getContext().startActivity(intent);
    }

    private void CreateWeb(String Path,View view) {
        Path = Path.replace("{$data$:$","");
        Path = Path.replace("$}","");
        Intent intent = new Intent(view.getContext(), WebActivity.class);
        intent.putExtra("Path",Path);
        view.getContext().startActivity(intent);
    }

    /*public Object CreatePage4(ViewGroup container){
        if(Page4 != null)
            return Page4;

        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.layout_web, container, false);
        container.addView(view);
        WebView www = view.findViewById(R.id.webb1);

        //リンクをタップしたときに標準ブラウザを起動させない
        www.setWebViewClient(new WebViewClient());
        // Web Storage を有効化
        www.getSettings().setBuiltInZoomControls(true);
        www.loadUrl("https://reserva.be/teamminamisawa");
        Page4 = view;
        return view;
    }*/
    public static ListView timelinelist;
    public Object CreatePage4(ViewGroup container){
        if(Page4 != null)
            return Page4;

        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.timelineact, container, false);
        container.addView(view);
        ListView listView = view.findViewById(R.id.タイムラインリスト);

        objects = new ArrayList<timeline>();
        ada = new TimeLineCustomAdapter(container.getContext(),0,objects);
        ada.setData(objects,listView,ada);

        loadlist(container.getContext(),listView,ada,objects);
        /*WebView www = view.findViewById(R.id.webb2);
        container.addView(view);
        www.setWebViewClient(new WebViewClient());
        www.getSettings().setBuiltInZoomControls(true);
        www.getSettings().setJavaScriptEnabled(true);
        www.loadUrl("https://shop.taylormadegolf.jp");*/
        Page4 = view;
        return view;
    }
    public static int position = 0;
    public static  void loadlist(final Context ccc,final ListView lists,final  TimeLineCustomAdapter adas,final List<timeline> objectss ){

        final AlertDialog dia =new AlertDialog.Builder(ccc)
                .setTitle("読み込み中")
                .setMessage("しばらくお待ち下さい")
                .setCancelable(false)
                .create();

        net.city.paapp.async.AsyncHelper.Async4(new AsyncInterFace2() {
            @Override
            public void start() {
            }

            @Override
            public List<timeline> back() {
                try {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://smileapp.yokohama/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    timelineInterface API = retrofit.create(timelineInterface.class);
                    return API.gettimeline().execute().body();
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public void end(List<timeline> result) {
                int a=0;
                a++;
                objectss.clear();
                try{
                    midokupri.save(ccc,Integer.parseInt(result.get(0).id));

                    for (int i=0; i<result.size(); i++){
                        objectss.add(result.get(i));
                        int aaa=0;
                        aaa++;
                    }
                    lists.setAdapter(adas);
                    //lists.setSelection(scroolpos);
                    lists.setSelectionFromTop(position, yOffset);

                    int aa=0;
                    aa++;
                }catch (Exception e){
                    int aa=0;
                    aa++;
                }
            }
        }).execute();
    }

    public Object CreatePage5(ViewGroup container){
        if(Page5 != null)
            return Page5;

        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.chat_list, container, false);
        container.addView(view);
        list = (ListView)view.findViewById(R.id.chatLists);
        ccc = container.getContext();
        objects2 = new ArrayList<CustomData>();
        adapter = new CustomAdapter(ccc,0,objects2);

        findtext = (TextView)view.findViewById(R.id.chatfindText);
        findtext.setInputType(InputType.TYPE_CLASS_TEXT);

        final Button findbtn = (Button)view.findViewById(R.id.chatfindbtn);
        findbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadList2(ccc);
            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                CustomAdapter ada = (CustomAdapter) list.getAdapter();
                CustomData aaa = ada.getItem(position);
                final String name = aaa.getTextData();

                new AlertDialog.Builder(ccc)
                        .setTitle("項目を削除しますか？")
                        .setMessage("よければOKボタンを押してください")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Realm.init(ccc);
                                Realm realm = Realm.getDefaultInstance();
                                rireki mem;
                                try {
                                    mem = realm.where(rireki.class).equalTo("rirekiname",name).findFirst();
                                    if(mem != null) {
                                        realm.beginTransaction();
                                        mem.deleteFromRealm();
                                        realm.commitTransaction();
                                    }
                                }
                                finally{
                                    realm.close();
                                }

                                loadList2(ccc);
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .show();
                return true;
            }
        });


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                ListView list = (ListView) adapter;
                CustomAdapter ada = (CustomAdapter) list.getAdapter();
                CustomData aaa = ada.getItem(position);

                final String rirekiname = aaa.getTextData();
                final String sendtype = aaa.getTextData2();

                long currentTimeMillis = System.currentTimeMillis();
                String times = String.valueOf(currentTimeMillis);
                Realm.init(ccc);
                Realm realm = Realm.getDefaultInstance();
                rireki mem = new rireki();
                mem.rirekiname = rirekiname;
                mem.sendtype = sendtype;
                mem.times = times;
                try {
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(mem);
                    realm.commitTransaction();
                }catch(Exception e)
                {
                    int Test = 0;
                }
                finally{
                    realm.close();
                }

                if (!sendtype.equals("kojin")) {
                    groupChatActivity.targetgroupname = rirekiname;
                    Intent intent = new Intent(ccc, groupChatActivity.class);
                    //startActivity(intent);

                } else {
                    DtlActivity.targetDisplayName = rirekiname;
                    Intent intent = new Intent(ccc, DtlActivity.class);
                    //startActivity(intent);
                }
            }
        });


        loadList2(ccc);
        /*
        ListView listView = view.findViewById(R.id.basic_list);
        ArrayList<ListItem> listItems = new ArrayList<>();
        for (int i = 0; i < Common.specialmodel.body.total; i++) {
            String path = Common.specialmodel.body.data.get(i).pictures.sizes.get(1).link;
            ListItem item = new ListItem(path, Common.specialmodel.body.data.get(i).name);
            listItems.add(item);
        }
        ListAdapter adapter = new ListAdapter(container.getContext(), R.layout.list_item, listItems);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CreateWeb(Common.specialmodel.body.data.get(position).description,view);
            }
        });

        listView.setAdapter(adapter);
        container.addView(view);
        */
        Page5 = view;
        return view;
        /*
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.layout_web3, container, false);
        WebView www = view.findViewById(R.id.webb3);
        container.addView(view);
        //リンクをタップしたときに標準ブラウザを起動させない
        www.setWebViewClient(new WebViewClient());
        www.getSettings().setJavaScriptEnabled(true);
        // Web Storage を有効化
        www.loadUrl("http://www.kingf-gc.jp");

        Page5 = view;
        return view;
        */
    }
    public static Context ccc;
    public static String jsons = "";
    public static TextView findtext;
    public static List<CustomData> objects2;
    public static ListView list;
    public static   CustomAdapter adapter;
    public static  void  loadList2(final Context ccc){
        objects2.clear();

        ArrayList<String> select = new ArrayList<String>();
        select.add("rirekiname");
        select.add("sendtype");
        select.add("times");
        String where ="id > ?";
        String order ="times DESC";
        ArrayList<String> bind = new ArrayList<String>();
        bind.add("-1");

        jsons = "[";

        Realm.init(ccc);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<rireki> memlist;
        try{
            memlist = realm.where(rireki.class).findAll().sort("times", Sort.DESCENDING);

            int i = 1;
            for(rireki mem : memlist) {
                if(mem.rirekiname == null)
                    continue;
                if (!mem.rirekiname.contains(findtext.getText())) return;
                final CustomData obj = new CustomData();
                obj.setTextData(mem.rirekiname);
                obj.setTextData2(mem.sendtype);

                if(mem.sendtype.equals("kojin")){
                    jsons+="{\"toname\":\""+ commons.username+"\",\"fromname\":\""+mem.rirekiname+"\",\"flg\":\"\"}";
                }else{
                    jsons+="{\"toname\":\"\",\"fromname\":\""+mem.rirekiname+"\",\"flg\":\""+mem.rirekiname+"\"}";
                }
                if(memlist.size() != i){
                    jsons+=",";
                }
                i++;
                //ここでディクショナリにあれば赤くする
                objects2.add(obj);
            }
        }
        finally{
            realm.close();
        }
        jsons += "]";

        GetMidoku.set(jsons, new ResultInterface2() {
            @Override
            public void result(List<KojinChat> r) {
                try{
                    for (int i=0; i<r.size(); i++){
                        final String flgs = r.get(i).flg;
                        final String id = r.get(i).id;
                        String toname = r.get(i).toname;
                        final String fromname = r.get(i).fromname;

                        ArrayList<String> select = new ArrayList<String>();
                        select.add("targetname");
                        select.add("flg");
                        select.add("ids");
                        String where ="";
                        String order ="id";
                        ArrayList<String> bind = new ArrayList<String>();

                        if(flgs.equals("")){
                            where ="targetname = ?";
                            bind.add(fromname);
                        }else{
                            where ="flg = ?";
                            bind.add(flgs);
                        }
                        //sugo Realmをテスト
                        Realm.init(ccc);
                        Realm realm = Realm.getDefaultInstance();
                        midoku mymidoku;
                        try
                        {
                            if(flgs.equals("")){
                                mymidoku = realm.where(midoku.class).equalTo("targetname",fromname).findAllSorted("ids").last();
                            }else{
                                mymidoku = realm.where(midoku.class).equalTo("flg",flgs).findAllSorted("ids").last();
                            }
                            if(mymidoku!=null)
                            {
                                if(mymidoku.flg.equals("")){
                                    if(mymidoku.targetname.equals(fromname))
                                    {
                                        if(Integer.parseInt(id)>Integer.parseInt(mymidoku.ids))
                                        {
                                            for(CustomData obj:objects2)
                                            {
                                                if(obj.getTextData().equals(fromname)){
                                                    obj.setcolorData(Color.RED);
                                                }
                                            }
                                        }
                                    }
                                }
                                else{
                                    if(mymidoku.flg.equals(flgs)){
                                        if(!fromname.equals(commons.username)){
                                            if(Integer.parseInt(id)>Integer.parseInt(mymidoku.ids)){
                                                for (CustomData obj:objects2) {
                                                    if(obj.getTextData().equals(mymidoku.flg)){
                                                        obj.setcolorData(Color.RED);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        finally
                        {
                            if(realm != null)
                                realm.close();
                        }

                        int aaa=0;
                        aaa++;
                    }
                    int aa=0;
                    aa++;
                }catch (Exception e){
                    int aa=0;
                    aa++;
                }
                adapter = new CustomAdapter(ccc,0,objects2);
                list.setAdapter(adapter);
            }
        });
    }

    public Object CreatePage6(ViewGroup container){
        if(Page6 != null)
            return Page6;

        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.layout_page, container, false);
        /*
        ListView listView = view.findViewById(R.id.basic_list);
        ArrayList<ListItem> listItems = new ArrayList<>();
        for (int i = 0; i < Common.linksmodel.body.total; i++) {
            String path = Common.linksmodel.body.data.get(i).pictures.sizes.get(1).link;
            ListItem item = new ListItem(path, Common.linksmodel.body.data.get(i).name);
            listItems.add(item);
        }
        ListAdapter adapter = new ListAdapter(container.getContext(), R.layout.list_item, listItems);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CreateWeb(Common.linksmodel.body.data.get(position).description,view);
            }
        });

        listView.setAdapter(adapter);
        container.addView(view);
        */
        Page6 = view;
        return view;
    }

}
