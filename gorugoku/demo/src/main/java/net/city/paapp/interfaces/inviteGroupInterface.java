package net.city.paapp.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface inviteGroupInterface {
    @FormUrlEncoded
    @POST("/addgroupmembersa.php")
    Call<String> inviteGroupapi(@Field("targetname") String targetname, @Field("groupname") String groupname);
}
