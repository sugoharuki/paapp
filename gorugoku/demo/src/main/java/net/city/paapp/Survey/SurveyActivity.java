package net.city.paapp.Survey;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import net.city.paapp.AsyncHelper;
import net.city.paapp.AsyncInterFace;
import net.city.paapp.Calendar.Calendar;
import net.city.paapp.Common.Common;
import net.city.paapp.Model.answer;
import net.city.paapp.Model.data;
import net.city.paapp.Model.bkAns;
import net.city.paapp.R;
import net.city.paapp.interfaces.ShiftKibouApi;
import net.city.paapp.interfaces.surveyentryApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SurveyActivity  extends AppCompatActivity {
        public static Context ccc;
        public static ListView list;
        public static ListView list2;
        //public static testModel objects;
        public static List<data> objects2;
        public static testAdapter ada;
        public static bkAns[] bkAns;

        public static boolean jotaiChecked = false;
        public static int broad_id;
        @Override
        protected void onResume() {
            super.onResume();
            TextView maintitle = findViewById(R.id.MainTitle);
            Intent intent = getIntent();
            String title = intent.getStringExtra("title");
            broad_id = intent.getIntExtra("id",0);
            maintitle.setText(title);
            Button btn = findViewById(R.id.Entry);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!checkKakunin()) {//チェック状態を確認する
                        return;
                    }
                    Entry_survey();
                }
            });
            loadlist(ccc,list,ada,objects2);
        }
        public boolean checkKakunin()//全ての問題のチェック済み確認を行う
        {
            for(int i = 0; i < Common.ContentModel.size(); i++)
            {
                if(ada.getItem(i).jotai == "0")
                {
                    jotaiChecked = true;
                    list.setSelection(i);
                    return false;
                }
            }
            return true;
        }

        public boolean outputjson(View view){
            //jsonを吐き出す処理

            return true;
        }

    private void Entry_survey()
    {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("データ登録中");
        progressDialog.setMessage("データ登録中です。\nしばらくお待ちください。");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        CreateBaseModel(progressDialog);
    }
    public Boolean errorFlag = false;
    public ProgressDialog progDialog;
    private void CreateBaseModel(ProgressDialog progressDialog){
        progDialog = progressDialog;
        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {

            }

            @Override
            public String back() {
                try {
                    String result = "";
                    int i = 0;
                    for(data d :objects2) {
                        String  broadcast_id = String.valueOf(broad_id);
                        String  user_id = "1";//ログイン処理後直す TODO
                        String  questions_id = String.valueOf(Common.ContentModel.get(i).id);
                        String  questions_naiyo = d.subtitle;
                        String  checktype_id = String.valueOf(Common.AnswerModel.get(Common.ContentModel.get(i).CHECK_TYPE - 1).id);
                        String answer = "";
                        for(answer ans : d.answer)
                        {
                            if(ans.checked.equals("1"))
                            {
                                answer = ans.naiyo;
                            }
                        }
                        result   = CreateSurveyModel("http://220.247.10.217/survey_entry.php/",broadcast_id,user_id,questions_id,questions_naiyo,checktype_id,answer);
                        if(result != "true") {
                            errorFlag = true;
                            break;
                        }
                        i++;
                    }

                } catch (IOException e) {
                    errorFlag=true;
                }
                return null;
            }

            @Override
            public void end(String result) {
                if(errorFlag) {
                    progDialog.dismiss();
                }
                else {
                    progDialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(ccc);
                    builder.setMessage("登録完了")
                            .setTitle("アンケートの登録が完了しました")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // ボタンをクリックしたときの動作
                                    dialog.dismiss();
                                }
                            });
                    builder.show();
                }
            }
        });
    }

    private String CreateSurveyModel(String Path,String broadcast_id,String user_id,String questions_id,String questions_naiyo,String checktype_id,String answer) throws IOException {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        surveyentryApi API = retrofit.create(surveyentryApi.class);
        String r = "";
        try {
            r = API.setSurveyEntry(broadcast_id, user_id, questions_id, questions_naiyo, checktype_id, answer).execute().body();
        }
        catch(Exception e)
        {
            int i = 0;
        }

        return  r;
    }

        public static int position = 0;
        public static int yOffset = 0;
        public static  void loadlist(final Context ccc,final ListView lists,final  testAdapter adas,final List<data> objectss ){
            lists.setAdapter(adas);
            //lists.setSelection(scroolpos);
            lists.setSelectionFromTop(position, yOffset);
        }
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_survey);
            ccc = this;
            list = (ListView)findViewById(R.id.sample_list);
            //list2 = (ListView)findViewById(R.id.answer_list);
            int size = Common.ContentModel.size();
            bkAns = new bkAns[size];
            List<data> datalist = new ArrayList<data>();
            for(int i = 0 ; i< size ; i++) {
                data dt = new data();
                {
                    dt.subtitle = Common.ContentModel.get(i).SUB_TITLE;
                    int checktype_id = Common.ContentModel.get(i).CHECK_TYPE -1;
                    if(Common.AnswerModel.get(checktype_id).checktype.equals("ラジオボタン"))
                        dt.type = "2";
                    else
                        dt.type = "1";
                    answer ans = new answer();
                    ans.naiyo = Common.AnswerModel.get(checktype_id).NAIYO1;
                    ans.checked = "0";
                    answer ans2 = new answer();
                    ans2.naiyo = Common.AnswerModel.get(checktype_id).NAIYO2;
                    ans2.checked = "0";
                    answer ans3 = new answer();
                    ans3.naiyo = Common.AnswerModel.get(checktype_id).NAIYO3;
                    ans3.checked = "0";
                    answer ans4 = new answer();
                    ans4.naiyo = Common.AnswerModel.get(checktype_id).NAIYO4;
                    ans4.checked = "0";
                    answer ans5 = new answer();
                    ans5.naiyo = Common.AnswerModel.get(checktype_id).NAIYO5;
                    ans5.checked = "0";
                    answer ans6 = new answer();
                    ans6.naiyo = Common.AnswerModel.get(checktype_id).NAIYO6;
                    ans6.checked = "0";


                    List<answer> anslist = new ArrayList<>();
                    if(ans.naiyo != "")
                        anslist.add(ans);
                    if(ans2.naiyo != "")
                        anslist.add(ans2);
                    if(ans3.naiyo != "")
                        anslist.add(ans3);
                    if(ans4.naiyo != "")
                        anslist.add(ans4);
                    if(ans5.naiyo != "")
                        anslist.add(ans5);
                    if(ans6.naiyo != "")
                        anslist.add(ans6);
                    dt.answer = anslist;
                    dt.jotai = "0";
                }
                datalist.add(dt);
            }
            //objects.data = datalist;
            objects2 = datalist;
            //
            ada = new testAdapter(this,R.layout.activity_survey,objects2);
            ada.setData(objects2,list,ada);
        }

}

