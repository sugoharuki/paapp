package net.city.paapp.multi;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.text.InputType;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.city.paapp.Article;
import net.city.paapp.ArticleAdapter;
import net.city.paapp.AsyncHelper;
import net.city.paapp.AsyncInterFace;
import net.city.paapp.Calendar.CalendarView;
import net.city.paapp.Calendar.Calendar;
import net.city.paapp.Calendar.CalendarLayout;
import net.city.paapp.Article2;
import net.city.paapp.ArticleAdapter2;
import net.city.paapp.Common.Common;
import net.city.paapp.Model.shiftKibouModel;
import net.city.paapp.Model.shiftModel;
import net.city.paapp.R;
import net.city.paapp.base.activity.BaseActivity;
import net.city.paapp.group.GroupItemDecoration;
import net.city.paapp.group.GroupRecyclerView;
import net.city.paapp.interfaces.ShiftApi;
import net.city.paapp.interfaces.ShiftKibouApi;
import net.city.paapp.interfaces.ShiftKibouApi2;

import java.io.IOException;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MultiActivity extends BaseActivity implements
        CalendarView.OnCalendarMultiSelectListener,
        CalendarView.OnYearChangeListener,
        CalendarView.OnMonthChangeListener,
        CalendarView.OnCalendarSelectListener,
        View.OnClickListener {


    Map<String, Calendar> mMap = new HashMap<>();
    TextView mTextMonthDay;

    TextView mTextYear;

    TextView mTextLunar;

    TextView mTextCurrentDay;

    CalendarView mCalendarView;

    RelativeLayout mRelativeTool;
    private int mYear;
    private int mMonth;
    CalendarLayout mCalendarLayout;
    GroupRecyclerView mRecyclerView;

    int Color_flg = 0;
    Context mContext;
    public static void show(Context context) {
            context.startActivity(new Intent(context, MultiActivity.class));
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_multi;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void initView() {
        setStatusBarDarkMode();
            mTextMonthDay = (TextView) findViewById(R.id.tv_month_day);
            mTextYear = (TextView) findViewById(R.id.tv_year);
            mTextLunar = (TextView) findViewById(R.id.tv_lunar);
            mRelativeTool = (RelativeLayout) findViewById(R.id.rl_tool);
            mCalendarView = (CalendarView) findViewById(R.id.calendarView);
            mTextCurrentDay = (TextView) findViewById(R.id.tv_current_day);
            mTextMonthDay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mCalendarLayout.isExpand()) {
                        mCalendarLayout.expand();
                        return;
                    }
                    mCalendarView.showYearSelectLayout(mYear);
                    mTextLunar.setVisibility(View.GONE);
                    mTextYear.setVisibility(View.GONE);
                    mTextMonthDay.setText(String.valueOf(mYear));
                }
            });
        findViewById(R.id.fl_current).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCalendarView.scrollToCurrent();
            }
        });
        mCalendarLayout = (CalendarLayout) findViewById(R.id.calendarLayout);
        mCalendarView.setOnCalendarMultiSelectListener(this);
        mCalendarView.setOnYearChangeListener(this);
        mCalendarView.setOnCalendarSelectListener(this);

        mCalendarView.setOnMonthChangeListener(this);
        mTextYear.setText(String.valueOf(mCalendarView.getCurYear()));
        mYear = mCalendarView.getCurYear();
        mTextMonthDay.setText(mCalendarView.getCurMonth() + "月" + mCalendarView.getCurDay() + "日");
        mCalendarView.scrollToNext(false);
        mTextCurrentDay.setText(String.valueOf(mCalendarView.getCurDay()));
        findViewById(R.id.iv_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCalendarView.clearMultiSelect();
            }
        });
        mContext = this;
        mRecyclerView = (GroupRecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new GroupItemDecoration<String, Article2>());
        mRecyclerView.setAdapter(new ArticleAdapter2(this));
        mRecyclerView.notifyDataSetChanged();
            mRecyclerView.setOnItemClickListener(new GroupRecyclerView.OnItemClickListener(){
            @Override
            public void onClick(View view) {
            }
        });
    }
    private void Entry_Shift()
    {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("データ登録中");
        progressDialog.setMessage("データ取得中です。\nしばらくお待ちください。");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        CreateBaseModel(progressDialog);
    }
    public Boolean errorFlag = false;
    public ProgressDialog progDialog;
    private void CreateBaseModel(ProgressDialog progressDialog){
        progDialog = progressDialog;
        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {

            }

            @Override
            public String back() {
                try {
                    String result = "";
                    for(String key :mMap.keySet()) {
                        result   = CreateShiftModel("http://220.247.10.217/shift_kibou_entry.php/",mMap.get(key));
                        if(result != "true") {
                            errorFlag = true;
                            break;
                        }
                    }

                } catch (IOException e) {
                    errorFlag=true;
                }
                return null;
            }

            @Override
            public void end(String result) {
                if(errorFlag) {
                    progDialog.dismiss();
                    goerror();
                }
                else {
                    progDialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("登録完了")
                            .setTitle("シフト希望の登録が完了しました")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // ボタンをクリックしたときの動作
                                    getShiftKibouModel();
                                    dialog.dismiss();
                                }
                            });
                    builder.show();
                }
            }
        });
    }

    public void getShiftKibouModel()
    {
        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {
            }

            @Override
            public String back() {
                try {
                    Common.sftkibouModel = CreateShiftkibouModel("http://220.247.10.217/get_shift_kibou.php/");

                } catch (IOException e) {
                    //e.printStackTrace();
                    errorFlag=true;
                }
                return null;
            }

            @Override
            public void end(String result) {
                if(errorFlag)
                    goerror();
                else
                    initData();

            }
        });
    }

    private List<shiftKibouModel> CreateShiftkibouModel(String Path) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ShiftKibouApi2 API = retrofit.create(ShiftKibouApi2.class);
        return  API.getShiftKibou(1).execute().body();
    }

    public void goerror(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("通信に失敗しました");
        alertDialog.setMessage("通信状況を確認してください");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.create();
        alertDialog.show();
    }

    private String CreateShiftModel(String Path,Calendar cal) throws IOException {
        //日付設定
        String month = String.format("%02d",cal.getMonth());
        String day = String.format("%02d",cal.getDay());
        String date = cal.getYear() + "-" + month + "-" + day;
        Date nowdate = new Date(System.currentTimeMillis());
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        ShiftKibouApi API = retrofit.create(ShiftKibouApi.class);
        return  API.setShiftKibou("1",date,cal.getScheme(),String.valueOf(nowdate)).execute().body();
    }


    private void buttonClick(int color_flg)
    {
        Color_flg = color_flg;
        List<Calendar> selectedCal = mCalendarView.getMultiSelectCalendars();
        ReinitData(selectedCal);
        mCalendarView.clearMultiSelect();
    }

    protected void ReinitData(List<Calendar> selectedCal) {
        int year = mYear;
        int month = mMonth;
        int select_color = Color.WHITE;
        String text = "";
        if(Color_flg == 1)
        {
            select_color = Common.haya2;
            text = "1";
        }
        else if (Color_flg == 2)
        {
            select_color = Common.oso2;
            text = "2";
        }
        else if(Color_flg == 3)
        {
            select_color = Common.oso2;
            text = "3";
        }
        else if(Color_flg == 4)
        {
            select_color = Common.sinnya2;
            text = "4";
        }
        else if(Color_flg == 5)
        {
            select_color = Color.LTGRAY;
            text = "5";
        }
        else if(Color_flg == 6)
        {
            select_color = Color.WHITE;
            text = "6";
        }


        for(Calendar cal : selectedCal)
        {
            mMap.put(getSchemeCalendar(year, month, cal.getDay(), select_color, text).toString(),
                    getSchemeCalendar(year, month, cal.getDay(), select_color, text));
        }

        mCalendarView.setSchemeDate(mMap);
        Color_flg = 0;
    }

    @Override
    protected void initData() {
        int year = mCalendarView.getCurYear();
        int month = mCalendarView.getCurMonth();


        mMap = new HashMap<>();
        for (shiftKibouModel shift : Common.sftkibouModel) {
            String[] datestr = shift.date.split("-");

            int select_color = Color.WHITE;
            if (shift.select_shift == 1) {
                select_color = Common.haya;
            } else if (shift.select_shift == 2 ) {
                select_color = Common.oso;
            } else if( shift.select_shift == 3) {
                select_color = Common.oso;
            } else if (shift.select_shift == 4) {
                select_color = Common.sinnya;
            }else if (shift.select_shift == 5) {
                select_color = Color.argb(123,100,100,100);
            }

            int Year2 = Integer.parseInt(datestr[0]);
            int Month2 = Integer.parseInt(datestr[1]);
            int date2 = Integer.parseInt(datestr[2]);

            mMap.put(getSchemeCalendar(Year2, Month2, date2, select_color, String.valueOf(shift.select_shift)).toString(),
                    getSchemeCalendar(Year2, Month2, date2, select_color, String.valueOf(shift.select_shift)));
        }

        //此方法在巨大的数据量上不影响遍历性能，推荐使用
        mCalendarView.setSchemeDate(mMap);
    }


    @Override
    public void onClick(View v) {
        Button btn = (Button)v;
        String name = String.valueOf(btn.getText());

        switch(name)
        {
            case "早番":
                buttonClick(1);
                break;

            case "中番":
                buttonClick(2);
                break;
            case "遅番":
                buttonClick(3);
                break;
            case "深夜番":
                buttonClick(4);
                break;
            case "全て":
                buttonClick(5);
                break;
            case "クリア":
                buttonClick(6);
                break;
            case "備考入力":
                // テキスト入力用Viewの作成
                final EditText editView = new EditText(mContext);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);


                // OKボタンの設定
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // OKボタンをタップした時の処理をここに記述
                    }
                });

// キャンセルボタンの設定
                builder.setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // キャンセルボタンをタップした時の処理をここに記述
                    }
                });

                AlertDialog Alertdialog = builder.create();
                Alertdialog.setTitle("備考入力");
                Alertdialog.setView(editView);


                Alertdialog.show();

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

                // Get screen width and height in pixels
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                // The absolute width of the available display size in pixels.
                int displayWidth = displayMetrics.widthPixels;
                // The absolute height of the available display size in pixels.
                int displayHeight = displayMetrics.heightPixels;

                int dialogWindowWidth = (int) (displayWidth * 0.9f);
                // Set alert dialog height equal to screen height 70%
                int dialogWindowHeight = (int) (displayHeight * 0.5f);
                editView.setGravity(Gravity.TOP | Gravity.LEFT);
                editView.setLines(15);
                editView.setMaxLines(10);
                lp.copyFrom(Alertdialog.getWindow().getAttributes());
                lp.width = dialogWindowWidth;
                lp.height = dialogWindowHeight;
                Alertdialog.getWindow().setAttributes(lp);
                break;
            case "登録":
                AlertDialog.Builder builder2 = new AlertDialog.Builder(mContext);
                builder2.setMessage("シフト希望を登録します。")
                        .setTitle("シフト希望登録")
                        .setPositiveButton("登録", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // ボタンをクリックしたときの動作
                                dialog.dismiss();
                                Entry_Shift();
                            }
                        })
                        .setNegativeButton("キャンセル", null);
                builder2.show();
                break;
        }
    }

    private Calendar getSchemeCalendar(int year, int month, int day, int color, String text) {
        Calendar calendar = new Calendar();
        calendar.setYear(year);
        calendar.setMonth(month);
        calendar.setDay(day);
        calendar.setSchemeColor(color);//如果单独标记颜色、则会使用这个颜色
        calendar.setScheme(text);
        calendar.addScheme(new Calendar.Scheme());
        calendar.addScheme(0xFF008800, "假");
        calendar.addScheme(0xFF008800, "节");
        return calendar;
    }


    @Override
    public void onCalendarMultiSelectOutOfRange(Calendar calendar) {
    }

    @Override
    public void onMultiSelectOutOfSize(Calendar calendar, int maxSize) {
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCalendarMultiSelect(Calendar calendar, int curSize, int maxSize) {
        mTextLunar.setVisibility(View.INVISIBLE);
        mTextYear.setVisibility(View.VISIBLE);
        mTextMonthDay.setText(calendar.getMonth() + "月" + calendar.getDay() + "日");
        mTextYear.setText(String.valueOf(calendar.getYear()));
        //mTextLunar.setText(calendar.getLunar());
        mYear = calendar.getYear();
    }


    @Override
    public void onYearChange(int year) {
        mTextMonthDay.setText(String.valueOf(year));
    }


    @Override
    public void onMonthChange(int year, int month) {
        net.city.paapp.Calendar.Calendar calendar = mCalendarView.getSelectedCalendar();
        mTextLunar.setVisibility(View.INVISIBLE);
        mTextYear.setVisibility(View.VISIBLE);
        mTextMonthDay.setText(month + "月");
        mTextYear.setText(String.valueOf(year));
        //mTextLunar.setText(calendar.getLunar());
        mYear = year;
        mMonth = month;
    }

    @Override
    public void onCalendarOutOfRange(Calendar calendar) {

    }

    @Override
    public void onCalendarSelect(Calendar calendar, boolean isClick) {
    }

}
