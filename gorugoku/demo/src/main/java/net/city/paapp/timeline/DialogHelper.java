package net.city.paapp.timeline;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import net.city.paapp.async.voidinter;


/**
 * Created by smileapp04 on 2016/03/14.
 */
public class DialogHelper {



    public static void simpleOkDialog(Context c,String title,String msg, final voidinter v){
        new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        v.act();
                    }
                })
                .show();
    }

    public static void simpleDialog(Context c,String title,String msg, final voidinter v){
        new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("Cancel", null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        v.act();
                    }
                })
                .show();
    }

    public static AlertDialog dialog;

    public static void simpleDialog(Context c,String title,String msg){
        new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK",null)
                .show();
    }

    public static void makeSimpleModalDialog(Context c,String title,String msg,final voidinter v){
        dialog = null;
        dialog =  new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        v.act();
                    }
                })
                .create()
                ;
    }

    public static AlertDialog makeSimpleModalDialog2(Context c,String title,String msg,final voidinter v){


        dialog =   new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        v.act();
                    }
                })
                .create();

        return  dialog;
    }

}
