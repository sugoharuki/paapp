package net.city.paapp.async;

import net.city.paapp.Model.GroupChat;

import java.util.List;


public interface AsyncInterFace3 {
    public void start();
    public List<GroupChat> back();
    public void end(List<GroupChat> result);
}
