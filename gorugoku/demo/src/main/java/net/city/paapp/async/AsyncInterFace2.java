package net.city.paapp.async;

import net.city.paapp.Model.timeline;

import java.util.List;


public interface AsyncInterFace2 {
    public void start();
    public List<timeline> back();
    public void end(List<timeline> result);
}
