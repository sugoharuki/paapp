package net.city.paapp.interfaces;

import net.city.paapp.Model.SurveyTitleModel;
import net.city.paapp.Model.bodyModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SurveyApi {
    @GET(".")
    Call<List<SurveyTitleModel>> getSurveyTitle();
}
