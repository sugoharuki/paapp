package net.city.paapp.WebView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.city.paapp.R;


public class WebActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(R.style.WebTheme);

        setContentView(R.layout.seting_web);
        WebView www = findViewById(R.id.setweb);

        //リンクをタップしたときに標準ブラウザを起動させない
        www.setWebViewClient(new WebViewClient());
        Intent intent = getIntent();
        String Path = intent.getStringExtra("Path");
        // JavaScriptを有効化
        www.getSettings().setJavaScriptEnabled(true);
        // Web Storage を有効化
        www.getSettings().setDomStorageEnabled(true);
        www.getSettings().setBuiltInZoomControls(true);
        www.getSettings().setAppCacheEnabled(true);
        www.loadUrl(Path);


    }
}
