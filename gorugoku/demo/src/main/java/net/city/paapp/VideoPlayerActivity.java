package net.city.paapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Util;
import net.city.paapp.Common.Common;

import java.net.URLEncoder;

public class VideoPlayerActivity extends Activity {

    private static final String KEY_PLAY_WHEN_READY = "play_when_ready";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_POSITION = "position";
    private static final String STATE_PLAYER_FULLSCREEN = "playerFullscreen";
    private static final String TIME_BAR_SIZE = "timebarSize";

    private PlayerView mPlayerView;
    private SimpleExoPlayer player;
    private ImageButton mFullScreenButton;
    private Dialog mFullScreenDialog;
    private Timeline.Window window;
    private DataSource.Factory mediaDataSourceFactory;
    private DefaultTrackSelector trackSelector;
    private boolean shouldAutoPlay;
    private BandwidthMeter bandwidthMeter;

    private int mTimebarSize;
    private ImageView ivHideControllerButton;
    private boolean playWhenReady;
    private int currentWindow;
    private long playbackPosition;
    private boolean FullScreenFlag = false;
    private Toolbar toolbar;

    public VideoPlayerActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        if (savedInstanceState == null) {
            playWhenReady = false;
            currentWindow = 0;
            playbackPosition = 0;
            FullScreenFlag = false;
            mTimebarSize = 0;
        } else {
            playWhenReady = savedInstanceState.getBoolean(KEY_PLAY_WHEN_READY);
            currentWindow = savedInstanceState.getInt(KEY_WINDOW);
            playbackPosition = savedInstanceState.getLong(KEY_POSITION);
            FullScreenFlag = savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN);
            mTimebarSize = savedInstanceState.getInt(TIME_BAR_SIZE);
        }

        if(!FullScreenFlag) {
            Intent intent = getIntent();
            Integer position = intent.getIntExtra("Position", -1);
            Integer position2 = intent.getIntExtra("Position2", -1);
            toolbar = findViewById(R.id.toolbar4);
            TextView mTitle = toolbar.findViewById(R.id.title4);
            /*String toolbartitle;
            if (position == -1)
                toolbartitle = "タイトルを表示";
            else
                toolbartitle = "タイトルを表示";
            mTitle.setText(toolbartitle);
*/          mTitle.setText(Common.videoModel.get(position2).title);

            // String SplitTitle[] = CreateString(position, position2);
            //String str = CreateMaintitle(SplitTitle);
            TextView textView = findViewById(R.id.exo_position);
            textView.setTextColor(Color.BLACK);
            TextView textView2 = findViewById(R.id.exo_duration);
            textView2.setTextColor(Color.BLACK);
            TextView mTitle2 = findViewById(R.id.subtitle);
            //mTitle2.setText(CreateInfo(SplitTitle));
            TextView mTitle3 = findViewById(R.id.maintitle);
            //mTitle3.setText(str);

            //再生ボタンのサイズ変更
            WindowManager wm = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
            Display disp = wm.getDefaultDisplay();
            Point size = new Point();
            disp.getSize(size);
            LinearLayout linearLayout = findViewById(R.id.playspace);
            ViewGroup.LayoutParams params = linearLayout.getLayoutParams();
            params.height = size.x - (int)(size.x /2.5);
            linearLayout.setLayoutParams(params);
        }
        else{
            findViewById(R.id.titlespace).setVisibility(View.INVISIBLE);


            WindowManager wm = (WindowManager)getSystemService(WINDOW_SERVICE);
            Display disp = wm.getDefaultDisplay();
            Point size = new Point();
            disp.getSize(size);
            TextView textView = findViewById(R.id.exo_position);
            textView.setTextColor(Color.WHITE);
            TextView textView2 = findViewById(R.id.exo_duration);
            textView2.setTextColor(Color.WHITE);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            params.height = size.y - mTimebarSize;
            findViewById(R.id.playspace).setLayoutParams(params);
        }

        shouldAutoPlay = playWhenReady;
        bandwidthMeter = new DefaultBandwidthMeter();
        mediaDataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "mediaPlayerSample"), (TransferListener<? super DataSource>) bandwidthMeter);
        window = new Timeline.Window();
        ivHideControllerButton = findViewById(R.id.exo_controller);
    }

    /*private String[] CreateString(Integer position, Integer position2)
    {
        String str;
        if(position == -1)
            str = Common.staffmodel.body.data.get(position2).description;
        else
            str = Common.list_lessonmodel.get(position).body.data.get(position2).description;
        String splitdata[] = str.split(",",0);
        return  splitdata;
    }*/

    private String CreateMaintitle(String[] str){
        String str2 = str[2].replace("$naiyou$:$","");
        str2 = str2.replace("$}","");
        str2 = str2.replace("##","\n");
        //String str3 = str2.split(",",0);
        return str2;
    }

    private String CreateInfo(String[] str){
        String str2 = str[1].replace("$info$:$","");
        str2 = str2.replace("$","");
        return str2;
    }

    private void initFullscreenDialog()
    {
        mFullScreenDialog = new Dialog(this,android.R.style.Theme_Black_NoTitleBar_Fullscreen){
            public void onBackPressed() {
                if(FullScreenFlag)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog(){
        //onCreate中にバーのサイズを取得できないためここで取得
        mTimebarSize = findViewById(R.id.timebarspace).getHeight();
        ((ViewGroup)mPlayerView.getParent()).removeView(mPlayerView);
        mFullScreenDialog.addContentView(mPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        mFullScreenButton.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.exo_controls_fullscreen_exit2));
        FullScreenFlag = true;
        mFullScreenDialog.show();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    private void closeFullscreenDialog()
    {
        ((ViewGroup)mPlayerView.getParent()).removeView(mPlayerView);
        ((FrameLayout)findViewById(R.id.main_media_frame)).addView(mPlayerView);
        FullScreenFlag = false;
        mFullScreenDialog.dismiss();
        mFullScreenButton.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.exo_controls_fullscreen_enter2));
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void initFullscreenButton(){
        mFullScreenButton = findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!FullScreenFlag) {
                    openFullscreenDialog();
                }
                else{
                    closeFullscreenDialog();
                }
            }
        });
    }

    private void initializePlayer() {
        mPlayerView.requestFocus();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        mPlayerView.setPlayer(player);
        player.setPlayWhenReady(shouldAutoPlay);
        Intent intent = getIntent();
        Integer position = intent.getIntExtra("Position", -1);
        Integer position2 = intent.getIntExtra("Position2", -1);

        String url = "http://220.247.10.217/uptest/server/php/files/";
        String filename = Common.videoModel.get(position2).name;
        try {
            url = url +  URLEncoder.encode(filename, "UTF-8");
            url = url.replace("+", "%20");
        }
        catch(Exception e)
        {
            int i = 0;
        }
        MediaSource mediaSource = new ExtractorMediaSource.Factory(mediaDataSourceFactory)
                .createMediaSource(Uri.parse(url));
        boolean haveStartPosition = currentWindow != C.INDEX_UNSET;
        if (haveStartPosition) {
            player.seekTo(currentWindow, playbackPosition);
        }

        mPlayerView.setControllerShowTimeoutMs(0);
        mPlayerView.setControllerHideOnTouch(false);

        player.prepare(mediaSource, !haveStartPosition, playWhenReady);

        ivHideControllerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlayerView.hideController();
            }
        });
    }

    private void releasePlayer() {
        if (player != null) {
            updateStartPosition();
            shouldAutoPlay = player.getPlayWhenReady();
            player.release();
            player = null;
            trackSelector = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mPlayerView == null){
            mPlayerView = (PlayerView)findViewById(R.id.player_view);
            initFullscreenDialog();
            initFullscreenButton();

            //initializePlayer();
        }
        initializePlayer();
        if(FullScreenFlag){
            ((ViewGroup)mPlayerView.getParent()).removeView(mPlayerView);
            mFullScreenDialog.addContentView(mPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
            mFullScreenButton.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.exo_controls_fullscreen_exit2));
            mFullScreenDialog.show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mPlayerView != null && mPlayerView.getPlayer() != null){
            releasePlayer();
        }

        if(mFullScreenDialog != null)
            mFullScreenDialog.dismiss();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        updateStartPosition();

        outState.putBoolean(KEY_PLAY_WHEN_READY, playWhenReady);
        outState.putInt(KEY_WINDOW, currentWindow);
        outState.putLong(KEY_POSITION, playbackPosition);
        outState.putBoolean(STATE_PLAYER_FULLSCREEN,FullScreenFlag);
        outState.putInt(TIME_BAR_SIZE,mTimebarSize);
        super.onSaveInstanceState(outState);
    }

    private void updateStartPosition() {
        if(player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
        }
    }
}