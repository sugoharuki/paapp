package net.city.paapp.chat;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class BitmapHelper {

    public static Bitmap getBitmapFromUri(Activity act,Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                act.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public static String saveBitmapToSd(final Context con,Bitmap mBitmap) {
        try {
            // sdcardフォルダを指定
            File root = Environment.getExternalStorageDirectory();

            // 日付でファイル名を作成　
            Date mDate = new Date();
            SimpleDateFormat fileName = new SimpleDateFormat("yyyyMMdd_HHmmss");

            // 保存処理開始
            FileOutputStream fos = null;
            //fos = new FileOutputStream(new File(root, fileName.format(mDate) + ".jpg"));


            String iconpath = pri2.loadLogName(con) + "icon" + ".png";

            File pa = new File(root,iconpath);

            String apath = pa.getAbsolutePath();
            String spath = pa.getPath();

            fos = new FileOutputStream(pa);


            // jpegで保存
            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            // 保存処理終了
            fos.close();

            return apath;
        } catch (Exception e) {
            Log.e("Error", "" + e.toString());
            return "err";
        }
    }

    public static Bitmap makeBmp(String path,Bitmap bmp){

        try{

            ExifInterface exifInterface = new ExifInterface(path);
            // 向きを取得
            int orientation = Integer.parseInt(exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION));

            switch(orientation){
                case 1://only scaling

                    break;
                case 2://flip vertical

                    break;
                case 3://rotate 180

                    break;
                case 4://flip horizontal

                    break;
                case 5://flip vertical rotate270

                    break;
                case 6://rotate 90

                    // 回転マトリックス作成（90度回転）
                    Matrix mat = new Matrix();
                    mat.postRotate(90);
                    // 回転したビットマップを作成
                    bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);

                    break;
                case 7://flip vertical, rotate 90

                    break;
                case 8://rotate 270

                    break;
            }
            return bmp;
        }
        catch (Exception e){

            return null;
        }
    }

}


