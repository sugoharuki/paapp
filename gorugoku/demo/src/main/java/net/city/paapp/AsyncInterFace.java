package net.city.paapp;


// イベントクラスの定義
public interface AsyncInterFace {
    void start();
    String back();
    void end(String result);
}
