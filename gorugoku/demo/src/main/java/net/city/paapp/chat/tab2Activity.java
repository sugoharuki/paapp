package net.city.paapp.chat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import net.city.paapp.Model.KojinChat;
import net.city.paapp.R;
import net.city.paapp.interfaces.ResultInterface2;
import net.city.paapp.realmModel.midoku;
import net.city.paapp.realmModel.rireki;
import net.city.paapp.timeline.commons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class tab2Activity extends Activity  {


    @Override
    protected void onResume() {
        super.onResume();
        findtext.setText("");
        tab2ActivityViewFlg=1;
        loadList();
    }
    public static int tab2ActivityViewFlg=0;


    @Override
    public void onPause(){
        super.onPause();
        tab2ActivityViewFlg=0;
    }


    public static String jsons = "";
    public static  void  loadList(){
        objects.clear();

        ArrayList<String> select = new ArrayList<String>();
        select.add("rirekiname");
        select.add("sendtype");
        select.add("times");
        String where ="id > ?";
        String order ="times DESC";
        ArrayList<String> bind = new ArrayList<String>();
        bind.add("-1");

         jsons = "[";

        Realm.init(ccc);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<rireki> memlist;
        try{
            memlist = realm.where(rireki.class).findAll().sort("times", Sort.DESCENDING);

            int i = 1;
            for(rireki mem : memlist) {
                if(mem.rirekiname == null)
                    continue;
                if (!mem.rirekiname.contains(findtext.getText())) return;
                final CustomData obj = new CustomData();
                obj.setTextData(mem.rirekiname);
                obj.setTextData2(mem.sendtype);

                if(mem.sendtype.equals("kojin")){
                    jsons+="{\"toname\":\""+ commons.username+"\",\"fromname\":\""+mem.rirekiname+"\",\"flg\":\"\"}";
                }else{
                    jsons+="{\"toname\":\"\",\"fromname\":\""+mem.rirekiname+"\",\"flg\":\""+mem.rirekiname+"\"}";
                }
                if(memlist.size() != i){
                    jsons+=",";
                }
                i++;
                //ここでディクショナリにあれば赤くする
                objects.add(obj);
            }
        }
        finally{
            realm.close();
        }
        jsons += "]";

        GetMidoku.set(jsons, new ResultInterface2() {
            @Override
            public void result(List<KojinChat> r) {
                try{
                    for (int i=0; i<r.size(); i++){
                        final String flgs = r.get(i).flg;
                        final String id = r.get(i).id;
                        String toname = r.get(i).toname;
                        final String fromname = r.get(i).fromname;

                        ArrayList<String> select = new ArrayList<String>();
                        select.add("targetname");
                        select.add("flg");
                        select.add("ids");
                        String where ="";
                        String order ="id";
                        ArrayList<String> bind = new ArrayList<String>();

                        if(flgs.equals("")){
                             where ="targetname = ?";
                            bind.add(fromname);
                        }else{
                             where ="flg = ?";
                            bind.add(flgs);
                        }
                        //sugo Realmをテスト
                        Realm.init(ccc);
                        Realm realm = Realm.getDefaultInstance();
                        midoku mymidoku;
                        try
                        {
                            if(flgs.equals("")){
                                mymidoku = realm.where(midoku.class).equalTo("targetname",fromname).findAllSorted("ids").last();
                            }else{
                                mymidoku = realm.where(midoku.class).equalTo("flg",flgs).findAllSorted("ids").last();
                            }
                            if(mymidoku!=null)
                            {
                                if(mymidoku.flg.equals("")){
                                    if(mymidoku.targetname.equals(fromname))
                                    {
                                        if(Integer.parseInt(id)>Integer.parseInt(mymidoku.ids))
                                        {
                                            for(CustomData obj:objects)
                                            {
                                                if(obj.getTextData().equals(fromname)){
                                                    obj.setcolorData(Color.RED);
                                                }
                                            }
                                        }
                                    }
                                }
                                else{
                                    if(mymidoku.flg.equals(flgs)){
                                        if(!fromname.equals(commons.username)){
                                            if(Integer.parseInt(id)>Integer.parseInt(mymidoku.ids)){
                                                for (CustomData obj:objects) {
                                                    if(obj.getTextData().equals(mymidoku.flg)){
                                                        obj.setcolorData(Color.RED);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        finally
                        {
                            if(realm != null)
                                realm.close();
                        }

                        int aaa=0;
                        aaa++;
                    }
                    int aa=0;
                    aa++;
                }catch (Exception e){
                    int aa=0;
                    aa++;
                }
                adapter = new CustomAdapter(ccc,0,objects);
                list.setAdapter(adapter);
            }
        });
    }

    public static Context ccc;
    public static ListView list;
    public static List<CustomData> objects;
    public static   CustomAdapter adapter;
    public static TextView findtext;

    HashMap<String,String> midokumap = new HashMap<String,String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_list);
        ccc = this;
        list = (ListView)findViewById(R.id.chatLists);

        objects = new ArrayList<CustomData>();
        adapter = new CustomAdapter(ccc,0,objects);

        findtext = (TextView)findViewById(R.id.chatfindText);
        findtext.setInputType(InputType.TYPE_CLASS_TEXT);

        final Button findbtn = (Button)findViewById(R.id.chatfindbtn);
        findbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadList();
            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                CustomAdapter ada = (CustomAdapter) list.getAdapter();
                CustomData aaa = ada.getItem(position);
                final String name = aaa.getTextData();

                new AlertDialog.Builder(ccc)
                        .setTitle("項目を削除しますか？")
                        .setMessage("よければOKボタンを押してください")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Realm.init(ccc);
                                Realm realm = Realm.getDefaultInstance();
                                rireki mem;
                                try {
                                    mem = realm.where(rireki.class).equalTo("rirekiname",name).findFirst();
                                    if(mem != null) {
                                        realm.beginTransaction();
                                        mem.deleteFromRealm();
                                        realm.commitTransaction();
                                    }
                                }
                                finally{
                                    realm.close();
                                }

                                loadList();
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .show();
                return true;
            }
        });


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                ListView list = (ListView) adapter;
                CustomAdapter ada = (CustomAdapter) list.getAdapter();
                CustomData aaa = ada.getItem(position);

                final String rirekiname = aaa.getTextData();
                final String sendtype = aaa.getTextData2();

                long currentTimeMillis = System.currentTimeMillis();
                String times = String.valueOf(currentTimeMillis);
                Realm.init(ccc);
                Realm realm = Realm.getDefaultInstance();
                rireki mem = new rireki();
                mem.rirekiname = rirekiname;
                mem.sendtype = sendtype;
                mem.times = times;
                try {
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(mem);
                    realm.commitTransaction();
                }catch(Exception e)
                {
                    int Test = 0;
                }
                finally{
                    realm.close();
                }

                if (!sendtype.equals("kojin")) {
                    groupChatActivity.targetgroupname = rirekiname;
                    Intent intent = new Intent(ccc, groupChatActivity.class);
                    startActivity(intent);
                } else {
                    DtlActivity.targetDisplayName = rirekiname;
                    Intent intent = new Intent(ccc, DtlActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}