package net.city.paapp;

import android.os.AsyncTask;

public class AsyncHelper {

    public static  void Async2(final AsyncInterFace v){
        //ブロッキングといえど非同期を使う
        new AsyncTask<Void, Void, String>() {
            //uiスレッドにて最初にやる処理
            @Override
            protected void onPreExecute() {
                v.start();
            }


            @Override
            protected String doInBackground(Void... params) {
                try {
                    return v.back();
                } catch (Exception e) {
                    return "ng";
                }
            }

            //バック完了後にuiスレッドにて最後にやる処理
            @Override
            protected void onPostExecute(String result) {
                v.end(result);
            }

        }.execute();
    }
}
