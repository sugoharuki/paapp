package net.city.paapp.chat;

import android.content.Context;
import android.content.SharedPreferences;

import net.city.paapp.timeline.ImageBuf;
import net.city.paapp.timeline.commons;


public class priicon {

    public static  void saveicon(Context con ,String n){
        // プリファレンスの準備 //
        SharedPreferences pref = con.getSharedPreferences( "saveicon", Context.MODE_PRIVATE );
        // プリファレンスに書き込むためのEditorオブジェクト取得 //
        SharedPreferences.Editor editor = pref.edit();
        // "user_name" というキーで名前を登録
        editor.putString( "name", n );
        // 書き込みの確定（実際にファイルに書き込む）
        editor.commit();
    }

    public static String loadIcon(Context con){
        SharedPreferences pref = con.getSharedPreferences( "saveicon", Context.MODE_PRIVATE );

        String iconpath = "def";
        if (ImageBuf.get(commons.syainid) != null)
        {
            iconpath = commons.syainid;
        }

        return pref.getString("name", iconpath + "icon.png");
        //return pref.getString("name","deficon.png");
    }



}
