package net.city.paapp.timeline;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;


import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.city.paapp.R;
import net.city.paapp.async.ImageManager;
import net.city.paapp.photo.subscaleview.ImageSource;
import net.city.paapp.photo.subscaleview.SubsamplingScaleImageView;


public class dialogHelper2  {


    // 画像保存
    private void saveImage(ImageView img, Context ccc) {
        // イメージビューからビットマップ保持
        //ImageView sampleImageView = (ImageView)findViewById(R.id.image);
        Bitmap sampleImage = ((BitmapDrawable)img.getDrawable()).getBitmap();

        // ビットマップを SDカードに保存
        ImageManager imageManager = new ImageManager(ccc);
        try {
            // 画像の保存実行
            String albumName = "Save image sample";
            imageManager.save(sampleImage, albumName);
        } catch (Error e) {
            Log.e("MainActivity", "onCreate: " + e);

            // 画像の保存失敗メッセージ表示
            Toast.makeText(ccc, "SDカードに保存できませんでした", Toast.LENGTH_SHORT).show();
        } finally {
            // 画像の保存完了メッセージ表示
            Toast.makeText(ccc, "SDカードに保存しました", Toast.LENGTH_SHORT).show();
        }
    }

        public Dialog onCreateDialog(final ImageView img, final Context ccc) {

            final Dialog dialog = new Dialog(ccc);
            // タイトル非表示
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            // フルスクリーン
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
            dialog.setContentView(R.layout.dialog_custom);
            // 背景を透明にする
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            dialog.findViewById(R.id.yessave).setBackgroundColor(Color.WHITE);
            dialog.findViewById(R.id.nosave).setBackgroundColor(Color.WHITE);

            TextView txt = (TextView)dialog.findViewById(R.id.situmon);
            txt.setTextColor(Color.WHITE);

            SubsamplingScaleImageView imgs = (SubsamplingScaleImageView)dialog.findViewById(R.id.toukouimg);
            Bitmap bmp = ((BitmapDrawable)img.getDrawable()).getBitmap();
            imgs.setImage(ImageSource.bitmap(bmp));

            // OK ボタンのリスナ
            dialog.findViewById(R.id.yessave).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveImage(img,ccc);
                    dialog.dismiss();
                }
            });
            // Close ボタンのリスナ
            dialog.findViewById(R.id.nosave).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            return dialog;
        }

}
