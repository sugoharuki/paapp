package net.city.paapp;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.city.paapp.Common.Common;

import java.util.List;

public class ListAdapter extends ArrayAdapter<ListItem> {

    private int mResource;
    private List<ListItem> mItems;
    private LayoutInflater mInflater;

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param resource リソースID
     * @param items リストビューの要素
     */
    public ListAdapter (Context context, int resource, List<ListItem> items) {
        super(context, resource, items);

        mResource = resource;
        mItems = items;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView != null) {
            view = convertView;
        }
        else {
            view = mInflater.inflate(mResource, null);
        }

        // リストビューに表示する要素を取得
        ListItem item = mItems.get(position);

        WindowManager wm = (WindowManager)view.getContext().getSystemService(Context.WINDOW_SERVICE);
        // Displayのインスタンス取得
        Display disp = wm.getDefaultDisplay();
        Point size = new Point();
        disp.getSize(size);
        int widthSize = (size.x);
        int heightSize = widthSize / 4;

        TextView textView = view.findViewById(R.id.title);
        ViewGroup.LayoutParams textParams = textView.getLayoutParams();
        textParams.height = heightSize;
        int textWidthSize = size.x;
        textParams.width = textWidthSize;
        textView.setLayoutParams(textParams);
        if(Common.mTextSize ==  0) {
            final float MIN_TEXT_SIZE = 10f;
            float textSize = 1000f;
            Paint paint = new Paint();
            paint.setTextSize(textSize);
            Paint.FontMetrics fm = paint.getFontMetrics();
            float textHeight = (float) (Math.abs(fm.top)) + (Math.abs(fm.descent));
            float textWidth = paint.measureText(textView.getText().toString());
            while (textParams.height < textHeight | textParams.width < textWidth) {
                // 調整しているテキストサイズが、定義している最小サイズ以下か。
                if (MIN_TEXT_SIZE >= textSize) {
                    // 最小サイズ以下になる場合は最小サイズ
                    textSize = MIN_TEXT_SIZE;
                    break;
                }
                // テキストサイズをデクリメント
                textSize--;
                // Paintにテキストサイズ設定
                paint.setTextSize(textSize);

                // テキストの縦幅を再取得
                fm = paint.getFontMetrics();
                textHeight = (float) (Math.abs(fm.top)) + (Math.abs(fm.descent));

                // テキストの横幅を再取得
                textWidth = paint.measureText(textView.getText().toString());
            }
            Common.mTextSize = textSize / 4;
        }
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, Common.mTextSize);

        // タイトルを設定
        TextView title = (TextView)view.findViewById(R.id.title);
        title.setText(item.getTitle());

        return view;
    }
}