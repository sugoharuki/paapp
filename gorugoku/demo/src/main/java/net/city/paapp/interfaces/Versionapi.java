package net.city.paapp.interfaces;

import net.city.paapp.VerModel.VerModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Versionapi {
    @GET("api")
    Call<VerModel> apiVerModel();
}
