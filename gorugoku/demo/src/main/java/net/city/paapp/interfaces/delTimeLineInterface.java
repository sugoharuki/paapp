package net.city.paapp.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface delTimeLineInterface {
    @FormUrlEncoded
    @POST("/del.php")
    Call<String> delTimeLineapi(@Field("dels") String dels);
}
