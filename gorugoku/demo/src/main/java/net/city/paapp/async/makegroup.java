package net.city.paapp.async;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.city.paapp.interfaces.inviteGroupInterface;
import net.city.paapp.interfaces.makeGroupInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class makegroup {

    public static  String  douki(String targetname,String groupname,String allsendflg){
                try {

                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://smileapp.yokohama/")
                            .addConverterFactory(GsonConverterFactory.create(gson))
                            .build();
                    makeGroupInterface API = retrofit.create(makeGroupInterface.class);
                    String r = API.makeGroupapi(targetname, groupname, allsendflg).execute().body();
                    //sugo TODO トピックを購入
                    /*FirebaseMessaging.getInstance().subscribeToTopic(groupname)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if (!task.isSuccessful()) {
                                        String r = "err";
                                    }
                                    String r = "ok";
                                }
                            });*/

                    return r;


                } catch (Exception ex) {
                    return  "err";
                }
    }

    public static  String  shoutai(String targetname,String groupname){
        try {

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://smileapp.yokohama/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            inviteGroupInterface API = retrofit.create(inviteGroupInterface.class);
            String r = API.inviteGroupapi(targetname, groupname).execute().body();
            return r;
        } catch (Exception ex) {
            return  "err";
        }
    }

}
