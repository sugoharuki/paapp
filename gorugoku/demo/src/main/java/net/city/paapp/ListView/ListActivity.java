package net.city.paapp.ListView;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import net.city.paapp.R;
import net.city.paapp.VideoPlayerActivity;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_list);
        Intent intent = getIntent();
        Integer position = intent.getIntExtra("Position", 0);
        String toolbartitle = intent.getStringExtra("TitleName");
        Toolbar toolbar = findViewById(R.id.toolbar3);
        TextView mTitle = toolbar.findViewById(R.id.title3);

        mTitle.setText(toolbartitle);

        ListView listView = findViewById(R.id.list_activity);
        ArrayList<ListActivityItem> listItems = new ArrayList<>();
        /*for (int i = 0; i < Common.list_lessonmodel.get(position).body.total; i++) {
            String path = Common.list_lessonmodel.get(position).body.data.get(i).pictures.sizes.get(3).link;
            String title2 = Common.list_lessonmodel.get(position).body.data.get(i).created_time;
            title2 = title2.replace("T", " ");
            title2 = title2.replace("+00:00", "投稿");
            String title3 = Common.list_lessonmodel.get(position).body.data.get(i).name;
            ListActivityItem item = new ListActivityItem(path, title2, title3);
            listItems.add(item);
        }*/
        ListActivityAdapter adapter = new ListActivityAdapter(this, R.layout.list_item2, listItems);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent nowintent = getIntent();
                Integer position2 = nowintent.getIntExtra("Position", 0);

                Intent intent = new Intent(ListActivity.this, VideoPlayerActivity.class);
                intent.putExtra("Position2",position);
                intent.putExtra("Position", position2);
                startActivity(intent);
            }
        });

    }
}