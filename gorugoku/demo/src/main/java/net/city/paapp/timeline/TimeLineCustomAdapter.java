package net.city.paapp.timeline;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.city.paapp.Model.timeline;
import net.city.paapp.R;
import net.city.paapp.async.AsyncHelper;
import net.city.paapp.async.AsyncInterFace;
import net.city.paapp.async.ImageAsyncTask2;
import net.city.paapp.async.ImageManager;
import net.city.paapp.async.inter;
import net.city.paapp.async.voidinter;
import net.city.paapp.interfaces.delTimeLineInterface;
import net.city.paapp.interfaces.incInterface;
import net.city.paapp.photo.subscaleview.SubsamplingScaleImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import static net.city.paapp.timeline.timelineActivity.yOffset;



public class TimeLineCustomAdapter extends ArrayAdapter<timeline> {
    private LayoutInflater layoutInflater_;
    //Bitmap oBmp;
    Context ccc;

    private boolean checkflg = false;

    // 画像保存
    private void saveImage(ImageView img) {
        // イメージビューからビットマップ保持
        //ImageView sampleImageView = (ImageView)findViewById(R.id.image);
        Bitmap sampleImage = ((BitmapDrawable)img.getDrawable()).getBitmap();

        // ビットマップを SDカードに保存
        ImageManager imageManager = new ImageManager(ccc);
        try {
            // 画像の保存実行
            String albumName = "Save image sample";
            imageManager.save(sampleImage, albumName);
        } catch (Error e) {
            Log.e("MainActivity", "onCreate: " + e);

            // 画像の保存失敗メッセージ表示
            Toast.makeText(ccc, "SDカードに保存できませんでした", Toast.LENGTH_SHORT).show();
        } finally {
            // 画像の保存完了メッセージ表示
            Toast.makeText(ccc, "SDカードに保存しました", Toast.LENGTH_SHORT).show();
        }
    }

    public TimeLineCustomAdapter(Context context, int textViewResourceId, List<timeline> objects) {
        super(context, textViewResourceId, objects);
        ccc = context;
        layoutInflater_ = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    List<timeline> objects;
    ListView list;
    TimeLineCustomAdapter ada;
    public void setData(List<timeline> objects, ListView list,TimeLineCustomAdapter ada) {
        this.list = list;
        this.objects = objects;
        this.ada = ada;
    }


    public Object fetch(String address) throws MalformedURLException,IOException {
        URL url = new URL(address);
        Object content = url.getContent();
        return content;
    }


    public void imageAct(final timeline item ,final ImageView icon){
        new ImageAsyncTask2(ccc, new inter() {
            @Override
            public void first() {
            }

            @Override
            public Bitmap act() {
                URL url;
                InputStream istream;
                try {
                    url = new URL("http://smileapp.yokohama/files/"+item.iconpath);
                    //インプットストリームで画像を読み込む
                    istream = url.openStream();
                    //読み込んだファイルをビットマップに変換
                    Bitmap oBmp = BitmapFactory.decodeStream(istream);
                    istream.close();
                    return oBmp;
                } catch (IOException e) {
                    // TODO 自動生成された catch ブロック
                    e.printStackTrace();
                    return  null;
                }
            }

            @Override
            public void end(Bitmap r)
            {
                ImageBuf.set(r,item.iconpath);
                icon.setImageBitmap(r);
                int a=0;
                a++;
            }
        }).execute();

    }

    private int flg=0;
    private int cnt=0;

    private void getloginid(final String name,final String syain){
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 特定の行(position)のデータを得る
        final timeline item = (timeline)getItem(position);

        // convertViewは使い回しされている可能性があるのでnullの時だけ新しく作る
        if (null == convertView) {
            convertView = layoutInflater_.inflate(R.layout.timeline, null);
        }

        final ImageView icon = (ImageView)convertView.findViewById(R.id.タイムラインアイコン);


        icon.setImageBitmap(null);

        if(!item.iconpath.equals("みみみみみみみ.png")){
            if(ImageBuf.get(item.iconpath)!=null){
                icon.setImageBitmap(ImageBuf.get(item.iconpath));
            }else{
                imageAct(item,icon);
            }
        }
        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<String> select = new ArrayList<String>();
                select.add("name");
                select.add("syain");
                select.add("furigana");
                String where ="id > ?";
                //String order =" name DESC ";
                String order =" id ";

                ArrayList<String> bind = new ArrayList<String>();
                bind.add("0");
                cnt = 0;
                //sugo Realm実装
                /*Realm.init(ccc);
                Realm realm = Realm.getDefaultInstance();
                RealmResults<allmember> allmember;
                allmember mem;
                try{
                    allmember = realm.where(allmember.class).findAll();
                    mem = allmember.where().equalTo("name",item.username).findFirst();
                    if(mem != null)
                    {
                        cnt = 1;
                    }
                }catch(Exception e){
                    int Test = 0;
                }
                finally{
                    realm.close();
                }
                if(cnt == 0)
                {
                    new AlertDialog.Builder(ccc)
                            .setTitle("エラー")
                            .setMessage("既に削除されている社員です")
                            .setPositiveButton("OK", null)
                            .show();

                    return;
                }
            */
                new AlertDialog.Builder(ccc)
                        .setTitle("確認")
                        .setMessage("プロフに移動しますか？")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // OK button pressed
                                String name = item.username;
                                String syain = item.iconpath.replace("icon.png","");
                                getloginid(name,syain);
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .show();
            }
        });

        icon.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                /*DialogHelper.simpleDialog(ccc, "確認", "コメントに移動しますか？", new voidinter() {
                    @Override
                    public void act() {
                        commentActivity.targetname = item.username;
                        commentActivity.id = item.id;
                        Intent intent = new Intent(ccc,commentActivity.class);
                        // 次画面のアクティビティ起動
                        ccc.startActivity(intent);
                    }
                });*/
                return true;
            }
        });


        final TextView names = (TextView)convertView.findViewById(R.id.タイムライン名前);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!commons.username.equals(item.username)){
                    return;
                }

                DialogHelper.simpleDialog(ccc, "削除確認", "削除しますか？", new voidinter() {
                    @Override
                    public void act() {
                        final AlertDialog dia = new AlertDialog.Builder(ccc)
                                .setTitle("削除中")
                                .setMessage("しばらくお待ち下さい")
                                .setCancelable(false)
                                .create();
                        AsyncHelper.Async3(new AsyncInterFace() {
                            @Override
                            public void start() {
                                dia.show();
                            }

                            @Override
                            public String back() {
                                try {
                                    Gson gson = new GsonBuilder()
                                            .setLenient()
                                            .create();
                                    Retrofit retrofit = new Retrofit.Builder()
                                            .baseUrl("http://smileapp.yokohama/")
                                            .addConverterFactory(GsonConverterFactory.create(gson))
                                            .build();
                                    delTimeLineInterface API = retrofit.create(delTimeLineInterface.class);
                                    return API.delTimeLineapi(item.id).execute().body();

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    //dia.dismiss();
                                    return "err";
                                }
                            }

                            @Override
                            public void end(String result) {
                                int a = 0;
                                a++;
                                try {
                                    timelineActivity.loadlist(ccc, list, ada, objects);
                                    int aa = 0;
                                    aa++;
                                } catch (Exception e) {
                                    int aa = 0;
                                    aa++;
                                }
                                dia.dismiss();
                            }
                        }).execute();
                    }
                });
            }
        });


        final TextView times = (TextView)convertView.findViewById(R.id.タイムライン時間);
        final TextView bodys = (TextView)convertView.findViewById(R.id.タイムライン本文);
        final ImageView images = (ImageView)convertView.findViewById(R.id.タイムライン画像);
        final TextView iinenum = (TextView)convertView.findViewById(R.id.タイムラインいいね数字);
        final ImageButton btn = (ImageButton)convertView.findViewById(R.id.タイムラインいいね);
        btn.setBackgroundColor(Color.TRANSPARENT);
        btn.setImageResource(R.drawable.good);
        btn.setScaleType(ImageView.ScaleType.FIT_CENTER);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogHelper.simpleDialog(ccc, "いいね！しますか？", "よければOKボタンを押してください", new voidinter() {
                    @Override
                    public void act() {
                        final AlertDialog dia = new AlertDialog.Builder(ccc)
                                .setTitle("送信処理中")
                                .setMessage("しばらくお待ち下さい")
                                .setCancelable(false)
                                .create();
                        AsyncHelper.Async3(new AsyncInterFace() {
                            @Override
                            public void start() {
                                dia.show();
                            }

                            @Override
                            public String back() {
                                try {
                                    Gson gson = new GsonBuilder()
                                            .setLenient()
                                            .create();
                                    Retrofit retrofit = new Retrofit.Builder()
                                            .baseUrl("http://smileapp.yokohama/")
                                            .addConverterFactory(GsonConverterFactory.create(gson))
                                            .build();
                                    incInterface API = retrofit.create(incInterface.class);
                                    Void r = API.incApi(item.id).execute().body();
                                    return "";

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    return "err";
                                }
                            }

                            @Override
                            public void end(String result) {
                                if (!result.equals("err")) {
                                    if (!result.equals("ng")) {
                                        new AlertDialog.Builder(ccc)
                                                .setTitle("いいね完了")
                                                .setCancelable(false)
                                                .setMessage("いいねしました。")
                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        timelineActivity.loadlist(ccc, list, ada, objects);
                                                        timelineActivity.scroolpos = Integer.parseInt(item.id);

                                                        timelineActivity.position = list.getFirstVisiblePosition();
                                                        yOffset = list.getChildAt(0).getTop();
                                                    }
                                                })
                                                .show();
                                    }
                                }
                                dia.dismiss();
                            }
                        }).execute();
                    }
                });
            }
        });




        images.setVisibility(View.GONE);
        names.setText(item.username);
        times.setText(item.times);
        bodys.setText(item.body);
        iinenum.setText(item.iine);

        images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ImageView img = (ImageView) v;

                new dialogHelper2().onCreateDialog(img,ccc).show();

            }
        });


        if(!item.imagepath.equals("")){
            images.setVisibility(View.VISIBLE);
            if(ImageBuf.get(item.imagepath)!=null){
                images.setImageBitmap(ImageBuf.get(item.imagepath));
            }else{
                //重いので廃止

                new ImageAsyncTask2(ccc, new inter() {
                    @Override
                    public void first() {
                    }
                    @Override
                    public Bitmap act() {
                        URL url;
                        InputStream istream;
                        try {
                            //url = new URL("https://www.gstatic.com/android/market_images/web/play_logo_x2.png");
                            //url = new URL("http://122.223.132.189:81/files/"+"test001.jpeg");
                            url = new URL("http://smileapp.yokohama/files/"+item.imagepath);
                            //インプットストリームで画像を読み込む
                            istream = url.openStream();
                            //読み込んだファイルをビットマップに変換
                            Bitmap oBmp = BitmapFactory.decodeStream(istream);
                            istream.close();
                            return oBmp;
                        } catch (IOException e) {
                            // TODO 自動生成された catch ブロック
                            e.printStackTrace();
                            return  null;
                        }
                    }

                    @Override
                    public void end(Bitmap r)
                    {
                        if(r==null)return;
                        ImageBuf.set(r,item.imagepath);
                        images.setImageBitmap(r);
                        int a=0;
                        a++;
                    }
                }).execute();
            }
        }
        return convertView;
    }
}