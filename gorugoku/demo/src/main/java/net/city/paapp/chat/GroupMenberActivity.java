package net.city.paapp.chat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import net.city.paapp.Model.GroupMenber;
import net.city.paapp.Model.MyGroup;
import net.city.paapp.R;
import net.city.paapp.async.AsyncHelper;
import net.city.paapp.async.AsyncInterFace;
import net.city.paapp.async.getgroupmember;
import net.city.paapp.async.getmygroup;
import net.city.paapp.async.makegroup;
import net.city.paapp.realmModel.allmember;
import net.city.paapp.timeline.commons;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class GroupMenberActivity extends Activity {

    static String str;

     AlertDialog.Builder alert;
     AlertDialog.Builder alertBuilder2;
     AlertDialog alert2;

    private void loadList(){

        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {
                View   lay = (View)findViewById(R.id.layoutlast);
                lay.setVisibility(View.VISIBLE);
            }

            @Override
            public String back() {

                objects.clear();

                //String r = getgroupmember.douki(str);
                List<GroupMenber> r = getgroupmember.douki(str);
                try{
                    //JSONArray arr = new JSONArray(r);
                    for (int i=0; i<r.size(); i++){
                        String id = r.get(i).id;
                        String groupname = r.get(i).groupname;
                        String targetname = r.get(i).targetname;
                        String buyflg = r.get(i).buyflg;
                        String allsendflg = r.get(i).allsendflg;
                        if (targetname.contains(findtext.getText())){
                            CustomData obj = new CustomData();
                            obj.setTextData(targetname);
                            obj.setcolorData(Color.YELLOW);
                            //obj.setTextData2(allsendflg);
                            objects.add(obj);
                        }

                    }
                }catch (Exception e){
                    int a= 0;
                    a++;
                }

                    //全員検索
                objects2.clear();

                final String strs = zenkakuHiraganaToZenkakuKatakana(findtext.getText().toString());

                Realm.init(ccc);
                Realm realm = Realm.getDefaultInstance();
                RealmResults<allmember> memberlist;
                try{
                    memberlist = realm.where(allmember.class).contains("name",findtext.getText().toString()).or().contains("furigana",strs).findAll();
                    for(allmember mem : memberlist)
                    {
                        CustomData obj = new CustomData();
                        obj.setTextData(mem.name);
                        objects2.add(obj);
                    }
                }
                finally{
                    realm.close();
                }
                return null;
            }

            @Override
            public void end(String result) {
                View   lay = (View)findViewById(R.id.layoutlast);
                lay.setVisibility(View.GONE);
                //ＵＩハンドラにて操作
                list.setAdapter(adapter);
                list2.setAdapter(adapter2);
                //alert2.dismiss();
            }
        });
    }

    Context ccc;

    ListView list;
    List<CustomData> objects;
    CustomAdapter adapter;
    ListView list2;
    List<CustomData> objects2;
    CustomAdapter adapter2;

    EditText findtext;
    Button findfind;
    Button ketteibtn;
    String prevactivityName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        prevactivityName = intent.getStringExtra("gamenname");

        setContentView(R.layout.groupmenberactivity);
        ccc = this;

        alert  =  new AlertDialog.Builder(ccc);
        alertBuilder2 = new AlertDialog.Builder(ccc);
        alert2 = alertBuilder2.create();
        alert2.setCancelable(false);
        alert2.setTitle("Title");
        alert2.setMessage("処理中");


        findtext = (EditText)findViewById(R.id.findfindtext);
        findfind = (Button)findViewById(R.id.findfind);
        findfind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadList2();
            }
        });
        findtext.setInputType(InputType.TYPE_CLASS_TEXT);

        ketteibtn = (Button)findViewById(R.id.ketteibtn);
        ketteibtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //グループトークから来た場合
                if(prevactivityName.equals("groupChatActivity"))
                {
                    shoutaiGroup(str);
                }

                //グループ作成の招待制から来た場合
                if(prevactivityName.equals("AddGroupActivity"))
                {
                    AddGroupActivity.adapter.clear();
                    int count = adapter.getCount();

                    for (int i = 0; i < count; i++)
                    {
                        AddGroupActivity.adapter.add(adapter.getItem(i));
                    }

                    AddGroupActivity.list.setAdapter(AddGroupActivity.adapter);

                    finish();
                }
            }
        });

//        TextView t=(TextView)findViewById(R.id.groupname);
//            t.setText(str);


         list = (ListView)findViewById(R.id.targetList);
         //list.setBackgroundColor(Color.TRANSPARENT);
         objects = new ArrayList<CustomData>();
         adapter = new CustomAdapter(this,0,objects);

         list2 = (ListView)findViewById(R.id.AllListView);
         objects2 = new ArrayList<CustomData>();
         adapter2 = new CustomAdapter(this,0,objects2);


        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            //voidだと怒られるのでとりあえずboolean値を返す
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                // 長押しされたアイテムを取得する。
                //adapter.remove(adapter.getItem(position));

                buy(position);

                return false;
            }
        });


        loadList();







        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {


                CustomAdapter ada = (CustomAdapter) list.getAdapter();

                String selectname = adapter2.getItem(position).getTextData().toString();
                if(selectname.equals(commons.username)) return;

                int count = ada.getCount();
                int selectflg = 0;

                for (int i = 0; i < count; i++)
                {
                    if(selectname.equals(ada.getItem(i).getTextData().toString()))
                    {
                        selectflg = 1;
                        break;
                    }
                }

                if(selectflg == 1) return;

                ada.add(adapter2.getItem(position));
            }
        });

    }

    public  String zenkakuHiraganaToZenkakuKatakana(String s) {
        StringBuffer sb = new StringBuffer(s);
        for (int i = 0; i < sb.length(); i++) {
            char c = sb.charAt(i);
            if (c >= 'ぁ' && c <= 'ん') {
                sb.setCharAt(i, (char)(c - 'ぁ' + 'ァ'));
            }
        }
        return sb.toString();
    }

    private void loadList2(){

        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {

            }

            @Override
            public String back() {
                //全員検索
                objects2.clear();
                final String strs = zenkakuHiraganaToZenkakuKatakana(findtext.getText().toString());
                Realm.init(ccc);
                Realm realm = Realm.getDefaultInstance();
                RealmResults<allmember> memberlist;
                try{
                    memberlist = realm.where(allmember.class).contains("name",findtext.getText().toString()).or().contains("furigana",strs).findAll();
                    for(allmember mem : memberlist)
                    {
                        CustomData obj = new CustomData();
                        obj.setTextData(mem.name);
                        objects2.add(obj);
                    }
                }
                finally{
                    realm.close();
                }
                return null;
            }

            @Override
            public void end(String result) {
                View   lay = (View)findViewById(R.id.layoutlast);
                lay.setVisibility(View.GONE);
                //ＵＩハンドラにて操作
                list2.setAdapter(adapter2);
                //alert2.dismiss();
            }
        });
    }

    AlertDialog dia;
    void shoutaiGroup(final String  groupStr){
        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {
                dia = new AlertDialog.Builder(ccc)
                        .setTitle("処理中")
                        .setMessage("しばらくお待ちください")
                        .setCancelable(false)
                        .create();
                dia.show();
            }

            @Override
            public String back() {
                String r ="";

                 //招待制
                int count = adapter.getCount();
                for (int i = 0; i < count; i++)
                {
                    r = makegroup.shoutai(adapter.getItem(i).getTextData(),groupStr);
                }

                if(r.equals("ok")){
                    //backs();
                    return "add";
                }else if(r.equals("doubleerr")){
                    return "in";
                }

                int aa = 0;
                aa++;
                return "ng";
            }

            @Override
            public void end(String end) {
                //dia.dismiss();

                AlertDialog.Builder d = new AlertDialog.Builder(ccc)
                        .setCancelable(false)
                        .setTitle("招待")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dia.dismiss();
                                dialog.dismiss();
                                finish();
                            }
                        });

                if (end.equals("add")) {
                    //変更したら画面に適用するため
                    //tab4Activity.changeFlg = 1;
                    list.setAdapter(adapter);

                    d.setMessage("グループに招待しました");
                    AlertDialog alertDialog = d.create();
                    alertDialog.show();
                } else if (end.equals("in")) {
                    d.setMessage("すでに使用されています");
                    AlertDialog alertDialog = d.create();
                    alertDialog.show();
                } else if (end.equals("ng")) {
                    d.setMessage("エラーです");
                    AlertDialog alertDialog = d.create();
                    alertDialog.show();
                }
            }
        });
    }


    public void buy(final int position){
        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {

            }

            @Override
            public String back() {
                String sankaflg = "";

                String selectname = adapter.getItem(position).getTextData().toString();
                List<MyGroup> myGroupList = getmygroup.douki2(selectname);

                try{
                    //JSONArray arr = new JSONArray(r);

                    for (int i=0; i< myGroupList.size(); i++){
                       // String groupname = arr.getJSONObject(i).getString("groupname");

                        if(myGroupList.get(i).groupname.equals(str))
                        {
                            sankaflg = "sanka";
                            break;
                        }
                    }
                }catch (Exception e){
                    sankaflg = "ng";
                    int a= 0;
                    a++;
                }

                if(sankaflg.equals("sanka"))
                {
                    return sankaflg;
                }
                else sankaflg = "ng";

                return sankaflg;
            }

            @Override
            public void end(String result) {
                if (result.equals("sanka")) return;

                adapter.remove(adapter.getItem(position));
            }
        });
    }


}
