package net.city.paapp.interfaces;

import net.city.paapp.Model.BroadCastModel;
import net.city.paapp.Model.contentModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface contentApi {
    @FormUrlEncoded
    @POST(".")
    Call<List<contentModel>> getContents(@Field("survey_id") int survey_id);
}
