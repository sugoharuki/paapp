package net.city.paapp.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;

public interface timelinecountInterface {
    //@Headers("Content-Type: application/json")
    @GET("timelinecount.php/")
    Call<String> gettimelinecount();
}
