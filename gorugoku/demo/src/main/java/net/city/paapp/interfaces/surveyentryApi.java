package net.city.paapp.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface surveyentryApi {
    @FormUrlEncoded
    @POST(".")
    Call<String> setSurveyEntry(@Field("broadcast_id") String broadcast_id, @Field("user_id") String user_id, @Field("questions_id") String questions_id, @Field("questions_naiyo") String questions_naiyo ,
                               @Field("checktype_id") String checktype_id, @Field("answer") String answer);
}
