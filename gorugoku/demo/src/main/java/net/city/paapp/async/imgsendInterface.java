package net.city.paapp.async;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface imgsendInterface {
    @FormUrlEncoded
    @POST("/image3.php")
    Call<String> imgSendApi(@Field("img") String img, @Field("name") String name);
}
