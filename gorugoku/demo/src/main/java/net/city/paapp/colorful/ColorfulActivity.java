package net.city.paapp.colorful;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;


import net.city.paapp.Article;
import net.city.paapp.ArticleAdapter;
import net.city.paapp.AsyncHelper;
import net.city.paapp.AsyncInterFace;
import net.city.paapp.Calendar.Calendar;
import net.city.paapp.Calendar.CalendarLayout;
import net.city.paapp.Calendar.CalendarView;
import net.city.paapp.Common.Common;
import net.city.paapp.R;
import net.city.paapp.base.activity.BaseActivity;
import net.city.paapp.group.GroupItemDecoration;
import net.city.paapp.group.GroupRecyclerView;
import net.city.paapp.Model.shiftModel;
import net.city.paapp.interfaces.ShiftApi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ColorfulActivity extends BaseActivity implements
        CalendarView.OnCalendarSelectListener,
        CalendarView.OnYearChangeListener,
        CalendarView.OnMonthChangeListener,
        View.OnClickListener {
    Context mContext;
    TextView mTextMonthDay;

    TextView mTextYear;

    TextView mTextLunar;

    TextView mTextCurrentDay;

    CalendarView mCalendarView;

    RelativeLayout mRelativeTool;
    private int mYear;
    CalendarLayout mCalendarLayout;
    GroupRecyclerView mRecyclerView;

    public static void show(Context context) {
        try {
            context.startActivity(new Intent(context, ColorfulActivity.class));
        }
        catch(Exception e)
        {
            int i = 0;
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_colorful;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void initView() {
        setStatusBarDarkMode();
        mTextMonthDay = (TextView) findViewById(R.id.tv_month_day);
        mTextYear = (TextView) findViewById(R.id.tv_year);
        mTextLunar = (TextView) findViewById(R.id.tv_lunar);
        mRelativeTool = (RelativeLayout) findViewById(R.id.rl_tool);
        mCalendarView = (CalendarView) findViewById(R.id.calendarView);
        mTextCurrentDay = (TextView) findViewById(R.id.tv_current_day);
        mTextMonthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mCalendarLayout.isExpand()) {
                    mCalendarLayout.expand();
                    return;
                }
                mCalendarView.showYearSelectLayout(mYear);
                mTextLunar.setVisibility(View.GONE);
                mTextYear.setVisibility(View.GONE);
                mTextMonthDay.setText(String.valueOf(mYear));
            }
        });
        findViewById(R.id.fl_current).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCalendarView.scrollToCurrent();
            }
        });
        mCalendarLayout = (CalendarLayout) findViewById(R.id.calendarLayout);
        mCalendarView.setOnCalendarSelectListener(this);
        mCalendarView.setOnYearChangeListener(this);
        mCalendarView.setOnMonthChangeListener(this);
        mTextYear.setText(String.valueOf(mCalendarView.getCurYear()));
        mYear = mCalendarView.getCurYear();
        mTextMonthDay.setText(mCalendarView.getCurMonth() + "月" + mCalendarView.getCurDay() + "日");
        mTextLunar.setText("今日");
        mTextCurrentDay.setText(String.valueOf(mCalendarView.getCurDay()));

        mContext = this;
    }

    @Override
    protected void initData() {

        int year = mCalendarView.getCurYear();
        int month = mCalendarView.getCurMonth();

        Map<String, Calendar> map = new HashMap<>();

        List<shiftModel> myShift = new ArrayList<shiftModel>();
        for(shiftModel shift : Common.sftModel)
        {
            if(shift.user_id == 1)
            {
                myShift.add(shift);
            }
        }
        if(myShift.size() > 0) {
            for (shiftModel shift : myShift) {
                String[] rgbstr = shift.in_color.split(",");
                int r = Integer.parseInt(rgbstr[0]);
                int g = Integer.parseInt(rgbstr[1]);
                int b = Integer.parseInt(rgbstr[2]);
                String[] datestr = shift.date.split("-");

                String text = "";
                if (shift.uki_flg == 2) {
                    text = "早番";
                } else if(shift.uki_flg == 3 || shift.uki_flg == 4){
                    text = "遅番";
                }   else if (shift.uki_flg == 5){
                    text = "深夜番";
                }

                int Year2 = Integer.parseInt(datestr[0]);
                int Month2 = Integer.parseInt(datestr[1]);
                int date2 = Integer.parseInt(datestr[2]);

                map.put(getSchemeCalendar(Year2, Month2, date2, Color.rgb(r, g, b), text).toString(),
                        getSchemeCalendar(Year2, Month2, date2, Color.rgb(r, g, b), text));

            }
            mCalendarView.setSchemeDate(map);
        }



        mRecyclerView = (GroupRecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new GroupItemDecoration<String, Article>());
        mRecyclerView.setAdapter(new ArticleAdapter(this,String.valueOf(mCalendarView.getCurYear()),String.valueOf(mCalendarView.getCurMonth()),String.valueOf(mCalendarView.getCurDay())));
        mRecyclerView.notifyDataSetChanged();

    }


    @Override
    public void onClick(View v) {
    }

    private Calendar getSchemeCalendar(int year, int month, int day, int color, String text) {
        Calendar calendar = new Calendar();
        calendar.setYear(year);
        calendar.setMonth(month);
        calendar.setDay(day);
        calendar.setSchemeColor(color);//如果单独标记颜色、则会使用这个颜色
        calendar.setScheme(text);
        return calendar;
    }



    @Override
    public void onCalendarOutOfRange(Calendar calendar) {

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCalendarSelect(Calendar calendar, boolean isClick) {
        mTextLunar.setVisibility(View.INVISIBLE);
        mTextYear.setVisibility(View.VISIBLE);
        mTextMonthDay.setText(calendar.getMonth() + "月" + calendar.getDay() + "日");
        mTextYear.setText(String.valueOf(calendar.getYear()));
        //mTextLunar.setText(calendar.getLunar());
        mYear = calendar.getYear();


        mRecyclerView = (GroupRecyclerView) findViewById(R.id.recyclerView);

        mRecyclerView.setAdapter(new ArticleAdapter(mContext,String.valueOf(calendar.getYear()),String.valueOf(calendar.getMonth()),String.valueOf(calendar.getDay())));
        mRecyclerView.notifyDataSetChanged();

    }

    @Override
    public void onYearChange(int year) {
        mTextMonthDay.setText(String.valueOf(year));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onMonthChange(int year, int month) {
        net.city.paapp.Calendar.Calendar calendar = mCalendarView.getSelectedCalendar();
        mTextLunar.setVisibility(View.INVISIBLE);
        mTextYear.setVisibility(View.VISIBLE);
        mTextMonthDay.setText(calendar.getMonth() + "月" + calendar.getDay() + "日");
        mTextYear.setText(String.valueOf(calendar.getYear()));
        //mTextLunar.setText(calendar.getLunar());
        mYear = calendar.getYear();
        //月が変わった場合その月のデータを取得する
        java.util.Calendar cale = java.util.Calendar.getInstance();
        cale.set(java.util.Calendar.YEAR, year);
        cale.set(java.util.Calendar.MONTH, month - 1);
        int month_end = cale.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);
        String date_start = year + "-" + month + "-1";
        String date_end = year + "-" + month + "-" + month_end;
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("データ取得中");
        progressDialog.setMessage("データ取得中です");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        CreateBaseModel(date_start ,date_end,progressDialog);

    }
    public Boolean errorFlag = false;
    public String date_st ="";
    public String date_ed = "";
    public ProgressDialog progDialog;
    private void CreateBaseModel(final String date_start, String date_end, ProgressDialog progressDialog){
        date_st = date_start;
        date_ed = date_end;
        progDialog = progressDialog;
        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {

            }

            @Override
            public String back() {
                try {
                    Common.sftModel = CreateShiftModel("http://220.247.10.217/shift_check.php/",date_st,date_ed);
                } catch (IOException e) {
                    errorFlag=true;
                }
                return null;
            }

            @Override
            public void end(String result) {
                if(errorFlag) {
                    progDialog.dismiss();
                    goerror();
                }
                else {
                    ReinitData();
                    progDialog.dismiss();
                }
            }
        });
    }

    public void goerror(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("通信に失敗しました");
        alertDialog.setMessage("通信状況を確認してください");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.create();
        alertDialog.show();
    }

    private List<shiftModel> CreateShiftModel(String Path,String date_start,String date_end) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ShiftApi API = retrofit.create(ShiftApi.class);
        return  API.getShiftApi(date_start,date_end).execute().body();
    }


    protected void ReinitData() {
        int year = mCalendarView.getCurYear();
        int month = mCalendarView.getCurMonth();
        Map<String, Calendar> map = new HashMap<>();
        List<shiftModel> myShift = new ArrayList<shiftModel>();
        for(shiftModel shift : Common.sftModel)
        {
            if(shift.user_id == 1)
            {
                myShift.add(shift);
            }
        }
        if(myShift.size() > 0) {
            for (shiftModel shift : myShift) {
                String[] rgbstr = shift.in_color.split(",");
                int r = Integer.parseInt(rgbstr[0]);
                int g = Integer.parseInt(rgbstr[1]);
                int b = Integer.parseInt(rgbstr[2]);
                String[] datestr = shift.date.split("-");

                String text = "";
                if (shift.uki_flg == 2) {
                    text = "早番";
                } else if(shift.uki_flg == 3 || shift.uki_flg == 4){
                    text = "遅番";
                }   else if (shift.uki_flg == 5){
                    text = "深夜番";
                }

                int Year2 = Integer.parseInt(datestr[0]);
                int Month2 = Integer.parseInt(datestr[1]);
                int date2 = Integer.parseInt(datestr[2]);

                map.put(getSchemeCalendar(Year2, Month2, date2, Color.rgb(r, g, b), text).toString(),
                        getSchemeCalendar(Year2, Month2, date2, Color.rgb(r, g, b), text));

            }
            mCalendarView.setSchemeDate(map);
        }
        mRecyclerView = (GroupRecyclerView) findViewById(R.id.recyclerView);

        mRecyclerView.setAdapter(new ArticleAdapter(mContext,String.valueOf(year),String.valueOf(month),String.valueOf(mCalendarView.getCurDay())));
        mRecyclerView.notifyDataSetChanged();
        /*mRecyclerView = (GroupRecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new GroupItemDecoration<String, Article>());
        mRecyclerView.setAdapter(new ArticleAdapter(this,String.valueOf(mCalendarView.getCurYear()),String.valueOf(mCalendarView.getCurMonth()),String.valueOf(mCalendarView.getCurDay())));
        mRecyclerView.notifyDataSetChanged();*/
    }

}
