package net.city.paapp.timeline;

import android.graphics.Color;

/**
 * Created by smileapp04 on 2016/07/10.
 */
public class commons {
    public static String  username = "名無しの社員さん";
    public static String  syainid = "syainid";
    public static String  userid = "";
    //石田追記16/09/22
    public static Boolean loginflg = true;

    public static void setUserData(String name,String uid,String sid){
        username = name;
        userid =uid;
        syainid = sid;
    }

    public static int syozokucolor(String syozokuname){
        int color = Color.TRANSPARENT;

        if (syozokuname == null) return color;

        if (syozokuname.contains("パーラー")) color = Color.rgb(255,127,80);
        if (syozokuname.contains("ネットカフェ")) color = Color.rgb(211,211,211);
        if (syozokuname.contains("おもてなし")) color = Color.rgb(144,238,144);
        if (syozokuname.contains("アプリケーション")) color = Color.rgb(173,216,230);
        if (syozokuname.contains("City design")) color = Color.rgb(173,216,230);
        if (syozokuname.contains("クールジャパン"))color = Color.rgb(173,216,230);
        if (syozokuname.contains("CoolJapan"))color = Color.rgb(173,216,230);

        if(syozokuname.contains("本社"))
        {
            color = Color.rgb(255,255,255);

            if (syozokuname.contains("パーラー")) color = Color.rgb(255,127,80);
            if (syozokuname.contains("ネットカフェ")) color = Color.rgb(211,211,211);
            if (syozokuname.contains("おもてなし")) color = Color.rgb(144,238,144);
            if (syozokuname.contains("アプリケーション")) color = Color.rgb(173,216,230);
            if (syozokuname.contains("City design")) color = Color.rgb(173,216,230);
            if (syozokuname.contains("クールジャパン"))color = Color.rgb(173,216,230);
            if (syozokuname.contains("CoolJapan"))color = Color.rgb(173,216,230);
        }

        return color;
    }


    /**
     * ベストフィットなスケーリング率を求める
     * @param dest_width 目的のサイズ（幅）
     * @param dest_height　目的のサイズ（高さ）
     * @param src_width　元のサイズ（幅）
     * @param src_height　元のサイズ（高さ）
     * @return
     */
    public static float getFitScale(int dest_width, int dest_height
            , int src_width, int src_height){
        float ret = 0;

        if(dest_width < dest_height){
            //縦が長い
            if(src_width < src_height){
                //縦が長い
                ret = (float)dest_height / (float)src_height;

                if((src_width * ret) > dest_width){
                    //縦に合わせると横がはみ出る
                    ret = (float)dest_width / (float)src_width;
                }
            }else{
                //横が長い
                ret = (float)dest_width / (float)src_width;
            }
        }else{
            //横が長い
            if(src_width < src_height){
                //縦が長い
                ret = (float)dest_height / (float)src_height;
            }else{
                //横が長い
                ret = (float)dest_width / (float)src_width;

                if((src_height * ret) > dest_height){
                    //横に合わせると縦がはみ出る
                    ret = (float)dest_height / (float)src_height;
                }
            }
        }

        return ret;
    }
}
