package net.city.paapp;

import com.google.android.exoplayer2.util.Util;

import net.city.paapp.utils.DemoData;
import net.city.recyclertablayout.RecyclerTabLayout;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import android.widget.ImageButton;
import java.util.List;

//import static com.nshmura.recyclertablayout.demo.DemoColorPagerAdapter.Page7;


public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener,ListView.OnItemClickListener{

    protected RecyclerTabLayout mRecyclerTabLayout;

    public int NowPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("PAアプリ");

        TextView mTitle = toolbar.findViewById(R.id.title);

        setSupportActionBar(toolbar);
        mTitle.setText(toolbar.getTitle());
        mTitle.setTextSize(15);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        final ImageButton imageButton = (ImageButton)findViewById(R.id.setbutton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            /** ボタンをクリックした時に呼ばれる */
            @Override
            public void onClick(View v) {
                startset();
            }
        });

        final ImageButton backButton = (ImageButton)findViewById(R.id.backbutton);
        backButton.setOnClickListener(new View.OnClickListener() {
            /** ボタンをクリックした時に呼ばれる */
            @Override
            public void onClick(View v) {
                backwebClick();
            }
        });

        final ImageButton nextButton = (ImageButton)findViewById(R.id.nextbutton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            /** ボタンをクリックした時に呼ばれる */
            @Override
            public void onClick(View v) {
                nextwebClick();
            }
        });

        List<ColorItem> items = DemoData.loadDemoColorItems(this);

        DemoColorPagerAdapter adapter = new DemoColorPagerAdapter();
        adapter.addAll(items);
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount() - 1);
        viewPager.addOnPageChangeListener(this);
        mRecyclerTabLayout = findViewById(R.id.recycler_tab_layout);
        mRecyclerTabLayout.setUpWithViewPager(viewPager);
        NowPosition = 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }

    private void startset() {
        Intent intent = new Intent(MainActivity.this, SetActivity.class);
        startActivity(intent);
    }

    private WebView webView;
    private void backwebClick(){
        switch (NowPosition)
        {
            case 3:
                webView = (WebView)findViewById(R.id.webb2);
                break;
            default:
                break;
        }

        if(webView.canGoBack()) webView.goBack();
    }

    private void nextwebClick(){
        switch (NowPosition)
        {
            case 3:
                webView = (WebView)findViewById(R.id.webb2);
                break;
            default:
                break;
        }
        if(webView.canGoForward())
            webView.goForward();
    }

    @Override
    public void onPageSelected(int position) {
        NowPosition = position;

        if(position == 3){
            ImageButton backButton = findViewById(R.id.backbutton);
            ImageButton nextButton = findViewById(R.id.nextbutton);
            backButton.setVisibility(View.VISIBLE);
            nextButton.setVisibility(View.VISIBLE);
        }
        else{
            ImageButton backButton = findViewById(R.id.backbutton);
            ImageButton nextButton = findViewById(R.id.nextbutton);
            backButton.setVisibility(View.INVISIBLE);
            nextButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onBackPressed() {
        DemoColorPagerAdapter.Page1 = null;
        DemoColorPagerAdapter.Page2 = null;
        DemoColorPagerAdapter.Page3 = null;
        DemoColorPagerAdapter.Page4 = null;
        DemoColorPagerAdapter.Page5 = null;
        DemoColorPagerAdapter.Page6 = null;
        //Page7 = null;
        finish();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
