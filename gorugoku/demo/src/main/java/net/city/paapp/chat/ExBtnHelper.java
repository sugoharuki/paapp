package net.city.paapp.chat;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;


public class ExBtnHelper extends Button{
    // コンストラクタ
    public ExBtnHelper(Context context) {
        super(context);
    }
    public ExBtnHelper(Context context, AttributeSet attrs) {
        super(context, attrs);
        //init(context);
    }

    public ExBtnHelper(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        //init(context);
    }

    private void init(Context context) {
        //do stuff that was in your original constructor...
    }

    public void Click(final Activity act) {
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //送信処理
                sendcommentHelper.send(act);
            }
        });
    }

}
