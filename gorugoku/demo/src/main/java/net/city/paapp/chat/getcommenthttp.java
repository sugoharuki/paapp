package net.city.paapp.chat;


import net.city.paapp.Model.TimeLineComment;
import net.city.paapp.interfaces.getCommentInterface;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class getcommenthttp {

    public static List<TimeLineComment> getjson(){
        List<TimeLineComment> rr;
        String r = "";
        try {
            // 現在の時刻を取得
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("kk':'mm");
            //return  r;
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://smileapp.yokohama/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            getCommentInterface API = retrofit.create(getCommentInterface.class);
            //rr = API.getCommentapi(commentActivity.id).execute().body();
            rr = API.getCommentapi("1").execute().body();
            return rr;
        } catch (IOException e) {
            e.printStackTrace();
            //r = "err";
            return null;
        }
    }

}
