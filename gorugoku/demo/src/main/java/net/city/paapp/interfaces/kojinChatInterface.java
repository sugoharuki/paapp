package net.city.paapp.interfaces;

import net.city.paapp.Model.KojinChat;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface kojinChatInterface {
    @FormUrlEncoded
    @POST("/chat.php")
    Call<List<KojinChat>> getKojinChatapi(@Field("toname") String toname, @Field("fromname") String fromname);
}
