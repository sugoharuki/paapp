package net.city.paapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import net.city.paapp.Common.Common;
import net.city.paapp.Model.SurveyTitleModel;
import net.city.paapp.Model.bodyModel;
import net.city.paapp.Model.BroadCastModel;
import net.city.paapp.Model.shiftKibouModel;
import net.city.paapp.Model.shiftModel;
import net.city.paapp.VerModel.VerModel;
import net.city.paapp.interfaces.ApiService;
import net.city.paapp.interfaces.BroadCastApi;
import net.city.paapp.interfaces.ShiftApi;
import net.city.paapp.interfaces.ShiftKibouApi2;
import net.city.paapp.interfaces.SurveyApi;
import net.city.paapp.interfaces.Versionapi;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import android.text.format.Time;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashActivity extends AppCompatActivity{

    private static final int VERSION = 4;
    private boolean errorFlag = false;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
            CreateBaseModel();
    }

    private void CreateBaseModel(){
        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {
            }

            @Override
            public String back() {
                try {
                    Common.videoModel = CreateModel("http://220.247.10.217/id3.php/");

                    Time time = new Time("Asia/Tokyo");
                    time.setToNow();
                    //本日日付の最終日を取得
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, time.year);
                    calendar.set(Calendar.MONTH, time.month);
                    int month = time.month + 1;
                    int month_end = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                    String date_start = time.year + "-" + month + "-1";
                    String date_end = time.year + "-" + month + "-" + month_end;

                    Common.sftModel = CreateShiftModel("http://220.247.10.217/shift_check.php/",date_start,date_end);

                    Common.sftkibouModel = CreateShiftkibouModel("http://220.247.10.217/get_shift_kibou.php/");

                    Common.broadCastModel = CreateBroadCastModel("http://220.247.10.217/broadCast.php/");
                    Common.surveyTitleModel= CreateSurveyModel("http://220.247.10.217/surveyList.php/");

                } catch (IOException e) {
                    //e.printStackTrace();
                    errorFlag=true;
                }
                return null;
            }

            @Override
            public void end(String result) {
                if(errorFlag)
                    goerror();
                else
                    go();
            }
        });
    }


    public void goerror(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("通信に失敗しました");
        alertDialog.setMessage("通信状況を確認してください");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.create();
        alertDialog.show();
    }

    public void  go(){
        //VERSIONチェック
        /*if(VERSION < Integer.parseInt(Common.nowVer.ver)){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("バージョンアップして下さい。");
            alertDialog.setMessage("新しいバージョンが出ました。アップデートして下さい。");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=" + getPackageName()));
                    startActivity(intent);
                    finish();
                }
            });
            alertDialog.create();
            alertDialog.show();
        }
        else {
        */
            if(!isFinishing()) {
               startActivity(new Intent(this, MainActivity.class));
               finish();
            }
       // }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private List<bodyModel> CreateModel(String Path) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService API = retrofit.create(ApiService.class);
        return  API.apiModel().execute().body();
    }

    private List<shiftModel> CreateShiftModel(String Path,String date_start,String date_end) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ShiftApi API = retrofit.create(ShiftApi.class);
        return  API.getShiftApi(date_start,date_end).execute().body();
    }

    private List<shiftKibouModel> CreateShiftkibouModel(String Path) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ShiftKibouApi2 API = retrofit.create(ShiftKibouApi2.class);
        return  API.getShiftKibou(1).execute().body();
    }

    private List<BroadCastModel> CreateBroadCastModel(String Path) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        BroadCastApi API = retrofit.create(BroadCastApi.class);
        return  API.getMyBroadCast(1).execute().body();
    }

    private List<SurveyTitleModel> CreateSurveyModel(String Path) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SurveyApi API = retrofit.create(SurveyApi.class);
        return  API.getSurveyTitle().execute().body();
    }

    private VerModel CreateVerModel(String Path) throws IOException{
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Versionapi API = retrofit.create(Versionapi.class);
        return API.apiVerModel().execute().body();
    }
}
