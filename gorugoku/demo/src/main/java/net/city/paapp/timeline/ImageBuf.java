package net.city.paapp.timeline;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by smileapp04 on 2016/03/18.
 */
public class ImageBuf {
    public Bitmap bmp;
    public String name;

    public static ArrayList<ImageBuf> list = new ArrayList<ImageBuf>();

    public static ArrayList<String> oneslist = new ArrayList<String>();

    public static void set(Bitmap bmp,String name){
        ImageBuf obj = new ImageBuf();
        obj.bmp = bmp;
        obj.name = name;
        list.add(obj);
    };

    public static void del(String name){
        for (ImageBuf obj:list) {
            if(obj.name.equals(name)){
                list.remove(obj);
                break;
            }
        }
    };

    public static Bitmap get(String name){
        for (ImageBuf obj:list) {
            if(obj.name.equals(name)){
                return  obj.bmp;
            }
        }
        return null;
    };


}
