package net.city.paapp.chat;

import android.content.Context;
import android.content.SharedPreferences;


public class pri2 {

    public static  void saveLog(Context con ,String n,String p,String displayname){
        // プリファレンスの準備 //
        SharedPreferences pref = con.getSharedPreferences( "save2", Context.MODE_PRIVATE );
        // プリファレンスに書き込むためのEditorオブジェクト取得 //
        SharedPreferences.Editor editor = pref.edit();
        // "user_name" というキーで名前を登録
        editor.putString( "name", n );
        editor.putString( "pass", p );
        editor.putString( "display", displayname );
        // 書き込みの確定（実際にファイルに書き込む）
        editor.commit();
    }

    public static String loadLogName(Context con){
        SharedPreferences pref = con.getSharedPreferences( "save2", Context.MODE_PRIVATE );
        return pref.getString("name","");
    }

    public static String loadLogPass(Context con){
        SharedPreferences pref = con.getSharedPreferences( "save2", Context.MODE_PRIVATE );
        return pref.getString("pass","");
    }

    public static String loadDisplayname(Context con){
        SharedPreferences pref = con.getSharedPreferences( "save2", Context.MODE_PRIVATE );
        return pref.getString("display","");
    }

}
