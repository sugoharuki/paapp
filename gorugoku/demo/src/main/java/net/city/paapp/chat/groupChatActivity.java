package net.city.paapp.chat;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import net.city.paapp.MainActivity;
import net.city.paapp.Model.GroupChat;
import net.city.paapp.R;
import net.city.paapp.async.AsyncHelper;
import net.city.paapp.async.AsyncInterFace;
import net.city.paapp.async.AsyncInterFace3;
import net.city.paapp.async.UploadAsyncTask2;
import net.city.paapp.async.pushgroup;
import net.city.paapp.async.uploadInterFace;
import net.city.paapp.async.voidinter;
import net.city.paapp.interfaces.groupChatInterface;
import net.city.paapp.interfaces.sendChatInterface;
import net.city.paapp.realmModel.midoku;
import net.city.paapp.timeline.DialogHelper;
import net.city.paapp.timeline.commons;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class groupChatActivity extends Activity {


    //送信先ユーザー
    public static List<chatCustomData> objects;
    public static String targetgroupname;
    //これでぶんき
    public static String allsendflg;
    public static ListView list;
    public static chatCustomAdapter customAdapater;
    public static Context con;
    public static String upsertflg;

    public static int groupChatactivityViewFlg=0;
    @Override
    protected void onResume() {
        super.onResume();
        //非同期なので流れてしまう
        groupChatactivityViewFlg=1;
        loadlist();

        //loadlist();
    }
    @Override
    public void onPause(){
        super.onPause();
        groupChatactivityViewFlg=0;
        //Log.v("LifeCycle", "onPause");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Kii.onSaveInstanceState(outState);
    }

    //sugo 差分取得を用意する (TODO)
    public static void loadlist(){
        AsyncHelper.Async5(new AsyncInterFace3() {
            @Override
            public void start() {
            }

            @Override
            public List<GroupChat> back() {
                List<GroupChat> r = null;
                try {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://smileapp.yokohama/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    groupChatInterface API = retrofit.create(groupChatInterface.class);
                    r = API.getGroupChatapi(targetgroupname).execute().body();
                } catch (IOException e) {
                    e.printStackTrace();
                    r = null;
                }
                return r;
            }

            @Override
            public void end(List<GroupChat> result) {
                objects.clear();
                try {
                    int saisinflg=0;
                    //JSONArray arr = new JSONArray(result);
                    for (int i = 0; i < result.size(); i++) {
                        final String id = result.get(i).id;
                        String toname = result.get(i).toname;
                        String fromname = result.get(i).fromname;
                        String diplayname = result.get(i).displayname;
                        String msg = result.get(i).msg;
                        String times = result.get(i).times;
                        String imagepath = result.get(i).imagepath;
                        String iconpath = result.get(i).iconpath;
                        /*
                        String iconpath = arr.getJSONObject(i).getString("iconpath");
                        String iine = arr.getJSONObject(i).getString("iine");
                        */

                        if(saisinflg ==0){
                            if(!fromname.equals(commons.username)){
                                saisinflg=1;
                                upsertflg="0";
                                Realm.init(con);
                                Realm realm = Realm.getDefaultInstance();
                                try
                                {
                                    midoku mido = new midoku();
                                    mido.targetname = "";
                                    mido.flg = targetgroupname;
                                    mido.ids = id;
                                    realm.beginTransaction();
                                    realm.copyToRealmOrUpdate(mido);
                                    realm.commitTransaction();
                                }
                                finally
                                {
                                    realm.close();
                                }
                            }
                        }

                        chatCustomData item2 = new chatCustomData();
                        if (fromname.equals(commons.username)) {
                            item2.settime(times);
                            item2.setbody(msg + "\n");
                            item2.setflg("my");
                            item2.setimagepath(imagepath);
                            item2.seticonpath(iconpath);
                        } else {
                            item2.setname(diplayname);
                            item2.settime(times);
                            item2.setbody(msg + "\n");
                            item2.setflg("target");
                            item2.setimagepath(imagepath);
                            item2.seticonpath(iconpath);
                        }
                        objects.add(item2);

                        int aaa = 0;
                        aaa++;
                    }
                    list.setAdapter(customAdapater);

                    int aa = 0;
                    aa++;
                } catch (Exception e) {
                    int aa = 0;
                    aa++;
                    //dia.dismiss();
                }
            }
        });
    }


    String imgname;

    private void go(String path,Uri uri){
        UploadAsyncTask2.Upload(this,uri,this, path, new uploadInterFace() {
            @Override
            public void end(final String result) {
                int a = 0;
                a++;
                Log.d("hoge", result);
                if (!result.equals("err")) {
                    if (!result.equals("ng")) {
                        AsyncHelper.Async2(new AsyncInterFace() {
                            @Override
                            public void start() {

                            }

                            @Override
                            public String back() {
                                Date date = new Date();
                                SimpleDateFormat sdf = new SimpleDateFormat("kk':'mm");

                                String r = "";
                                try {
                                    Retrofit retrofit = new Retrofit.Builder()
                                            .baseUrl("http://smileapp.yokohama/")
                                            .addConverterFactory(GsonConverterFactory.create())
                                            .build();

                                    sendChatInterface API = retrofit.create(sendChatInterface.class);
                                    Void resu = API.sendChat("",commons.username,result,sdf.format(date),targetgroupname,commons.username,result,priicon.loadIcon(con)).execute().body();

                                    pushgroup.douki(targetgroupname, result);
                                    //push.douki(commons.username,targetgroupname, result,"push/iosgrouppush.php");

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    r = "err";
                                }
                                if (!r.equals("err")) {
                                }
                                return null;
                            }

                            @Override
                            public void end(String result) {
                                loadlist();
                            }
                        });
                    }
                }
            }
        }).execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        imgname = "";
        String path = "";
        try {
            Uri uri = data.getData();
            if(Build.VERSION.SDK_INT < 19) {
                String[] columns = { MediaStore.Images.Media.DATA };
                Cursor cursor = getContentResolver().query(uri, columns, null, null, null);
                cursor.moveToFirst();
                path = cursor.getString(0);
                go(path,uri);
                cursor.close();
            } else {
                String id = DocumentsContract.getDocumentId(uri);
                String selection = "_id=?";
                String[] selectionArgs = new String[]{id.split(":")[1]};

                Cursor cursor = getContentResolver().query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.MediaColumns.DATA},
                        selection, selectionArgs, null);

                if (cursor.moveToFirst()) {
                    path = cursor.getString(0);
                    go(path,uri);
                }
                cursor.close();
            }
        }catch (Exception e){

        }
    }

    public static Activity acti;

    public int flg =0;


    synchronized void methodA(){
    }


    synchronized void send(){
        String aaa = e.getText().toString();
        if(e.getText().toString().equals(""))return;
        btn.setEnabled(false);
    final String sendmsgs = e.getText().toString();
        e.setText("");
    //送信ボタンをおしたら入力を空にする
    AsyncHelper.Async2(new AsyncInterFace() {
        @Override
        public void start() {
            DialogHelper.dialog = null;
            DialogHelper.makeSimpleModalDialog(con, "送信中", "中断しますか？\n（送信する場合もあります）", new voidinter() {
                @Override
                public void act() {
                    finish();
                }
            });
            DialogHelper.dialog.show();
        }

        @Override
        public String back() {

            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("kk':'mm");

            String r = "";
            try {

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://smileapp.yokohama/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                sendChatInterface API = retrofit.create(sendChatInterface.class);
                Void resu = API.sendChat("",commons.username,sendmsgs,sdf.format(date),targetgroupname,commons.username,"nasi",priicon.loadIcon(con)).execute().body();

                pushgroup.douki(targetgroupname, sendmsgs);
                //fromname pushを飛ばさないユーザー
                // name　  ｸﾞﾙｰﾌﾟ名
                //msg　メッセージ

                //sugo 後でなおす
                //push.douki(commons.username,targetgroupname, sendmsgs,"push/iosgrouppush.php");
            } catch (IOException e) {
                e.printStackTrace();
                r = "err";
            }
            if (!r.equals("err")) {
                return "ok";
            }
            return "err";
        }

        @Override
        public void end(String result) {
            if (result.equals("err")) {
                DialogHelper.simpleDialog(con, "通信エラー", "通信に失敗しました");
            }
            btn.setEnabled(true);
            DialogHelper.dialog.dismiss();
            loadlist();
        }
    });
}

    EditText e;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Kii.onRestoreInstanceState(savedInstanceState);
        con = this;
        acti = this;
        setContentView(R.layout.group_chat_activity);

        String username = pri2.loadDisplayname(this);
        String password = pri2.loadLogPass(this);
        String name = pri2.loadLogName(this);

        commons.setUserData(username, password, name);
        //commons.syainid =

        Intent intent = getIntent();
        if(intent.getStringExtra("DATA") != null && !intent.getStringExtra("DATA").equals(""))
        {
            targetgroupname = intent.getStringExtra("DATA");
            //MainActivity.seticon();
        }

        list = (ListView)findViewById(R.id.groupchatListView);
        objects = new ArrayList<chatCustomData>();
        customAdapater = new chatCustomAdapter(this, 0, objects);
        //customAdapater.setData(objects,list,customAdapater);


        TextView t = (TextView)findViewById(R.id.groupname);
        t.setText(targetgroupname);

        e = (EditText)findViewById(R.id.groupmsg);

        btn = (Button)findViewById(R.id.groupmsgsend);


        Button groupplus = (Button)findViewById(R.id.groupplus);
        groupplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// ギャラリー呼び出し
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, 0);
            }
        });

        //送信ボタン
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send();
            }
        });

        loadlist();

        Button   ok = (Button)findViewById(R.id.shoutaibtn);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView str = (TextView) findViewById(R.id.groupname);
                GroupMenberActivity.str = str.getText().toString();
                Intent intent = new Intent(groupChatActivity.this, GroupMenberActivity.class);
                intent.putExtra("gamenname","groupChatActivity");
                // 次画面のアクティビティ起動
                startActivity(intent);
            }
        });
    }
}
