package net.city.paapp.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface sendChatInterface {
    @FormUrlEncoded
    @POST("/sendchat.php")
    Call<Void> sendChat(@Field("toname") String toname, @Field("fromname") String fromname, @Field("msg") String msg,
                        @Field("times") String times, @Field("flg") String flg, @Field("displayname") String displayname,
                        @Field("imagepath") String imagepath, @Field("iconpath") String iconpath);
}
