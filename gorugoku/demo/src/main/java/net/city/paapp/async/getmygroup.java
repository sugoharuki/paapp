package net.city.paapp.async;

import net.city.paapp.Model.MyGroup;
import net.city.paapp.interfaces.myGroupInterface;
import net.city.paapp.timeline.commons;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class getmygroup {

    public static List<MyGroup> douki(){
                try {

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://smileapp.yokohama/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    myGroupInterface API = retrofit.create(myGroupInterface.class);
                    return API.mygroupapi(commons.username).execute().body();
                } catch (Exception ex) {
                    return  null;
                }
    }

    public static  List<MyGroup>  douki2(String targetname){
        try {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://smileapp.yokohama/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            myGroupInterface API = retrofit.create(myGroupInterface.class);
            return API.mygroupapi(targetname).execute().body();
        } catch (Exception ex) {
            return  null;
        }
    }

    /*public static  List<Syozoku>  syozoku(){
        try {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://smileapp.yokohama/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            syozokuInterface API = retrofit.create(syozokuInterface.class);
            return API.getSyozoku().execute().body();
        } catch (Exception ex) {
            return  null;
        }
    }*/

}
