package net.city.paapp.interfaces;

import net.city.paapp.Model.shiftKibouModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ShiftKibouApi2 {
    @FormUrlEncoded
    @POST(".")
    Call<List<shiftKibouModel>> getShiftKibou(@Field("user_id") int user_id);
}
