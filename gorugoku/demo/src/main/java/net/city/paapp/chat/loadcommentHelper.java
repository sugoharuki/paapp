package net.city.paapp.chat;

import android.app.Activity;
import android.view.View;
import android.widget.ListView;


import net.city.paapp.Model.TimeLineComment;
import net.city.paapp.R;
import net.city.paapp.async.AsyncHelper;
import net.city.paapp.async.AsyncInterFace6;

import java.util.ArrayList;
import java.util.List;


public class loadcommentHelper {

    public static void loadlist(final Activity act){
        AsyncHelper.Async8(new AsyncInterFace6() {
            @Override
            public void start() {

            }

            @Override
            public List<TimeLineComment> back() {
                return getcommenthttp.getjson();
            }

            @Override
            public void end(List<TimeLineComment> result) {
                if(result.equals(null))return;

                View v =  act.getWindow().getDecorView();
                ListView list = (ListView)v.findViewById(R.id.dtlListView);
                List<chatCustomData>  objects = new ArrayList<chatCustomData>();
                objects.clear();
                chatCustomAdapter customAdapater = new chatCustomAdapter(act, 0, objects);

                try{
                    //JSONArray arr = new JSONArray(result);
                    for (int i=0; i<result.size(); i++){
                        String sendname = result.get(i).sendname;
                        String msg = result.get(i).msg;
                        chatCustomData item2 = new chatCustomData();
                        item2.settime("");
                        item2.setbody(msg);
                        item2.setflg("target");
                        item2.setname(sendname);
                        item2.setimagepath("nasi");
                        objects.add(item2);
                    }
                    list.setAdapter(customAdapater);
                }catch (Exception e){
                }
            }
        });
    }

}
