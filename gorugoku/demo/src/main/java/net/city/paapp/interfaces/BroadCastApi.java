package net.city.paapp.interfaces;

import net.city.paapp.Model.BroadCastModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface BroadCastApi {
    @FormUrlEncoded
    @POST(".")
    Call<List<BroadCastModel>> getMyBroadCast(@Field("user_id") int user_id);
}
