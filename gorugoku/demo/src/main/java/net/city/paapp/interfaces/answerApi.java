package net.city.paapp.interfaces;

import net.city.paapp.Model.answerModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface answerApi {
    @GET(".")
    Call<List<answerModel>> getAnswerModel();
}
