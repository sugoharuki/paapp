package net.city.paapp.Common;

import android.graphics.Color;

import net.city.paapp.Model.SurveyTitleModel;
import net.city.paapp.Model.BroadCastModel;
import net.city.paapp.Model.answerModel;
import net.city.paapp.Model.contentModel;
import net.city.paapp.Model.shiftKibouModel;
import net.city.paapp.Model.shiftModel;
import net.city.paapp.Model.bodyModel;
import net.city.paapp.VerModel.VerModel;

import java.util.List;

public class Common {
    public static VerModel nowVer;
    public static List<bodyModel> videoModel;
    public static float mTextSize = 0;
    public static List<shiftModel> sftModel;
    public static List<shiftKibouModel> sftkibouModel;
    public static List<SurveyTitleModel> surveyTitleModel;
    public static List<BroadCastModel> broadCastModel;
    public static List<contentModel> ContentModel;
    public static List<answerModel> AnswerModel;

    public static int haya = Color.rgb(255, 255, 153);
    public static int oso = Color.rgb(255, 204, 153);
    public static int sinnya = Color.rgb(204, 255, 255);
    public static int haya2 = Color.argb(123,255, 255, 153);
    public static int oso2 = Color.argb(123,255, 204, 153);
    public static int sinnya2 = Color.argb(123,204, 255, 255);
    public static int yuukyu = Color.rgb(192, 192, 192);
    public static int kekkin = Color.rgb(51, 51, 51);

}
