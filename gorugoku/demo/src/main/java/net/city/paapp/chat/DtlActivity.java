package net.city.paapp.chat;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import net.city.paapp.MainActivity;
import net.city.paapp.Model.KojinChat;
import net.city.paapp.R;
import net.city.paapp.async.AsyncHelper;
import net.city.paapp.async.AsyncInterFace;
import net.city.paapp.async.AsyncInterFace4;
import net.city.paapp.async.UploadAsyncTask2;
import net.city.paapp.async.push;
import net.city.paapp.async.uploadInterFace;
import net.city.paapp.async.voidinter;
import net.city.paapp.interfaces.kojinChatInterface;
import net.city.paapp.interfaces.sendChatInterface;
import net.city.paapp.realmModel.midoku;
import net.city.paapp.timeline.DialogHelper;
import net.city.paapp.timeline.commons;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class DtlActivity extends Activity {


    //送信先ユーザー
    public static String target;
    public static String targetDisplayName;


    public static ListView list;
    public static chatCustomAdapter customAdapater;
    public static List<chatCustomData> objects;


    public static Context con;
    String imgname;
    public static String upsertflg;
     String str="";

    private void go(String path,Uri uri){
        UploadAsyncTask2.Upload(this,uri,this, path, new uploadInterFace() {
            @Override
            public void end(final String result) {
                int a = 0;
                a++;
                Log.d("hoge",result);
                if(!result.equals("err")){
                    if(!result.equals("ng")){
                        AsyncHelper.Async2(new AsyncInterFace() {
                            @Override
                            public void start() {

                            }

                            @Override
                            public String back() {
                                //KiiPushMessage.Data data = new KiiPushMessage.Data();
                                //Date date = new Date();
                                // 現在の時刻を取得
                                Date date = new Date();
                                //SimpleDateFormat sdf = new SimpleDateFormat("yyyy'年'MM'月'dd'日'　kk'時'mm'分'ss'秒'");
                                SimpleDateFormat sdf = new SimpleDateFormat("kk':'mm");
                                //data.put("displayname", commons.username);
                                //data.put("toName", DtlActivity.targetDisplayName);
                                //data.put("fromName", commons.username);
                                //data.put("msg", result);
                                //data.put("time", "" + sdf.format(date));
                                //data.put("imagepath",result);*/
                                String r ="";
                                try {
                                    Retrofit retrofit = new Retrofit.Builder()
                                            .baseUrl("http://smileapp.yokohama/")
                                            .addConverterFactory(GsonConverterFactory.create())
                                            .build();

                                    sendChatInterface API = retrofit.create(sendChatInterface.class);
                                    Void resu = API.sendChat(DtlActivity.targetDisplayName,
                                            commons.username,
                                            result,
                                            sdf.format(date),
                                            "",
                                            commons.username,
                                            result,
                                            priicon.loadIcon(con)
                                    ).execute().body();
                                    //sugo push通知 アンドロイド
                                    push.androiddouki(commons.username,DtlActivity.targetDisplayName,"画像受信");
                                    //push.douki(commons.username,DtlActivity.targetDisplayName,"画像受信","push/sample_push.php");

                                }catch (IOException e) {
                                    e.printStackTrace();
                                    r = "err";
                                }
                                if(!r.equals("err")){
                                }
                                return null;
                            }

                            @Override
                            public void end(String result) {
                                loadlist();
                            }
                        });
                    }
                }
            }
        }).execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        imgname = "";
        String path = "";
        try {
            Uri uri = data.getData();
            if(Build.VERSION.SDK_INT < 19) {
                String[] columns = { MediaStore.Images.Media.DATA };
                Cursor cursor = getContentResolver().query(uri, columns, null, null, null);
                cursor.moveToFirst();
                path = cursor.getString(0);
                go(path,uri);
                cursor.close();
            } else {
                String id = DocumentsContract.getDocumentId(uri);
                String selection = "_id=?";
                String[] selectionArgs = new String[]{id.split(":")[1]};

                Cursor cursor = getContentResolver().query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.MediaColumns.DATA},
                        selection, selectionArgs, null);

                if (cursor.moveToFirst()) {
                    path = cursor.getString(0);
                    go(path,uri);
                }
                cursor.close();
            }
        }catch (Exception e){

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Kii.onSaveInstanceState(outState);
    }

    public static int DtlactivityViewFlg=0;
    @Override
    protected void onResume() {
        super.onResume();
        //非同期なので流れてしまう
        DtlactivityViewFlg=1;
        DtlActivity.loadlist();
    }
    @Override
    public void onPause(){
        super.onPause();
        DtlactivityViewFlg=0;
        //Log.v("LifeCycle", "onPause");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        finish();
    }

    public static void loadlist(){
        AsyncHelper.Async6(new AsyncInterFace4() {
            @Override
            public void start() {
            }

            @Override
            public List<KojinChat> back() {
                String r = "";
                try {

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://smileapp.yokohama/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    kojinChatInterface API = retrofit.create(kojinChatInterface.class);
                    List<KojinChat> resu = API.getKojinChatapi(DtlActivity.targetDisplayName,commons.username).execute().body();
                    return resu;

                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            @Override
            public void end(List<KojinChat> result) {
                objects.clear();
                try{
                    int saisinflg=0;
                    for (int i=0; i<result.size(); i++){
                        final String id = result.get(i).id;
                        String toname = result.get(i).toname;
                        final String fromname = result.get(i).fromname;
                        String diplayname = result.get(i).displayname;
                        String msg = result.get(i).msg;
                        String times = result.get(i).times;
                        String imagepath = result.get(i).imagepath;
                        String iconpath = result.get(i).iconpath;

                        if(saisinflg ==0){
                            if(toname.equals(commons.username)){
                                saisinflg=1;
                                upsertflg="0";
                                Realm.init(con);
                                Realm realm = Realm.getDefaultInstance();
                                try
                                {
                                    midoku mido = new midoku();
                                    mido.targetname = fromname;
                                    mido.flg = "";
                                    mido.ids = id;
                                    realm.beginTransaction();
                                    realm.copyToRealmOrUpdate(mido);
                                    realm.commitTransaction();
                                }
                                finally
                                {
                                    realm.close();
                                }
                            }
                        }

                        chatCustomData item2 = new chatCustomData();
                        if(fromname.equals(commons.username)){
                            item2.settime(times);
                            item2.setbody(msg + "\n");
                            item2.setflg("my");
                            item2.setimagepath(imagepath);
                            item2.seticonpath(iconpath);
                        }else{
                            item2.setname(diplayname);
                            item2.settime(times);
                            item2.setbody(msg + "\n");
                            item2.setflg("target");
                            item2.setimagepath(imagepath);
                            item2.seticonpath(iconpath);
                        }
                        objects.add(item2);

                        int aaa=0;
                        aaa++;
                    }
                    list.setAdapter(customAdapater);
                    int aa=0;
                    aa++;
                }catch (Exception e){
                    int aa=0;
                    aa++;
                    //dia.dismiss();
                }
            }
        });
    }


    public static Activity acti;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Kii.onRestoreInstanceState(savedInstanceState);
        con = this;
        acti = this;

        setContentView(R.layout.dtl_activity);
        TextView t = (TextView)findViewById(R.id.groupname);
        final EditText e = (EditText)findViewById(R.id.msg);
        Button btn = (Button)findViewById(R.id.msgsend);

        list = (ListView)findViewById(R.id.dtlListView);
        objects = new ArrayList<chatCustomData>();
        customAdapater = new chatCustomAdapter(this, 0, objects);

        String username = pri2.loadDisplayname(this);
        String password = pri2.loadLogPass(this);
        String name = pri2.loadLogName(this);

        commons.setUserData(username, password, name);

        Intent intent = getIntent();
        if(intent.getStringExtra("DATA") != null && !intent.getStringExtra("DATA").equals(""))
        {
            targetDisplayName = intent.getStringExtra("DATA");
            //MainActivity.seticon();
        }

        Button plusimg = (Button)findViewById(R.id.plusimg);
        plusimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ギャラリー呼び出し
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, 0);
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(e.getText().toString().equals(""))return;
                //ログイン確認
                final Button btns =(Button)v;
                btns.setEnabled(false);
                AsyncHelper.Async2(new AsyncInterFace() {
                    @Override
                    public void start() {
                        str = e.getText().toString();
                        DialogHelper.dialog = null;
                        DialogHelper.makeSimpleModalDialog(con, "送信中", "中断しますか？\n（送信してしまう場合もあります）", new voidinter() {
                            @Override
                            public void act() {
                                finish();
                            }
                        });
                        DialogHelper.dialog.show();
                    }

                    @Override
                    public String back() {
                        // 現在の時刻を取得
                        Date date = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("kk':'mm");

                        String r = "";
                        try {
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl("http://smileapp.yokohama/")
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();

                            sendChatInterface API = retrofit.create(sendChatInterface.class);
                            Void resu = API.sendChat(DtlActivity.targetDisplayName,commons.username,str,sdf.format(date),"",commons.username,"nasi",priicon.loadIcon(con)).execute().body();

                            //sugo プッシュ通知 アンドロイドのみ
                            //String token = getresid.douki(DtlActivity.targetDisplayName);
                            push.androiddouki(commons.username,DtlActivity.targetDisplayName,str);
                            //push.douki(commons.username,DtlActivity.targetDisplayName,str,"push/sample_push.php");

                        } catch (IOException e) {
                            e.printStackTrace();
                            r = "err";
                        }
                        if (!r.equals("err")) {
                            return "ok";
                        }
                        return "err";
                    }

                    @Override
                    public void end(String result) {
                        if (result.equals("err")) {
                            DialogHelper.simpleDialog(con, "通信エラー", "通信に失敗しました");
                        } else {
                            e.setText("");
                        }
                        btns.setEnabled(true);
                        loadlist();
                        DialogHelper.dialog.dismiss();
                    }
                });
            }
        });

        t.setText(targetDisplayName);
        loadlist();

        Button   ok = (Button)findViewById(R.id.shoutaibtn);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView str = (TextView) findViewById(R.id.groupname);
                GroupMenberActivity.str = str.getText().toString();
                Intent intent = new Intent(DtlActivity.this, AddGroupActivity.class);
                //intent.putExtra("gamenname","DtlActivity");
                // 次画面のアクティビティ起動
                startActivity(intent);
            }
        });

    }
}
