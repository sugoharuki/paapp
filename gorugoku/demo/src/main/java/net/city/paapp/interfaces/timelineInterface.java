package net.city.paapp.interfaces;

import net.city.paapp.Model.timeline;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface timelineInterface {
        //@Headers("Content-Type: application/json")
        @GET("/timeline.php")
        Call<List<timeline>> gettimeline();
}
