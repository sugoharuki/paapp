package net.city.paapp;

import net.city.paapp.WebView.WebActivity;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SetActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private static final String[] SetingListItem = {
        "お問い合わせ","プライバシーポリシー","株式会社ＭＧＰ","株式会社シティコミュニケーションズ","株式会社スマイルアップ"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);

        setContentView(R.layout.activity_set);

        Toolbar toolbar = findViewById(R.id.toolbar2);
        TextView mTitle = toolbar.findViewById(R.id.title2);
        mTitle.setText("ゴルフの極意");


        ListView listView = findViewById(R.id.set_list);

        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,SetingListItem);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(this);

    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch(position)
        {
            case 0:
                CreateWeb("https://smileapp.biz/contact");
                break;
            case 1:
                CreateWeb("https://smileapp.biz/%E3%83%97%E3%83%A9%E3%82%A4%E3%83%90%E3%82%B7%E3%83%BC%E3%83%9D%E3%83%AA%E3%82%B7%E3%83%BC");
                break;
            case 2:
                CreateWeb("http://www.vintage-golf.net/");
                break;
            case 3:
                CreateWeb("http://www.city-s.co.jp/");
                break;
            case 4:
                CreateWeb("https://smileapp.biz/");
                break;
            default:
                break;

        }

    }

    private void CreateWeb(String Path) {
        Intent intent = new Intent(this, WebActivity.class);
        intent.putExtra("Path",Path);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
