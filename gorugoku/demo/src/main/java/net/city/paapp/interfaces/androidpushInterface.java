package net.city.paapp.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface androidpushInterface {
    @FormUrlEncoded
    @POST("/firebasepushandroid.php")
    //@POST("/firebasepush.php")
    Call<String> androidpush(@Field("fromname") String fromname, @Field("targetname") String targetname, @Field("msg") String msg, @Field("talktype") String talktype);
}
