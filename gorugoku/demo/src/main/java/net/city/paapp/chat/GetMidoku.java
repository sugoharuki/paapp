package net.city.paapp.chat;

import android.os.AsyncTask;

import net.city.paapp.Model.KojinChat;
import net.city.paapp.interfaces.ResultInterface2;
import net.city.paapp.interfaces.getLatestChatInterface;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GetMidoku {

    public static  void set(final String jsons ,final ResultInterface2 obj){

        new AsyncTask<Void, Void, List<KojinChat>>() {
            @Override
            protected List<KojinChat> doInBackground(Void... params) {
                try {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://smileapp.yokohama/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    getLatestChatInterface API = retrofit.create(getLatestChatInterface.class);
                    List<KojinChat> resu = API.getLatestChatapi(jsons).execute().body();

                    int a=0;
                    a++;
                    return resu;
                } catch (Exception ex) {
                    return  null;
                }
            }

            @Override
            protected void onPostExecute(List<KojinChat> r) {
                obj.result(r);
            }
        }.execute(null, null, null);

    }

}
