package net.city.paapp;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import net.city.paapp.Common.Common;
import net.city.paapp.Model.shiftModel;
import net.city.paapp.group.GroupRecyclerAdapter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 适配器
 * Created by huanghaibin on 2017/12/4.
 */

public class ArticleAdapter extends GroupRecyclerAdapter<String, Article> {


    private RequestManager mLoader;

    public ArticleAdapter(Context context,String year,String month,String day) {
        super(context);
        mLoader = Glide.with(context.getApplicationContext());
        LinkedHashMap<String, List<Article>> map = new LinkedHashMap<>();
        List<String> titles = new ArrayList<>();
        map.put("その日付に入っている人", create(year,month,day));
        resetGroups(map,titles);
    }


    @Override
    protected RecyclerView.ViewHolder onCreateDefaultViewHolder(ViewGroup parent, int type) {
        return new ArticleViewHolder(mInflater.inflate(R.layout.item_list_article, parent, false));
    }

    @Override
    protected void onBindViewHolder(RecyclerView.ViewHolder holder, Article item, int position) {
        ArticleViewHolder h = (ArticleViewHolder) holder;
        h.mTextTitle.setText(item.getTitle());
        h.mTextContent.setText(item.getContent());
        String[] rgbstr = item.getColor().split(",");
        int r = Integer.parseInt(rgbstr[0]);
        int g = Integer.parseInt(rgbstr[1]);
        int b = Integer.parseInt(rgbstr[2]);
        h.mLinearLayout.setBackgroundColor(Color.rgb(r,g,b));
        /*mLoader.load(item.getImgUrl())
                .asBitmap()
                .centerCrop()
                .into(h.mImageView);*/
    }

    private static class ArticleViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextTitle,
                mTextContent;
        private LinearLayout mLinearLayout;

        private ArticleViewHolder(View itemView) {
            super(itemView);
            mTextTitle = (TextView) itemView.findViewById(R.id.tv_title);
            mTextContent = (TextView) itemView.findViewById(R.id.tv_content);
            mLinearLayout = (LinearLayout)itemView.findViewById(R.id.article_list);

        }
    }


    private static Article create(String title, String content,String colort_in,String color_out) {
        Article article = new Article();
        article.setTitle(title);
        article.setContent(content);
        article.setColor(colort_in);
        return article;
    }

    private static List<Article> create(String year,String month ,String day) {
        List<Article> list = new ArrayList<>();
        month = String.format("%02d",Integer.parseInt(month));
        day = String.format("%02d",Integer.parseInt(day));
        String selectDate = year+"-"+month+"-"+day;
        for(shiftModel shift : Common.sftModel)
        {
            String date = shift.date;

            if(shift.date.equals(selectDate))
            {

                list.add(create(shift.user_name,shift.time_in + "～" + shift.time_out,shift.in_color,shift.out_color));
            }
        }

        return list;
    }
}
