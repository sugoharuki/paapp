package net.city.paapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import net.city.paapp.group.GroupRecyclerAdapter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 适配器
 * Created by huanghaibin on 2017/12/4.
 */

public class ArticleAdapter2 extends GroupRecyclerAdapter<String, Article2> {


    private RequestManager mLoader;

    public ArticleAdapter2(Context context) {
        super(context);
        mLoader = Glide.with(context.getApplicationContext());
        LinkedHashMap<String, List<Article2>> map = new LinkedHashMap<>();
        List<String> titles = new ArrayList<>();
        map.put("ボタンリスト",create());
        resetGroups(map,titles);
    }


    @Override
    protected RecyclerView.ViewHolder onCreateDefaultViewHolder(ViewGroup parent, int type) {
        return new ArticleViewHolder(mInflater.inflate(R.layout.item_list_article2, parent, false));
    }

    @Override
    protected void onBindViewHolder(RecyclerView.ViewHolder holder, Article2 item, int position) {
        ArticleViewHolder h = (ArticleViewHolder) holder;
        h.mButtonTitle.setText(item.getTitle());
    }

    private static class ArticleViewHolder extends RecyclerView.ViewHolder {
        private Button mButtonTitle;

        private ArticleViewHolder(View itemView) {
            super(itemView);
            mButtonTitle = (Button) itemView.findViewById(R.id.tv_button);
        }
    }


    private static Article2 create(String title) {
        Article2 article = new Article2();
        article.setTitle(title);
        return article;
    }

    private static List<Article2> create() {
        List<Article2> list = new ArrayList<>();

        list.add(create("早番"));
        list.add(create("中番"));
        list.add(create("遅番"));
        list.add(create("深夜番"));
        list.add(create("全て"));
        list.add(create("クリア"));
        list.add(create("備考入力"));
        list.add(create("登録"));

        return list;
    }
}
