package net.city.paapp.async;

import android.os.AsyncTask;

import net.city.paapp.Model.GroupChat;
import net.city.paapp.Model.KojinChat;
import net.city.paapp.Model.TimeLineComment;
import net.city.paapp.Model.timeline;

import java.util.List;



public class AsyncHelper {

    public static  void Async2(final AsyncInterFace v){
        //ブロッキングといえど非同期を使う
        new AsyncTask<Void, Void, String>() {
            //uiスレッドにて最初にやる処理
            @Override
            protected void onPreExecute() {
                v.start();
            }
            //バック完了後にuiスレッドにて最後にやる処理

            @Override
            protected void onPostExecute(String result) {
                v.end(result);
            }

            @Override
            protected String doInBackground(Void... params) {
                try {
                    return v.back();
                } catch (Exception e) {
                    return "ng";
                }
            }
        }.execute();
    }

    //共有防止？
    public static  AsyncTask<Void, Void, String> Async3(final AsyncInterFace v){
        //ブロッキングといえど非同期を使う
        return  new AsyncTask<Void, Void, String>() {
            //uiスレッドにて最初にやる処理
            @Override
            protected void onPreExecute() {
                v.start();
            }
            //バック完了後にuiスレッドにて最後にやる処理

            @Override
            protected void onPostExecute(String result) {
                v.end(result);
            }

            @Override
            protected String doInBackground(Void... params) {
                try {
                    return v.back();
                } catch (Exception e) {
                    return null;
                }
            }
        };
    }

    public static  AsyncTask<Void, Void, List<timeline>> Async4(final AsyncInterFace2 v){
        //ブロッキングといえど非同期を使う
        return  new AsyncTask<Void, Void, List<timeline>>() {
            //uiスレッドにて最初にやる処理
            @Override
            protected void onPreExecute() {
                v.start();
            }
            //バック完了後にuiスレッドにて最後にやる処理

            @Override
            protected void onPostExecute(List<timeline> result) {
                v.end(result);
            }

            @Override
            protected List<timeline> doInBackground(Void... params) {
                try {
                    return v.back();
                } catch (Exception e) {
                    return null;
                }
            }
        };
    }

    public static  void Async8(final AsyncInterFace6 v){
        //ブロッキングといえど非同期を使う
        new AsyncTask<Void, Void, List<TimeLineComment>>() {
            //uiスレッドにて最初にやる処理
            @Override
            protected void onPreExecute() {
                v.start();
            }
            //バック完了後にuiスレッドにて最後にやる処理

            @Override
            protected void onPostExecute(List<TimeLineComment> result) {
                v.end(result);
            }

            @Override
            protected List<TimeLineComment> doInBackground(Void... params) {
                try {
                    return v.back();
                } catch (Exception e) {
                    return null;
                }
            }
        }.execute();
    }

    public static  void Async5(final AsyncInterFace3 v){
        //ブロッキングといえど非同期を使う
        new AsyncTask<Void, Void, List<GroupChat>>() {
            //uiスレッドにて最初にやる処理
            @Override
            protected void onPreExecute() {
                v.start();
            }
            //バック完了後にuiスレッドにて最後にやる処理

            @Override
            protected void onPostExecute(List<GroupChat> result) {
                v.end(result);
            }

            @Override
            protected List<GroupChat> doInBackground(Void... params) {
                try {
                    return v.back();
                } catch (Exception e) {
                    return null;
                }
            }
        }.execute();
    }

    public static  void Async6(final AsyncInterFace4 v){
        //ブロッキングといえど非同期を使う
        new AsyncTask<Void, Void, List<KojinChat>>() {
            //uiスレッドにて最初にやる処理
            @Override
            protected void onPreExecute() {
                v.start();
            }
            //バック完了後にuiスレッドにて最後にやる処理

            @Override
            protected void onPostExecute(List<KojinChat> result) {
                v.end(result);
            }

            @Override
            protected List<KojinChat> doInBackground(Void... params) {
                try {
                    return v.back();
                } catch (Exception e) {
                    return null;
                }
            }
        }.execute();
    }
}
