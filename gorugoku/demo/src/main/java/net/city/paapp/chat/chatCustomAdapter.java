package net.city.paapp.chat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import net.city.paapp.R;
import net.city.paapp.async.ImageAsyncTask2;
import net.city.paapp.async.ImageManager;
import net.city.paapp.async.inter;
import net.city.paapp.timeline.ImageBuf;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;



public class chatCustomAdapter extends ArrayAdapter<chatCustomData> {
    private LayoutInflater layoutInflater_;
    //Bitmap oBmp;
    Context ccc;
    Handler _handler = new Handler();
    Runnable ru;

    // 画像保存
    private void saveImage(ImageView img) {
        // イメージビューからビットマップ保持
        //ImageView sampleImageView = (ImageView)findViewById(R.id.image);
        Bitmap sampleImage = ((BitmapDrawable)img.getDrawable()).getBitmap();

        // ビットマップを SDカードに保存
        ImageManager imageManager = new ImageManager(ccc);
        try {
            // 画像の保存実行
            String albumName = "Save image sample";
            imageManager.save(sampleImage, albumName);
        } catch (Error e) {
            Log.e("MainActivity", "onCreate: " + e);

            // 画像の保存失敗メッセージ表示
            Toast.makeText(ccc, "SDカードに保存できませんでした", Toast.LENGTH_SHORT).show();
        } finally {
            // 画像の保存完了メッセージ表示
            Toast.makeText(ccc, "SDカードに保存しました", Toast.LENGTH_SHORT).show();
        }
    }

    public chatCustomAdapter(Context context, int textViewResourceId, List<chatCustomData> objects) {
        super(context, textViewResourceId, objects);
        ccc = context;
        layoutInflater_ = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public Object fetch(String address) throws MalformedURLException,IOException {
        URL url = new URL(address);
        Object content = url.getContent();
        return content;
    }

    private void imageup(final ImageView imageView,final String path){

        if(ImageBuf.get(path)!=null){
            Bitmap oBmp = ImageBuf.get(path);

            if(oBmp == null) return;

//            //表示利用域に合わせたサイズを計算
//            float scale = commons.getFitScale(imageView.getWidth(), imageView.getHeight(), oBmp.getWidth(), oBmp.getHeight());
//
//            if(scale == 0.0)
//            {
//                return;
//            }
//
//            //リサイズマトリクス
//            Matrix matrix = new Matrix();
//            matrix.postScale(scale, scale);

            //ビットマップ作成
            //oBmp = Bitmap.createBitmap(oBmp, 0, 0, oBmp.getWidth(), oBmp.getHeight(), matrix, true);

//            float www = 400.0f / oBmp.getHeight() * oBmp.getWidth();
//            float ws = Math.round(www);
//            int ww = (int) ws;
//            oBmp = Bitmap.createScaledBitmap(oBmp, ww, 400, false);

            imageView.setImageBitmap(oBmp);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final ImageView img = (ImageView) v;
                    new AlertDialog.Builder(ccc)
                            .setTitle("画像保存")
                            .setMessage("画像を保存しますか？")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    saveImage((ImageView) img);
                                }
                            })
                            .setNegativeButton("Cancel", null)
                            .show();
                }
            });
            return;
        }

        new ImageAsyncTask2(ccc, new inter() {
            @Override
            public void first() {
            }

            @Override
            public Bitmap act() {
                URL url;
                InputStream istream;
                try {
                    //url = new URL("https://www.gstatic.com/android/market_images/web/play_logo_x2.png");
                    //url = new URL("http://122.223.132.189:81/files/"+"test001.jpeg");
                    //url = new URL("http://122.223.132.189:81/files/"+item.getimagepath());
                    //url = new URL("http://122.223.132.189:81/files/"+path);
                    url = new URL("http://smileapp.yokohama/files/"+path);
                    //インプットストリームで画像を読み込む
                    istream = url.openStream();
                    //読み込んだファイルをビットマップに変換
                    Bitmap oBmp = BitmapFactory.decodeStream(istream);
                    istream.close();
                    return oBmp;
                } catch (IOException e) {
                    // TODO 自動生成された catch ブロック
                    e.printStackTrace();
                    return  null;
                }
            }

            @Override
            public void end(Bitmap r)
            {

                if(r == null) return;


                ImageBuf.set(r,path);



                imageView.setImageBitmap(r);

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final ImageView img =(ImageView)v;
                        new AlertDialog.Builder(ccc)
                                .setTitle("画像保存")
                                .setMessage("画像を保存しますか？")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveImage((ImageView)img);
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .show();
                    }
                });
                int a=0;
                a++;
            }
        }).execute();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 特定の行(position)のデータを得る
        final chatCustomData item = (chatCustomData)getItem(position);

        // convertViewは使い回しされている可能性があるのでnullの時だけ新しく作る
        if (null == convertView) {
            convertView = layoutInflater_.inflate(R.layout.chat3, null);
        }


        View my  =  (View)convertView.findViewById(R.id.meitem);
         my.setVisibility(View.GONE);
         View target  =  (View)convertView.findViewById(R.id.targetitem);
         target.setVisibility(View.GONE);


        if(item.getflg().equals("my")){
            my.setVisibility(View.VISIBLE);
            TextView mytimes = (TextView)convertView.findViewById(R.id.mytimes);
            mytimes.setText(item.gettime());
            TextView body = (TextView)convertView.findViewById(R.id.mybody);
            body.setText(item.getbody());
            final ImageView imageView;
            imageView = (ImageView)convertView.findViewById(R.id.myimage);
            imageView.setVisibility(View.GONE);
            if(!item.getimagepath().equals("nasi")){
                imageView.setVisibility(View.VISIBLE);

                imageup(imageView,item.getimagepath());

            };
        }else{
            target.setVisibility(View.VISIBLE);

            final ImageView icons  = (ImageView)convertView.findViewById(R.id.チャットアイコン);


            //icons.setVisibility(View.INVISIBLE);
            if(item.geticonpath() != null && !item.geticonpath().equals(""))
            {
                if (ImageBuf.get(item.geticonpath()) != null && item.geticonpath().equals("deficon.png"))
                {
                    Bitmap r = ImageBuf.get(item.geticonpath());

                    // 画像サイズ取得
                    int width  = r.getWidth();
                    int height = r.getHeight();

                    // リサイズ後サイズ
                    int w = 100;
                    int h = 100;

                    // 切り取り領域となるbitmap生成
                    Bitmap clipArea = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

                    // 角丸矩形を描写
                    Canvas c = new Canvas(clipArea);
                    c.drawRoundRect(new RectF(0, 0, w, h), 50, 50, new Paint(Paint.ANTI_ALIAS_FLAG));

                    // 角丸画像となるbitmap生成
                    Bitmap newImage = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

                    // 切り取り領域を描写
                    Canvas canvas = new Canvas(newImage);
                    Paint paint = new Paint();
                    canvas.drawBitmap(clipArea, 0, 0, paint);

                    // 切り取り領域内にオリジナルの画像を描写
                    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                    canvas.drawBitmap(r, new Rect(0, 0, width, height), new Rect(0, 0, w, h), paint);

                    //icons.setImageBitmap(ImageBuf.get(item.geticonpath()));
                    icons.setImageBitmap(newImage);
                }
                else if (ImageBuf.get(item.geticonpath()) != null && !item.geticonpath().equals("deficon.png"))
                {
                    if (ImageBuf.get(item.geticonpath()) != null)
                    {
                        Bitmap r = ImageBuf.get(item.geticonpath());

                        // 画像サイズ取得
                        int width  = r.getWidth();
                        int height = r.getHeight();

                        // リサイズ後サイズ
                        int w = 100;
                        int h = 100;

                        // 切り取り領域となるbitmap生成
                        Bitmap clipArea = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

                        // 角丸矩形を描写
                        Canvas c = new Canvas(clipArea);
                        c.drawRoundRect(new RectF(0, 0, w, h), 50, 50, new Paint(Paint.ANTI_ALIAS_FLAG));

                        // 角丸画像となるbitmap生成
                        Bitmap newImage = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

                        // 切り取り領域を描写
                        Canvas canvas = new Canvas(newImage);
                        Paint paint = new Paint();
                        canvas.drawBitmap(clipArea, 0, 0, paint);

                        // 切り取り領域内にオリジナルの画像を描写
                        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                        canvas.drawBitmap(r, new Rect(0, 0, width, height), new Rect(0, 0, w, h), paint);

                        //icons.setImageBitmap(ImageBuf.get(item.geticonpath()));
                        icons.setImageBitmap(newImage);
                    }
                }
                else
                {
                    new ImageAsyncTask2(ccc, new inter() {
                        @Override
                        public void first() {

                        }

                        @Override
                        public Bitmap act() {
                            URL url;
                            InputStream istream;
                            try {
                                //url = new URL("https://www.gstatic.com/android/market_images/web/play_logo_x2.png");
                                //url = new URL("http://122.223.132.189:81/files/"+"test001.jpeg");
                                url = new URL("http://smileapp.yokohama/files/" + item.geticonpath());
                                //インプットストリームで画像を読み込む
                                istream = url.openStream();
                                //読み込んだファイルをビットマップに変換
                                Bitmap oBmp = BitmapFactory.decodeStream(istream);

                                istream.close();
                                return oBmp;
                            } catch (IOException e) {
                                // TODO 自動生成された catch ブロック
                                e.printStackTrace();
                                return null;
                            }
                        }

                        @Override
                        public void end(Bitmap r) {
                            ImageBuf.set(r, item.geticonpath());
                            //icons.setImageBitmap(r);

                            // 画像サイズ取得
                            int width  = r.getWidth();
                            int height = r.getHeight();

                            // リサイズ後サイズ
                            int w = 100;
                            int h = 100;

                            // 切り取り領域となるbitmap生成
                            Bitmap clipArea = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

                            // 角丸矩形を描写
                            Canvas c = new Canvas(clipArea);
                            c.drawRoundRect(new RectF(0, 0, w, h), 50, 50, new Paint(Paint.ANTI_ALIAS_FLAG));

                            // 角丸画像となるbitmap生成
                            Bitmap newImage = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

                            // 切り取り領域を描写
                            Canvas canvas = new Canvas(newImage);
                            Paint paint = new Paint();
                            canvas.drawBitmap(clipArea, 0, 0, paint);

                            // 切り取り領域内にオリジナルの画像を描写
                            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                            canvas.drawBitmap(r, new Rect(0, 0, width, height), new Rect(0, 0, w, h), paint);

                            icons.setImageBitmap(newImage);

                            int a = 0;
                            a++;
                        }
                    }).execute();
                }
            }

            TextView targetname = (TextView)convertView.findViewById(R.id.targetname);
            targetname.setText(item.getname());
            TextView targettime = (TextView)convertView.findViewById(R.id.targettime);
            targettime.setText(item.gettime());
            TextView textView = (TextView)convertView.findViewById(R.id.targetbody);
            textView.setText(item.getbody());
            final ImageView imageView;
            imageView = (ImageView)convertView.findViewById(R.id.targetimage);
                imageView.setVisibility(View.GONE);
            if(!item.getimagepath().equals("nasi")){
                imageView.setVisibility(View.VISIBLE);


                imageup(imageView, item.getimagepath());
            }
        }





        return convertView;
    }

    List<chatCustomData> objects;
    ListView list;
    chatCustomAdapter ada;
    public void setData(List<chatCustomData> objects, ListView list, chatCustomAdapter ada) {
        this.list = list;
        this.objects = objects;
        this.ada = ada;
    }
}