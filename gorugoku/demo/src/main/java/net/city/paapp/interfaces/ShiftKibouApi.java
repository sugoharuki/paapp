package net.city.paapp.interfaces;

import net.city.paapp.Model.shiftModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ShiftKibouApi {
    @FormUrlEncoded
    @POST(".")
    Call<String> setShiftKibou(@Field("user_id") String user_id, @Field("date") String date,@Field("select_shift") String select_shift,@Field("update_date") String update_date);
}
