package net.city.paapp.chat;

import android.app.Activity;
import android.view.View;
import android.widget.EditText;

import net.city.paapp.R;
import net.city.paapp.async.AsyncHelper;
import net.city.paapp.async.AsyncInterFace;
import net.city.paapp.async.voidinter;
import net.city.paapp.timeline.DialogHelper;


public class sendcommentHelper {

    public static void send(final Activity act){
        AsyncHelper.Async2(new AsyncInterFace() {
            @Override
            public void start() {
                View v =  act.getWindow().getDecorView();
                ExBtnHelper btn = (ExBtnHelper)v.findViewById(R.id.msgsend);
                btn.setEnabled(false);
                DialogHelper.makeSimpleModalDialog2(act, "送信中", "中断しますか？\n（送信してしまう場合もあります）", new voidinter() {
                    @Override
                    public void act() {
                        act.finish();
                    }
                }).show();
            }

            @Override
            public String back() {
                //ココ危険いずれ直すスタートでやって
                View v =  act.getWindow().getDecorView();
                EditText ed = (EditText)v.findViewById(R.id.msg);
                String msg = ed.getText().toString();

                //http通信する
                if (!commenthttp.send(msg).equals("err"))return "ok";
                return "err";
            }

            @Override
            public void end(String result) {
                View v =  act.getWindow().getDecorView();
                if (result.equals("err")) {
                    DialogHelper.simpleDialog(act, "通信エラー", "通信に失敗しました");
                } else {
                    EditText eee = (EditText)v.findViewById(R.id.msg);
                    eee.setText("");
                }
                ExBtnHelper btn = (ExBtnHelper)v.findViewById(R.id.msgsend);
                btn.setEnabled(true);
                loadcommentHelper.loadlist(act);
                //loadcommentHelper.loadlist(act);
                DialogHelper.dialog.dismiss();
            }
        });
    }

}
