package net.city.paapp.interfaces;

import net.city.paapp.Model.GroupChat;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface groupChatInterface {
    @FormUrlEncoded
    @POST("/groupchat.php")
    Call<List<GroupChat>> getGroupChatapi(@Field("flg") String targetname);
}
